package com.aimright.admin.demoapp.io.dto

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

data class NewsPagingResponse(
    @Expose
    @SerializedName("status")
    var status: Boolean,
    @Expose
    @SerializedName("data")
    var data: NewsPagingDataEntity
)

data class NewsPagingDataEntity(
    @Expose
    @SerializedName("results")
    var resultsEntity: ArrayList<NewsEntity>,
    @Expose
    @SerializedName("meta")
    var meta: NewsPagingMetaEntity
)

data class NewsPagingMetaEntity(
    @Expose
    @SerializedName("page_count")
    var pageCount: Int,
    @Expose
    @SerializedName("total_results")
    var totalResults: Int,
    @Expose
    @SerializedName("current_page_no")
    var currentPageNo: Int,
    @Expose
    @SerializedName("limit")
    val limit: String,
    @Expose
    @SerializedName("last_page")
    var lastPage: Boolean
)

data class NewsPagingResultsEntity(
    @Expose
    @SerializedName("id")
    var id: Int,
    @Expose
    @SerializedName("news_title")
    val newsTitle: String,
//    @Expose
//    @SerializedName("highlights")
//    val highlights: String,
//    @Expose
//    @SerializedName("news_body")
//    val newsBody: String,
//    @Expose
//    @SerializedName("tags_name")
//    var tagsName: List<String>,
//    @Expose
//    @SerializedName("category")
//    var category: List<String>,
//    @Expose
//    @SerializedName("featured")
//    var featured: Boolean,
//    @Expose
//    @SerializedName("district")
//    var district: List<String>,
//    @Expose
//    @SerializedName("breaking_news")
//    var breakingNews: Boolean,
//    @Expose
//    @SerializedName("is_visible")
//    var isVisible: Boolean,
    @Expose
    @SerializedName("time_stamp")
    val timeStamp: Date,
//    @Expose
//    @SerializedName("is_recommended")
//    var isRecommended: Boolean,
//    @Expose
//    @SerializedName("photos")
//    var photos: List<String>,
    @Expose
    @SerializedName("thumbnails")
    var thumbnails: List<String>,
//    @Expose
//    @SerializedName("videos")
//    var videos: List<String>,
    @Expose
    @SerializedName("author")
    val author: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readSerializable() as Date,
        parcel.createStringArrayList(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(newsTitle)
        parcel.writeSerializable(timeStamp)
        parcel.writeStringList(thumbnails)
        parcel.writeString(author)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<NewsPagingResultsEntity> {
        override fun createFromParcel(parcel: Parcel): NewsPagingResultsEntity {
            return NewsPagingResultsEntity(parcel)
        }

        override fun newArray(size: Int): Array<NewsPagingResultsEntity?> {
            return arrayOfNulls(size)
        }
    }
}
