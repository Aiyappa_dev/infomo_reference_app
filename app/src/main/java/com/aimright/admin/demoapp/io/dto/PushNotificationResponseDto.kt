package com.aimright.admin.demoapp.io.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class PushNotificationResponseDto(
    @Expose
    @SerializedName("status")
    var status: Boolean,
    @Expose
    @SerializedName("message")
    val message: String
)
