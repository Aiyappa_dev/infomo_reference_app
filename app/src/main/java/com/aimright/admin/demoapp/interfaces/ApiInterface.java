package com.aimright.admin.demoapp.interfaces;

import com.aimright.admin.demoapp.model.CategoryList;
import com.aimright.admin.demoapp.model.DistrictListData;
import com.aimright.admin.demoapp.model.search.searchData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {
    @GET("category/list")
    Call<CategoryList> getCategoryList();


    @GET("news/getbydistrict/{Id}")
    Call<DistrictListData> getDistrictContent(@Path("Id") String id);

    @GET("news/getbydistrict/{Id}")
    Call<DistrictListData> getDistrictContentLoad(@Path("Id") String id,  @Query("page") int page);

    @GET("find")
    Call<searchData> fetchSearch(@Path("search") String id) ;

}
