package com.aimright.admin.demoapp.io.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.aimright.admin.demoapp.model.Subcategory

data class CategoryListResponse(
    @Expose
    @SerializedName("status")
    var status: Boolean,
    @Expose
    @SerializedName("data")
    var categoriesList: List<CategoryDataEntity> = ArrayList()
)

data class CategoryDataEntity(

    @Expose
    @SerializedName("subcategory")
    var subcategory: List<Subcategory> =ArrayList(),

    @Expose
    @SerializedName("id")
    var id: String,
    @Expose
    @SerializedName("category_name")
    val categoryName: String,
    @Expose
    @SerializedName("icon_url_greyscale")
    val iconUrlGreyscale: String,
    @Expose
    @SerializedName("icon_url_white")
    val iconUrlWhite: String,
    @Expose
    @SerializedName("is_visible")
    var isVisible: Boolean,
    @Expose
    @SerializedName("category_order_number")
    var categoryOrderNumber: Int,
//    @Expose
//    @SerializedName("subcategory")
//    var subcategory: List<String>,
    @Expose
    @SerializedName("is_special")
    var isSpecial: Boolean,
    @Expose
    @SerializedName("category_type")
    var categoryType: String,

//            "category_type": "photo"
    @Expose
    @SerializedName("fetch_url")
    var fetchUrl:String,

    @Transient
    var type: String? = "",

    @Transient
    var url: String = ""



) {

    fun isHomeCategory(): Boolean = categoryName == "Home"
}