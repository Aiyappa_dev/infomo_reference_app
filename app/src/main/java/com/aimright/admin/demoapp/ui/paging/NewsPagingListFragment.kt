package com.aimright.admin.demoapp.ui.paging

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.aimright.admin.demoapp.R
import com.aimright.admin.demoapp.base.BaseFragment
import com.aimright.admin.demoapp.io.RetrofitApiServiceBuilder
import com.aimright.admin.demoapp.ui.CATEGORY_ID
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_news_paging_list.*

class NewsPagingListFragment : BaseFragment() {
    var isLastPageInScreen: Boolean = false
    var isLoadinginScreen: Boolean = false
    private var categoryId: String? = null
    private var pageNumber = 1
    private lateinit var newsPagingAdapter: NewsPagingAdapter
   // lateinit var facebookAdView: com.facebook.ads.AdView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        categoryId = arguments?.getString(CATEGORY_ID)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_news_paging_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val toolbar: Toolbar = view.findViewById(R.id.newsPgingtoolbar)
        loadFacebookAd()

        toolbar.setNavigationIcon(R.drawable.ic_back)
        toolbar.setNavigationOnClickListener { activity!!.onBackPressed() }
        val linearLayoutManager = LinearLayoutManager(context)
        newsPagingRv.layoutManager = linearLayoutManager
        newsPagingRv.setHasFixedSize(false)
        newsPagingRv.addOnScrollListener(object : PaginationScrollListener(linearLayoutManager) {
            override fun loadMoreItems() {
                loadmoreItemsInScreen()
            }

            override fun isLastPage(): Boolean {
                return isLastPageInScreen
            }

            override fun isLoading(): Boolean {
                return isLoadinginScreen
            }

        })

        loadItems()

    }

    private fun loadItems() {
        isLoadinginScreen = true
        compsiteDisposable.add(
            RetrofitApiServiceBuilder.getService().fetchNewsByPagination(categoryId, 20, 1)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    isLoadinginScreen = false
                    if (activity != null) {
                        pageNumber++
                        isLastPageInScreen = it.data.meta.lastPage
//                        pagingSwipeLayout.isRefreshing = false
                        newsPagingAdapter = NewsPagingAdapter(it.data.resultsEntity)
                        newsPagingRv.adapter = newsPagingAdapter
                        pagingProgressBar.visibility = View.GONE
                        loadFacebookAd()

                    }
                }, Throwable::printStackTrace)
        )
    }

    private fun loadmoreItemsInScreen() {
//        pagingProgressBar.visibility = View.VISIBLE

        isLoadinginScreen = true
        compsiteDisposable.add(
            RetrofitApiServiceBuilder.getService().fetchNewsByPagination(categoryId, 10, pageNumber)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    isLoadinginScreen = false
                    if (activity != null) {
                        isLastPageInScreen = it.data.meta.lastPage
                        pageNumber++
//                        pagingSwipeLayout.isRefreshing = false
                        newsPagingAdapter.addItems(it.data.resultsEntity)
                        pagingProgressBar.visibility = View.GONE
                        loadFacebookAd()

                    }
                }, Throwable::printStackTrace)
        )
    }

    companion object {
        @JvmStatic
        fun newInstance(categoryId: String, isHomeCategory: Boolean) =
            NewsPagingListFragment().apply {
                arguments = Bundle().apply {
                    putString(CATEGORY_ID, categoryId)
                }
            }
    }
    private fun loadFacebookAd() {
        news_paging_banner_container.removeAllViews()
      /*  facebookAdView =
            AdView(context, "122154748214193_710112169418445", AdSize.BANNER_HEIGHT_50)
        news_paging_banner_container.addView(facebookAdView)
        facebookAdView.loadAd()*/


    }

    override fun onDestroy() {
     /*   if (::facebookAdView.isInitialized && facebookAdView != null) {
            facebookAdView.destroy()
        }*/
        super.onDestroy()
    }

}