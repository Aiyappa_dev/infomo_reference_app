package com.aimright.admin.demoapp.model.search;

public class Videos {
    private String video_thumbnail;

    private String duration;

    private String video_url;

    private String comments;

    private String video_title;

    private String time_stamp;

    private String id;

    private String views;

    private String video_id;

    private String likes;

    public String getVideo_thumbnail ()
    {
        return video_thumbnail;
    }

    public void setVideo_thumbnail (String video_thumbnail)
    {
        this.video_thumbnail = video_thumbnail;
    }

    public String getDuration ()
    {
        return duration;
    }

    public void setDuration (String duration)
    {
        this.duration = duration;
    }

    public String getVideo_url ()
    {
        return video_url;
    }

    public void setVideo_url (String video_url)
    {
        this.video_url = video_url;
    }

    public String getComments ()
    {
        return comments;
    }

    public void setComments (String comments)
    {
        this.comments = comments;
    }

    public String getVideo_title ()
    {
        return video_title;
    }

    public void setVideo_title (String video_title)
    {
        this.video_title = video_title;
    }

    public String getTime_stamp ()
    {
        return time_stamp;
    }

    public void setTime_stamp (String time_stamp)
    {
        this.time_stamp = time_stamp;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getViews ()
    {
        return views;
    }

    public void setViews (String views)
    {
        this.views = views;
    }

    public String getVideo_id ()
    {
        return video_id;
    }

    public void setVideo_id (String video_id)
    {
        this.video_id = video_id;
    }

    public String getLikes ()
    {
        return likes;
    }

    public void setLikes (String likes)
    {
        this.likes = likes;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [video_thumbnail = "+video_thumbnail+", duration = "+duration+", video_url = "+video_url+", comments = "+comments+", video_title = "+video_title+", time_stamp = "+time_stamp+", id = "+id+", views = "+views+", video_id = "+video_id+", likes = "+likes+"]";
    }
}
