package com.aimright.admin.demoapp.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import com.aimright.admin.demoapp.R
import com.aimright.admin.demoapp.interfaces.ApiInterface
import com.aimright.admin.demoapp.model.DistrictListData
import com.aimright.admin.demoapp.model.Results
import com.aimright.admin.demoapp.ui.NotificationsAdapter
import com.aimright.admin.demoapp.util.ApiClient1
import kotlinx.android.synthetic.main.fragment_notification_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DistrictList : AppCompatActivity() {

    private var id: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_notification_list)
        notificationRv.layoutManager = LinearLayoutManager(this)
        notificationRv.setHasFixedSize(true)
        id=intent.getIntExtra("id",id)
        search.visibility=View.GONE
        loadData(id.toString())

    }
    private fun loadData(id: String?) {
        progressBar.visibility = View.GONE
        val apiInterface = ApiClient1.getApiClient().create(
            ApiInterface::class.java)


        val call: Call<DistrictListData>
        // call = apiInterface.getNewsCC("in",countryVal, API_KEY);

        call = apiInterface.getDistrictContent(id)
        call.enqueue(object : Callback<DistrictListData> {
            override fun onResponse(call: Call<DistrictListData>, response: Response<DistrictListData>) {
                if (response.body() != null) {
                    val data1= response.body()!!.data.results as Array<Results>
                    //  val data= Arrays.asList(data1)  as ArrayList<Results>
                    notificationRv.adapter= NotificationsAdapter(data1)



                }
            }

            override fun onFailure(call: Call<DistrictListData>, t: Throwable) {
                Toast.makeText(this@DistrictList, "No Result: " + t.message, Toast.LENGTH_SHORT)
                    .show()

            }
        })
    }
}
