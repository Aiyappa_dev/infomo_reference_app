package com.aimright.admin.demoapp.io.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

//
//import com.google.gson.annotations.Expose
//import com.google.gson.annotations.SerializedName
//
//data class SearchResponseDto(
//    @Expose
//    @SerializedName("status")
//    var status: Boolean,
//    @Expose
//    @SerializedName("data")
//    var data: SearchDataEntity
//)
//
//data class SearchDataEntity(
//    @Expose
//    @SerializedName("News")
//    var newsEntity: List<SearchNewsEntity>,
//    @Expose
//    @SerializedName("Videos")
//    var videosEntity: List<SearchVideosEntity>,
//    @Expose
//    @SerializedName("Photos")
//    var photosEntity: List<SearchPhotosEntity>
//)
//
//data class SearchPhotosEntity(
//    @Expose
//    @SerializedName("id")
//    var id: Int,
//    @Expose
//    @SerializedName("album_title")
//    val albumTitle: String,
//    @Expose
//    @SerializedName("description")
//    val description: String,
//    @Expose
//    @SerializedName("tags")
//    var tags: List<String>,
//    @Expose
//    @SerializedName("time_stamp")
//    val timeStamp: String,
//    @Expose
//    @SerializedName("thumbnail")
//    val thumbnail: String,
//    @Expose
//    @SerializedName("is_visible")
//    var isVisible: Boolean,
//    @Expose
//    @SerializedName("photo")
//    var photo: List<PhotoUrls>,
//    @Expose
//    @SerializedName("category")
//    var categoryEntity: List<SearchCategoryEntity>
//)
//
//data class SearchCategoryEntity(
//    @Expose
//    @SerializedName("id")
//    var id: Int,
//    @Expose
//    @SerializedName("category_name")
//    val categoryName: String,
//    @Expose
//    @SerializedName("is_visible")
//    var isVisible: Boolean,
//    @Expose
//    @SerializedName("category_order_number")
//    var categoryOrderNumber: Int,
//    @Expose
//    @SerializedName("is_special")
//    var isSpecial: Boolean
//)
//
//data class SearchVideosEntity(
//    @Expose
//    @SerializedName("id")
//    var id: Int,
//    @Expose
//    @SerializedName("video_title")
//    val videoTitle: String,
//    @Expose
//    @SerializedName("video_url")
//    val videoUrl: String,
//    @Expose
//    @SerializedName("video_thumbnail")
//    val videoThumbnail: String,
//    @Expose
//    @SerializedName("duration")
//    val duration: String,
//    @Expose
//    @SerializedName("time_stamp")
//    val timeStamp: String,
//    @Expose
//    @SerializedName("video_id")
//    val videoId: String,
//    @Expose
//    @SerializedName("description")
//    val description: String,
//    @Expose
//    @SerializedName("is_visible")
//    var isVisible: Boolean,
//    @Expose
//    @SerializedName("tags")
//    var tags: List<String>,
//    @Expose
//    @SerializedName("comments")
//    val comments: String,
//    @Expose
//    @SerializedName("likes")
//    val likes: String,
//    @Expose
//    @SerializedName("views")
//    val views: String,
//    @Expose
//    @SerializedName("category")
//    var categoryEntity: List<SearchCategoryEntity>
//)
//
//
//data class SearchNewsEntity(
//    @Expose
//    @SerializedName("id")
//    var id: Int,
//    @Expose
//    @SerializedName("news_title")
//    val newsTitle: String,
//    @Expose
//    @SerializedName("highlights")
//    val highlights: String,
////    @Expose
////    @SerializedName("news_body")
////    val newsBody: String,
//    @Expose
//    @SerializedName("tags_name")
//    var tagsName: List<String>,
//    @Expose
//    @SerializedName("category")
//    var categoryEntity: List<CategoryEntity>,
//    @Expose
//    @SerializedName("featured")
//    var featured: Boolean,
////    @Expose
////    @SerializedName("district")
////    var district: List<String>,
//    @Expose
//    @SerializedName("breaking_news")
//    var breakingNews: Boolean,
//    @Expose
//    @SerializedName("is_visible")
//    var isVisible: Boolean,
//    @Expose
//    @SerializedName("time_stamp")
//    val timeStamp: String,
//    @Expose
//    @SerializedName("is_recommended")
//    var isRecommended: Boolean,
//    @Expose
//    @SerializedName("photos")
//    var photos: List<String>,
//    @Expose
//    @SerializedName("thumbnails")
//    var thumbnails: List<String>,
//    @Expose
//    @SerializedName("videos")
//    var videos: List<String>
//)
//
data class PhotoUrls(
    @Expose
    @SerializedName("id")
    var id: Int,
    @Expose
    @SerializedName("photo")
    val photo: String
)

data class SearchResponseDto(
    @Expose
    @SerializedName("status")
    var status: Boolean,
    @Expose
    @SerializedName("data")
    var data: SearchDataEntity
)

data class SearchDataEntity(
    @Expose
    @SerializedName("News")
    var newsEntity: List<SearchNewsEntity>,
    @Expose
    @SerializedName("Videos")
    var videosEntity: List<SearchVideosEntity>,
    @Expose
    @SerializedName("Photos")
    var photosEntity: List<PhotosEntity>
)

data class SearchPhotosEntity(
    @Expose
    @SerializedName("id")
    var id: Int,
    @Expose
    @SerializedName("album_title")
    val albumTitle: String,
    @Expose
    @SerializedName("description")
    val description: String,
    @Expose
    @SerializedName("tags")
    var tags: List<String>,
    @Expose
    @SerializedName("time_stamp")
    val timeStamp: String,
    @Expose
    @SerializedName("thumbnail")
    val thumbnail: String,
    @Expose
    @SerializedName("is_visible")
    var isVisible: Boolean,
    @Expose
    @SerializedName("photo")
    var photo: List<PhotoUrls>,
    @Expose
    @SerializedName("category")
    var categoryEntity: List<CategoryEntity>
)



data class SearchVideosEntity(
    @Expose
    @SerializedName("id")
    var id: Int,
    @Expose
    @SerializedName("video_title")
    val videoTitle: String,
    @Expose
    @SerializedName("video_url")
    val videoUrl: String,
    @Expose
    @SerializedName("video_thumbnail")
    val videoThumbnail: String,
    @Expose
    @SerializedName("duration")
    val duration: String,
    @Expose
    @SerializedName("time_stamp")
    val timeStamp: Date,
    @Expose
    @SerializedName("video_id")
    val videoId: String,
    @Expose
    @SerializedName("description")
    val description: String,
    @Expose
    @SerializedName("is_visible")
    var isVisible: Boolean,
    @Expose
    @SerializedName("tags")
    var tags: List<String>,
    @Expose
    @SerializedName("comments")
    val comments: String,
    @Expose
    @SerializedName("likes")
    val likes: String,
    @Expose
    @SerializedName("views")
    val views: String,
    @Expose
    @SerializedName("category")
    var categoryEntity: List<CategoryEntity>
)


data class SearchNewsEntity(
    @Expose
    @SerializedName("id")
    var id: String,
    @Expose
    @SerializedName("news_title")
    val newsTitle: String,
    @Expose
    @SerializedName("time_stamp")
    val timeStamp: Date,
    @Expose
    @SerializedName("thumbnails")
    var thumbnails: List<String>,
    @Expose
    @SerializedName("author")
    val author: String
)