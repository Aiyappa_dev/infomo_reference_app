package com.aimright.admin.demoapp.ui

import android.content.Context
import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.aimright.admin.demoapp.R;
import kotlinx.android.synthetic.main.fragment_item_list_dialog.*
import kotlinx.android.synthetic.main.fragment_item_list_dialog_item.view.*

// TODO: Customize parameter argument names
const val ARG_ITEM_COUNT = "item_count"

/**
 *
 * A fragment that shows a list of items as a modal bottom sheet.
 *
 * You can show this modal bottom sheet from your activity like this:
 * <pre>
 *    BottomNavigationDialog.newInstance(30).show(supportFragmentManager, "dialog")
 * </pre>
 *
 * You activity (or fragment) needs to implement [BottomNavigationDialog.Listener].
 */
class BottomNavigationDialog : BottomSheetDialogFragment() {
    private var mListener: Listener? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_item_list_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        list.layoutManager = LinearLayoutManager(context)
        list.adapter = ItemAdapter()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val parent = parentFragment
        if (parent != null) {
            mListener = parent as Listener
        } else {
            mListener = context as Listener
        }
    }

    override fun onDetach() {
        mListener = null
        super.onDetach()
    }

    interface Listener {
        fun onItemClicked(position: Int, clickedItemType: String)
    }

    private inner class ViewHolder internal constructor(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.fragment_item_list_dialog_item, parent, false)) {

        internal val text: TextView = itemView.text
        internal val imageView: ImageView = itemView.bottom_sheet_IV

        init {
            text.setOnClickListener {
                mListener?.let {
                    it.onItemClicked(adapterPosition, arguments?.getString(CLICKED_ITEM_TYPE)!!)
                    dismiss()
                }
            }
        }
    }

    private inner class ItemAdapter :
        RecyclerView.Adapter<ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(parent.context), parent)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {

            when (position) {
                0 -> {
                    holder.text.text = getString(R.string.save_for_later)
                    holder.imageView.setImageResource(R.drawable.ic_bookmark)
                }
                1 -> {
                    holder.text.text = getString(R.string.share)
                    holder.imageView.setImageResource(R.drawable.ic_share_blue)
                }
                2 -> {
                    holder.text.text = getString(R.string.cancel)
                    holder.imageView.setImageResource(R.drawable.ic_close)
                }
            }
            holder.text.tag = position
        }

        override fun getItemCount(): Int {
            return 3
        }
    }

    companion object {

        fun newInstance(clickedItemType: String): BottomNavigationDialog =
            BottomNavigationDialog().apply {
                arguments = Bundle().apply {
                    putString(CLICKED_ITEM_TYPE, clickedItemType)
                }
            }

    }
}
