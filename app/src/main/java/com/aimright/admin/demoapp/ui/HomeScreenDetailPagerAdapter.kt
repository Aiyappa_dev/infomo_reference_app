package com.aimright.admin.demoapp.ui

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.aimright.admin.demoapp.io.dto.HomeScreenResponseDto

class HomeScreenDetailPagerAdapter(
    private val homeScreenResponseDto: HomeScreenResponseDto,
    fragmentManager: FragmentManager
) : FragmentPagerAdapter(fragmentManager) {


    override fun getItem(position: Int): Fragment? {
        val item = homeScreenResponseDto.data.resultsEntity[position]

//        if (item.itemType == VIDEO_TYPE) {
//            return YoutubeWebviewVideoFragment.newInstance(item.videoId)
//        } else {
//            return NewsDetailFragment.newInstance(item.id, item.itemType)
//        }

        return NewsDetailFragment.newInstance(item.id, item.itemType)
    }

    override fun getCount(): Int {
        return homeScreenResponseDto.data.resultsEntity.size
    }

}