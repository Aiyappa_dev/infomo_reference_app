package com.aimright.admin.demoapp.io.dto

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*
import kotlin.collections.ArrayList


data class CategoryResponse(
    @Expose
    @SerializedName("status")
    var status: Boolean,
    @Expose
    @SerializedName("data")
    var data: CategoryResponseDataEntity
)

data class CategoryResponseDataEntity(
    @Expose
    @SerializedName("News")
    var newsEntity: ArrayList<NewsEntity>,
    @Expose
    @SerializedName("Videos")
    var videosEntity: List<CategoryResponseVideosEntity>,
    @Expose
    @SerializedName("Photos")
    var photosEntity: List<PhotosEntity>
)

data class PhotosEntity(
    @Expose
    @SerializedName("id")
    var id: Int,
    @Expose
    @SerializedName("album_title")
    val albumTitle: String,
    @Expose
    @SerializedName("description")
    val description: String,
    @Expose
    @SerializedName("tag")
    var tag: List<String>,
    @Expose
    @SerializedName("time_stamp")
    val timeStamp: String,
    @Expose
    @SerializedName("thumbnail")
    val thumbnail: String,
    @Expose
    @SerializedName("is_visible")
    var isVisible: Boolean,
    @Expose
    @SerializedName("photo")
    var photoEntity: ArrayList<PhotoEntity>
) : Parcelable {
    constructor(source: Parcel) : this(
        source.readInt(),
        source.readString(),
        source.readString(),
        source.createStringArrayList(),
        source.readString(),
        source.readString(),
        1 == source.readInt(),
        ArrayList<PhotoEntity>().apply { source.readList(this, PhotoEntity::class.java.classLoader) }
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt(id)
        writeString(albumTitle)
        writeString(description)
        writeStringList(tag)
        writeString(timeStamp)
        writeString(thumbnail)
        writeInt((if (isVisible) 1 else 0))
        writeList(photoEntity)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<PhotosEntity> = object : Parcelable.Creator<PhotosEntity> {
            override fun createFromParcel(source: Parcel): PhotosEntity = PhotosEntity(source)
            override fun newArray(size: Int): Array<PhotosEntity?> = arrayOfNulls(size)
        }
    }
}

data class PhotoEntity(
    @Expose
    @SerializedName("id")
    var id: Int,
    @Expose
    @SerializedName("album_id")
    var albumId: Int,
    @Expose
    @SerializedName("photo_urls_thumbnail")
    val photoUrlsThumbnail: String?,
    @Expose
    @SerializedName("photo_urls")
    val photoUrls: String?
) : Parcelable {
    constructor(source: Parcel) : this(
        source.readInt(),
        source.readInt(),
        source.readString(),
        source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt(id)
        writeInt(albumId)
        writeString(photoUrlsThumbnail)
        writeString(photoUrls)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<PhotoEntity> = object : Parcelable.Creator<PhotoEntity> {
            override fun createFromParcel(source: Parcel): PhotoEntity = PhotoEntity(source)
            override fun newArray(size: Int): Array<PhotoEntity?> = arrayOfNulls(size)
        }
    }
}

data class CategoryResponseVideosEntity(
    @Expose
    @SerializedName("id")
    var id: Int,
    @Expose
    @SerializedName("video_title")
    val videoTitle: String,
    @Expose
    @SerializedName("video_url")
    val videoUrl: String,
    @Expose
    @SerializedName("video_thumbnail")
    val videoThumbnail: String,
    @Expose
    @SerializedName("duration")
    val duration: String,
    @Expose
    @SerializedName("time_stamp")
    val timeStamp: Date,
    @Expose
    @SerializedName("video_id")
    val videoId: String,
    @Expose
    @SerializedName("description")
    val description: String = "",
    @Expose
    @SerializedName("is_visible")
    var isVisible: Boolean,
    @Expose
    @SerializedName("tag")
    var tag: List<String>,
    @Expose
    @SerializedName("comments")
    val comments: String,
    @Expose
    @SerializedName("likes")
    val likes: String,
    @Expose
    @SerializedName("views")
    val views: String
){
    fun getDurationString(): String {
        return " $duration "
    }
}

data class NewsEntity(
    @Expose
    @SerializedName("id")
    var id: String,
    @Expose
    @SerializedName("news_title")
    val newsTitle: String? = "",
    @Expose
    @SerializedName("highlights")
    val highlights: String? = "",
    @Expose
    @SerializedName("news_body")
    val newsBody: String? = "",
    @Expose
    @SerializedName("tags")
    var tags: List<String> = ArrayList(),
    @Expose
    @SerializedName("featured")
    var featured: Boolean,
//    @Expose
//    @SerializedName("district")
//    var districtEntity: List<DistrictEntity>,
    @Expose
    @SerializedName("breaking_news")
    var breakingNews: Boolean,
    @Expose
    @SerializedName("is_visible")
    var isVisible: Boolean,
    @Expose
    @SerializedName("time_stamp")
    val timeStamp: Date,
//    @Expose
//    @SerializedName("lifespan")
//    val lifespan: String,
    @Expose
    @SerializedName("is_recommended")
    var isRecommended: Boolean,
    @Expose
    @SerializedName("photos")
    var photos: List<String> = ArrayList(),
    @Expose
    @SerializedName("thumbnails")
    var thumbnails: List<String> = ArrayList(),
    @Expose
    @SerializedName("author")
    var author: String
) : Parcelable {
    constructor(source: Parcel) : this(
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.createStringArrayList(),
        1 == source.readInt(),
        1 == source.readInt(),
        1 == source.readInt(),
        source.readSerializable() as Date,
        1 == source.readInt(),
        source.createStringArrayList(),
        source.createStringArrayList(),
        source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(id)
        writeString(newsTitle)
        writeString(highlights)
        writeString(newsBody)
        writeStringList(tags)
        writeInt((if (featured) 1 else 0))
        writeInt((if (breakingNews) 1 else 0))
        writeInt((if (isVisible) 1 else 0))
        writeSerializable(timeStamp)
        writeInt((if (isRecommended) 1 else 0))
        writeStringList(photos)
        writeStringList(thumbnails)
        writeString(author)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<NewsEntity> = object : Parcelable.Creator<NewsEntity> {
            override fun createFromParcel(source: Parcel): NewsEntity = NewsEntity(source)
            override fun newArray(size: Int): Array<NewsEntity?> = arrayOfNulls(size)
        }
    }
}


data class VideosEntity(
    @Expose
    @SerializedName("video")
    val video: String
)

data class DistrictEntity(
    @Expose
    @SerializedName("id")
    var id: Int,
    @Expose
    @SerializedName("district_name")
    val districtName: String,
    @Expose
    @SerializedName("district_order")
    var districtOrder: Int,
    @Expose
    @SerializedName("is_visible")
    var isVisible: Boolean
)

data class ShowMoreItem(val emptystring :String)
