package com.aimright.admin.demoapp.ui

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.aimright.admin.demoapp.R
import com.aimright.admin.demoapp.base.BaseFragment
import com.aimright.admin.demoapp.io.RetrofitApiServiceBuilder
import com.aimright.admin.demoapp.io.dto.CategoryResponseVideosEntity
import com.aimright.admin.demoapp.io.dto.NewsEntity
import com.aimright.admin.demoapp.io.dto.ResultsEntity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_home_category.*


class HomeFragment : BaseFragment(), CategotyItemClickCallback, BottomNavigationDialog.Listener {

    override fun onSwiPeDisbleEnable(enable: Boolean) {
        enableDisableSwipeRefresh(enable)
    }

    override fun onNewsDetailClicked(newsItem: ResultsEntity) {
        val homeScreenActivity = activity as HomeScreenActivity
        homeScreenActivity.replaceDetailFragment(homeScreenActivity.newsData?.data?.resultsEntity?.indexOf(newsItem)!!)
    }

    private var listener: CategoryFragment.OnFragmentInteractionListener? = null
    private var categoryId: String? = null
    private var isHomeCategory: Boolean = false

    private lateinit var categoryResponse: List<ResultsEntity>

    companion object {
        @JvmStatic
        fun newInstance(categoryId: String, isHomeCategory: Boolean) =
            HomeFragment().apply {
                arguments = Bundle().apply {
                    putString(CATEGORY_ID, categoryId)
                    putBoolean(IS_HOME_CATEGORY, isHomeCategory)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        categoryId = arguments?.getString(CATEGORY_ID)
        isHomeCategory = arguments?.getBoolean(IS_HOME_CATEGORY)!!
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home_category, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        homeSwipeLayout.setOnRefreshListener {
            Log.i(TAG, "onRefresh called from SwipeRefreshLayout")
            loadData()
        }

        homeSwipeLayout.setColorSchemeResources(
            R.color.colorPrimary
        )

        homeCategoryRv.addItemDecoration(
            DividerItemDecoration(
                context!!,
                DividerItemDecoration.VERTICAL
            )
        )

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        loadData()
    }

    private fun loadData() {
        compsiteDisposable.add(
            RetrofitApiServiceBuilder.getService().fetchHomeNews()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    if (activity != null) {
                        (activity as HomeScreenActivity).setData(it)
                        categoryResponse = it.data.resultsEntity
                        homeSwipeLayout.isRefreshing = false
//                    Log.d("NEWS", "" + it.data.resultsEntity)
                        homeCategoryRv.setHasFixedSize(false)
                        homeCategoryRv.layoutManager = LinearLayoutManager(context)
                        setAdapter(it.data.resultsEntity)


//                    news_pager.adapter = HomeHeaderPagerAdapter(
//                        ArrayList(it.data.resultsEntity.subList(0, 5))
//                    )
//                    indicator.setViewPager(news_pager)
                        progressBar.visibility = View.GONE
                    }
                }, Throwable::printStackTrace)
        )
    }

    private fun setAdapter(it: List<ResultsEntity>) {
        homeCategoryRv.adapter = HomeScreenAdapter(it.toMutableList(), this)
    }

    lateinit var newsEntity: NewsEntity
    lateinit var videosEntity: CategoryResponseVideosEntity

    override fun onNewsItemClicked(newsEntity: NewsEntity) {
        this.newsEntity = newsEntity
        BottomNavigationDialog.newInstance(NEWS_ITEM).show(childFragmentManager, "")
    }

    override fun onVideoItemClicked(videosEntity: CategoryResponseVideosEntity) {
        this.videosEntity = videosEntity
        BottomNavigationDialog.newInstance(VIDEO_ITEM).show(childFragmentManager, "")
    }

    private fun enableDisableSwipeRefresh(enable: Boolean) {
        if (homeSwipeLayout != null) {
            homeSwipeLayout.isEnabled = enable
        }
    }

    override fun onItemClicked(position: Int, clickedItemType: String) {
        if (position == 1)
            when (clickedItemType) {
                NEWS_ITEM -> {
                    val sendIntent: Intent = Intent().apply {
                        action = Intent.ACTION_SEND
                        putExtra(Intent.EXTRA_TEXT, newsEntity.newsTitle)
                        type = "text/plain"

//                        putExtra(Intent.EXTRA_TEXT, "url to send")
                    }
                    startActivity(Intent.createChooser(sendIntent, "Share via"))
                }
                VIDEO_ITEM -> {

                    val sendIntent: Intent = Intent().apply {
                        action = Intent.ACTION_SEND
                        putExtra(Intent.EXTRA_TEXT, videosEntity.videoTitle)
                        type = "text/plain"

                        putExtra(Intent.EXTRA_TEXT, videosEntity.videoUrl)
                    }
                    startActivity(Intent.createChooser(sendIntent, "Share via"))
                }
            }
    }
}