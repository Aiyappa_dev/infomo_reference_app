package com.aimright.admin.demoapp.ui

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.facebook.login.LoginManager
import com.aimright.admin.demoapp.R
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import kotlinx.android.synthetic.main.activity_profile_page.*
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.firebase.auth.FirebaseAuth
import com.aimright.admin.demoapp.activity.Login
import com.aimright.admin.demoapp.activity.RegisterAll


class ProfilePage : AppCompatActivity() {

    private var googleApiClient: GoogleApiClient? = null
    private var mAuth: FirebaseAuth? = null
    private lateinit var mGoogleSignInClient: GoogleSignInClient
    private var gso: GoogleSignInOptions? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_page)
        mAuth = FirebaseAuth.getInstance()

        back.setOnClickListener {
            finish()
        }
        register.setOnClickListener {
            val intent=Intent(this@ProfilePage, RegisterAll::class.java)
            startActivity(intent)
        }
       // data.setText(HomeScreenActivity.data)

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken("67733698617-s5niss47p1vi6dvidcjoi7h626ftemei.apps.googleusercontent.com")
            .requestEmail()
            .build()
        val user = mAuth!!.currentUser
       val sharedPreferences = getSharedPreferences("Reg", Context.MODE_PRIVATE);
      val  uName = sharedPreferences.getBoolean("isFacebookLogged", false);


        if (user != null /*|| Login.userName.isNotEmpty()*/ || uName) {
            login.setText("Logout")
            register.visibility=View.INVISIBLE
            signIn_text.visibility=View.INVISIBLE
        }
        else
        {
            register.visibility=View.VISIBLE
            signIn_text.visibility=View.VISIBLE
        }
       /* if (Login.userName.isNotEmpty() && Login.profilePic.isNotEmpty())
        {
            username.visibility=View.VISIBLE
            profile_image.visibility=View.VISIBLE
            username.setText(Login.userName)
            profile_image.setImageURI(Login.profilePic as Uri)
        }*/

        mGoogleSignInClient = GoogleSignIn.getClient(this,gso)
        login.setOnClickListener {
            val   editor = sharedPreferences.edit()
            editor.putBoolean("isFacebookLogged",false)
            editor.commit()
            if (user != null) {
                login.setText("LOGOUT")
                FirebaseAuth.getInstance().signOut()

                mAuth!!.signOut()
                LoginManager.getInstance().logOut();

                startActivity(Intent(this@ProfilePage, Login::class.java))
                finishAffinity()

// this listener will be called when there is change in firebase user session
                val authListener = FirebaseAuth.AuthStateListener { firebaseAuth ->
                    val user = firebaseAuth.currentUser

                    if (user == null) {
                        // user auth state is changed - user is null
                        // launch login activity
                        startActivity(Intent(this@ProfilePage, Login::class.java))
                        finish()
                    }
                }
            }
            else
            {
                login.setText("LOGIN")

                val intent=Intent(this@ProfilePage, Login::class.java)
                startActivity(intent)
            }





          /*  FirebaseAuth.getInstance().signOut()
            mAuth!!.signOut()
            LoginManager.getInstance().logOut();

            startActivity(Intent(this@ProfilePage, Login::class.java))
            finishAffinity()

// this listener will be called when there is change in firebase user session
            val authListener = FirebaseAuth.AuthStateListener { firebaseAuth ->
                val user = firebaseAuth.currentUser

                if (user == null) {
                    // user auth state is changed - user is null
                    // launch login activity
                    startActivity(Intent(this@ProfilePage, Login::class.java))
                    finish()
                }
            }
*/

        }

    }


}
