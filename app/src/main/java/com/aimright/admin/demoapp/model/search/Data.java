package com.aimright.admin.demoapp.model.search;

import java.io.Serializable;

public class Data implements Serializable
{
    private String[] Photos;

    private News[] news;

    private Videos[] videos;

    public String[] getPhotos ()
    {
        return Photos;
    }

    public void setPhotos (String[] Photos)
    {
        this.Photos = Photos;
    }

    public News[] getNews ()
    {
        return news;
    }

    public void setNews (News[] News)
    {
        this.news = News;
    }

    public Videos[] getVideos ()
    {
        return videos;
    }

    public void setVideos (Videos[] Videos)
    {
        this.videos = Videos;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Photos = "+Photos+", News = "+news+", Videos = "+videos+"]";
    }


}
