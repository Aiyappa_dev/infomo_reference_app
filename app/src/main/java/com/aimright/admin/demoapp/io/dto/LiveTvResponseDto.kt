package com.aimright.admin.demoapp.io.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class LiveTvResponseDto(
    @Expose
    @SerializedName("status")
    var status: Boolean,
    @Expose
    @SerializedName("data")
    var data: LiveTvDataEntity
)

data class LiveTvDataEntity(
    @Expose
    @SerializedName("id")
    var id: Int,
    @Expose
    @SerializedName("stream_title")
    val streamTitle: String,
    @Expose
    @SerializedName("stream_url")
    val streamUrl: String,
    @Expose
    @SerializedName("is_active")
    var isActive: Boolean,
    @Expose
    @SerializedName("live_stream_video_id")
    val liveStreamVideoId: String
)