package com.aimright.admin.demoapp.io.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class PushNotificationRequestDto(
    @Expose
    @SerializedName("fcm_token")
    val fcmToken: String
)