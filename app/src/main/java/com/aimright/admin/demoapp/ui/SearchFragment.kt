package com.aimright.admin.demoapp.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.aimright.admin.demoapp.R
import com.aimright.admin.demoapp.base.BaseFragment
import com.aimright.admin.demoapp.io.RetrofitApiServiceBuilder
import com.aimright.admin.demoapp.io.dto.CategoryResponseVideosEntity
import com.aimright.admin.demoapp.io.dto.NewsEntity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_search_list.*


@SuppressLint("ValidFragment")
class SearchFragment(private val p0: String?) : BaseFragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_search_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        back.setOnClickListener {
            this.activity!!.finish()
        }


                progressBar.visibility = View.VISIBLE
                compsiteDisposable.add(
                    RetrofitApiServiceBuilder.getService().searchFromServer(p0.toString())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            if (activity != null) {
//                            Log.d(TAG, it.data.toString())

                                searchRv.layoutManager = LinearLayoutManager(context)
                                searchRv.adapter = SearchAdapter(it, object : CategotyItemClickCallback {

                                    override fun onNewsItemClicked(newsEntity: NewsEntity) {


                                    }

                                    override fun onVideoItemClicked(videosEntity: CategoryResponseVideosEntity) {
                                    }

                                })
                            }
                        }, Throwable::printStackTrace)

                )
                progressBar.visibility = View.GONE


    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }
}