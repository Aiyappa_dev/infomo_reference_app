package com.aimright.admin.demoapp.ui

import android.os.Bundle
import com.aimright.admin.demoapp.R
import com.aimright.admin.demoapp.base.BaseActivity

class NewsDetailActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val articleId = intent.getStringExtra(ARTICLE_ID)
        val itemType = intent.getStringExtra(ITEM_TYPE) ?: "news"

        val beginTransaction = supportFragmentManager.beginTransaction()
        beginTransaction.replace(R.id.fragment_container, NewsDetailFragment.newInstance(articleId, itemType))
        beginTransaction.commit()
    }


    override fun onBackPressed() {
        super.onBackPressed()

//        val intent = Intent(this, HomeScreenActivity::class.java)
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
//        startActivity(intent)
    }
}
