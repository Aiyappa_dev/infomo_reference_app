package com.aimright.admin.demoapp.ui

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.PagerAdapter
import com.aimright.admin.demoapp.io.dto.CategoryDataEntity

class HomeScreenPagerAdapter(fm: FragmentManager, val categoriesList: List<CategoryDataEntity>) :
    FragmentStatePagerAdapter(fm) {


  //  private lateinit var  adList: ArrayList<AdResponse>

    override fun getItem(posiotion: Int): Fragment {
//
        if (posiotion == 0) {
            return HomeFragment.newInstance(
                categoriesList[posiotion].id,
                categoriesList[posiotion].isHomeCategory()
            )

        }

        return CategoryFragment.newInstance(
            categoriesList[posiotion].fetchUrl,
            ""//,
           /* adList*/
            /*, adList*/)
    }

    override fun getCount(): Int = categoriesList.size

    override fun getPageTitle(position: Int): CharSequence? {
        return categoriesList[position].categoryName
    }

    override fun getItemPosition(item: Any): Int {
        return if (item is HomeFragment)
            PagerAdapter.POSITION_NONE
        else
            PagerAdapter.POSITION_UNCHANGED
    }


}
