package com.aimright.admin.demoapp.ui

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.aimright.admin.demoapp.R
import com.aimright.admin.demoapp.io.dto.*
import com.aimright.admin.demoapp.ui.youtube.FullscreenDemoActivity
import com.aimright.admin.demoapp.ui.youtube.YoutubeWebviewActivity
import com.squareup.picasso.Picasso


class SearchAdapter(
    searchResponseDto: SearchResponseDto, private val categotyItemClickCallback: CategotyItemClickCallback
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val list =
        searchResponseDto.data.newsEntity + searchResponseDto.data.videosEntity + searchResponseDto.data.photosEntity


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == 0) NewsViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.news_list_item, parent, false)
        )
        else if (viewType == 1) PhotosViewHolder(
            (LayoutInflater.from(parent.context).inflate(
                R.layout.photos_list_item,
                parent,
                false
            ))
        ) else if (viewType == 3) {
            NewsViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.news_header_list_item, parent, false)
            )
        } else NewsViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.news_list_item, parent, false)
        )
//        )
//        return NewsViewHolder(
//            LayoutInflater.from(parent.context).inflate(R.layout.category_list_item, parent, false)
//        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun getItemViewType(position: Int): Int {

        return when {
            list[position] is SearchNewsEntity -> 0
            list[position] is PhotosEntity -> 1
            else -> 2
        }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, pos: Int) {
        when (getItemViewType(pos)) {
            0 -> {
                val newsItem = list[pos] as SearchNewsEntity
                viewHolder.itemView.setOnClickListener {
                    val intent = Intent(viewHolder.itemView.context, NewsDetailActivity::class.java)
                    intent.putExtra(ARTICLE_ID, newsItem.id)
                    viewHolder.itemView.context.startActivity(intent)
                }
                val newsViewHolder = viewHolder as NewsViewHolder

                newsViewHolder.title.text = newsItem.newsTitle
                val now = System.currentTimeMillis()

                val relativeTimeSpanString = DateUtils.getRelativeTimeSpanString(
                    newsItem.timeStamp.time,
                    now,
                    DateUtils.MINUTE_IN_MILLIS
                )
                newsViewHolder.timestampTv.text = relativeTimeSpanString

                if (newsItem.thumbnails.isNotEmpty() && newsItem.thumbnails[0].isNotEmpty()) {
                    Picasso.with(newsViewHolder.itemView.context)
                        .load(newsItem.thumbnails[0])
                        .fit()
                        .centerCrop()
                        .placeholder(R.drawable.ic_more_horiz_grey)
                        .into(newsViewHolder.newsListImageView)
                } else {
                    newsViewHolder.newsListImageView.setImageResource(R.drawable.ic_more_horiz_grey)
                }
                newsViewHolder.moreIv.visibility = View.GONE


            }
            1 -> {
                val photosViewHolder = viewHolder as PhotosViewHolder
                val photoItem = list[pos] as PhotosEntity


                viewHolder.itemView.setOnClickListener {
                    val intent = Intent(viewHolder.itemView.context, PhotosDetailActivity::class.java)
                    intent.putParcelableArrayListExtra(PHOTOS_LIST, photoItem.photoEntity)
                    intent.putExtra(ALBUM_TITLE, photoItem.albumTitle)
                    viewHolder.itemView.context.startActivity(intent)
                }
                photosViewHolder.phototitle.text = photoItem.albumTitle
                Picasso.with(viewHolder.itemView.context)
                    .load(photoItem.thumbnail)
                    .fit()
                    .centerCrop()
                    .placeholder(R.drawable.ic_more_horiz_grey)
                    .into(photosViewHolder.photoImage)
                if (photoItem.photoEntity != null)
                    photosViewHolder.albumCount.text = "1/${photoItem.photoEntity.size}"
            }
            2 -> {
                val catviewHolder = viewHolder as NewsViewHolder
                val videoItem = list[pos] as SearchVideosEntity

                catviewHolder.itemView.setOnClickListener {

                    val intent = Intent(
                        catviewHolder.itemView.context,
                        YoutubeWebviewActivity::class.java
                    )
                    intent.putExtra(FullscreenDemoActivity.YOUTUBE_ID, videoItem.videoId)
                    catviewHolder.itemView.context.startActivity(intent)
                }
                catviewHolder.title.text = videoItem.videoTitle
                val now = System.currentTimeMillis()

                if (videoItem.timeStamp != null && videoItem.timeStamp.time != null) {
                    val relativeTimeSpanString = DateUtils.getRelativeTimeSpanString(
                        videoItem.timeStamp.time,
                        now,
                        DateUtils.SECOND_IN_MILLIS, DateUtils.FORMAT_ABBREV_ALL
                    )
                    catviewHolder.timestampTv.text = relativeTimeSpanString
                }

                Picasso.with(catviewHolder.itemView.context)
                    .load(videoItem.videoThumbnail)
                    .fit()
                    .centerCrop()
                    .placeholder(R.drawable.ic_more_horiz_grey)
                    .into(catviewHolder.newsListImageView)

//                catviewHolder.moreIv.setOnClickListener {
//                    categotyItemClickCallback.onVideoItemClicked(videoItem)
//                }

                catviewHolder.moreIv.visibility = View.GONE
            }
        }
    }


    inner class NewsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title = itemView.findViewById(R.id.category_title_tv) as TextView
        val timestampTv = itemView.findViewById(R.id.news_timestamp) as TextView
        val newsListImageView = itemView.findViewById(R.id.news_list_IV) as ImageView
        val moreIv: ImageView = itemView.findViewById(R.id.more_IV)
    }

    inner class PhotosViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val photoImage = itemView.findViewById(R.id.photo_IV) as ImageView
        val phototitle = itemView.findViewById(R.id.photoTv) as TextView
        val albumCount = itemView.findViewById(R.id.albumCount) as TextView
    }


}