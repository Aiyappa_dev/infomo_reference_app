package com.aimright.admin.demoapp.adapter;

public class ImageSliderAdapter /*extends PagerAdapter*/ {

   /* private LayoutInflater inflater;
    private boolean doNotifyDataSetChangedOnce = false;
    private Context context;
   // private ArrayList<AdResponse> adsList;
   *//* public ImageSliderAdapter(Context context, ArrayList<AdResponse> getAdsList) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        adsList=getAdsList;
    }*//*

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount()
    {
        return adsList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View myImageLayout = inflater.inflate(R.layout.slide, view, false);
        ImageView myImage = (ImageView) myImageLayout
                .findViewById(R.id.image);
        InfomoSDK sdk= InfomoSDK.instance().init(context);
            sdk.impression(adsList.get(position).getImpressionId());
        myImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!adsList.get(position).getClickUrl().isEmpty())
                {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(adsList.get(position).getClickUrl()));
                    context.startActivity(browserIntent);
                }

            }
        });
        try {
            Glide.with(context).load(adsList.get(position).getImageUrl()).into(myImage);
        } catch (Exception e) {
            e.printStackTrace();
        }

        view.addView(myImageLayout, 0);
        return myImageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }*/
}