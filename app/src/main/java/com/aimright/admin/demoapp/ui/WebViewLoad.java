package com.aimright.admin.demoapp.ui;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.aimright.admin.demoapp.R;
import com.aimright.admin.infomosdk.utils.LogE;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class WebViewLoad extends AppCompatActivity {

    Handler handlerForJavascriptInterface = new Handler();
    @SuppressLint("JavascriptInterface")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view_load);
        WebView webview=findViewById(R.id.webview);

       // String html="https://stage01.infomo.ws/ads/www/delivery/floatal.php?zoneid=993&width=1080&adheight=&sdk=sdk&gaid=&idfa=&background=&systemtype=&macaddress=&location=BLR&vlan=&color=&weight=85&height=5%277&marital_status=&player=&compad=&cwidth=&cheight=&pwidth=&pheight=&ptype=&pvideo=&pimg=&autoplay=&format=&vast=&mute=&repeat=&stretching=&title_desc=&limit=&oposition=&adstime=&lattitude=0.0&longitude=0.0&ip=&layerstyle=&screenwidth=1080&screenheight=2030&displaywidth=1080&displayheight=2160&displaytype=Mobile&devicemodel=vince&devicebrand=Xiaomi&deviceos=Android&deviceosversion=8.1.0&is_js_enabled=true&carrier=&country=&countryname=&region=&city=&useragent=Mozilla%2F5.0+%28Linux%3B+Android+8.1.0%3B+Redmi+Note+5+Build%2FOPM1.171019.019%3B+wv%29+AppleWebKit%2F537.36+%28KHTML%2C+like+Gecko%29+Version%2F4.0+Chrome%2F84.0.4147.125+Mobile+Safari%2F537.36&language=en&postalcode=&device_appid=com.aimright.admin.demoapp&app_name=&device_ifa=04fd4fcd-9f8e-4926-a92f-945f3152cd17&device_app_cat=&device_app_sha1=0cd76d43e4927d0622bdf2a45095e5a5ab4bb416&device_app_md5=bdd20b042df41202333a477feb08d18a&device_app_dpidsha1=&device_app_dpidmd5=&device_app_ipv6=&udid=5d7eca097c06c4bf&timezone=Asia/Kolkata&dataspeed=&connection=Keep-Alive&connectiontype=WIFI&username=&useremail=&userphone=&position=&age=&gender=&sentiment=&site=&keywords=Sports%2CPolitics%2CArt%2CMovies%2CBooks&request_id=&viewerid=&align=left&valign=top&closebutton=f&backcolor=FFFFFF&bordercolor=000000";

      //  String html="<!DOCTYPE html><html><body style= \"width=\"100%\";height=\"100%\";initial-scale=\"1.0\"; maximum-scale=\"1.0\"; user-scalable=\"no\";\"><script type='text/javascript' src='https://stage01.infomo.ws/ads/www/delivery/floatal.php?zoneid=993&width=1080&adheight=&sdk=sdk&gaid=&idfa=&background=&systemtype=&macaddress=&location=BLR&vlan=&color=&weight=85&height=5%277&marital_status=&player=&compad=&cwidth=&cheight=&pwidth=&pheight=&ptype=&pvideo=&pimg=&autoplay=&format=&vast=&mute=&repeat=&stretching=&title_desc=&limit=&oposition=&adstime=&lattitude=0.0&longitude=0.0&ip=&layerstyle=&screenwidth=1080&screenheight=2030&displaywidth=1080&displayheight=2160&displaytype=Mobile&devicemodel=vince&devicebrand=Xiaomi&deviceos=Android&deviceosversion=8.1.0&is_js_enabled=true&carrier=&country=&countryname=&region=&city=&useragent=Mozilla%2F5.0+%28Linux%3B+Android+8.1.0%3B+Redmi+Note+5+Build%2FOPM1.171019.019%3B+wv%29+AppleWebKit%2F537.36+%28KHTML%2C+like+Gecko%29+Version%2F4.0+Chrome%2F84.0.4147.125+Mobile+Safari%2F537.36&language=en&postalcode=&device_appid=com.aimright.admin.demoapp&app_name=&device_ifa=04fd4fcd-9f8e-4926-a92f-945f3152cd17&device_app_cat=&device_app_sha1=0cd76d43e4927d0622bdf2a45095e5a5ab4bb416&device_app_md5=bdd20b042df41202333a477feb08d18a&device_app_dpidsha1=&device_app_dpidmd5=&device_app_ipv6=&udid=5d7eca097c06c4bf&timezone=Asia/Kolkata&dataspeed=&connection=Keep-Alive&connectiontype=WIFI&username=&useremail=&userphone=&position=&age=&gender=&sentiment=&site=&keywords=Sports%2CPolitics%2CArt%2CMovies%2CBooks&request_id=&viewerid=&align=left&valign=top&closebutton=f&backcolor=FFFFFF&bordercolor=000000'></script></body></html>\n";
        String html="<html>" +
                "<body style= \"width=\"100%\";height=\"100%\";initial-scale=\"1.0\"; maximum-scale=\"1.0\"; user-scalable=\"no\";\">" +
                "<script type=\"text/javascript\">\n" +
                "google_ad_client = \"ca-pub-8834194653550774\";\n" +
                "/* cm_infomo_net_320x50 */\n" +
                "google_ad_slot = \"cm_infomo_net_320x50\";\n" +
                "google_ad_width = 320;\n" +
                "google_ad_height = 50;\n" +
                "</script>\n" +
                "<script type=\"text/javascript\" src=\"https://pagead2.googlesyndication.com/pagead/show_ads.js\">\n" +
                "</script>" +
                "</body>" +
                "</html>\n";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (0 != (getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE))
            { WebView.setWebContentsDebuggingEnabled(true); }
        }

        webview.getSettings().setJavaScriptEnabled(true);
       // webview.addJavascriptInterface(new MyJavaScriptInterface(this), "HtmlViewer");
        webview.setWebViewClient(new WebViewClient()
                                 {
                                     @Override
                                     public void onPageFinished(WebView view, String url)
                                     {
                          /*               webview.loadUrl("javascript:window.HtmlViewer.showHTML" +
                                                 "('&lt;html&gt;'+document.getElementsByTagName('html')[0].innerHTML+'&lt;/html&gt;');");
                          */           }

                                     @Override
                                     public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                         if (url.startsWith("https://")) {
                                             try {
                                                 String html = URLDecoder.decode(url, "UTF-8").substring(9);
                                                 LogE.printMe("WEBVIEW RESPONSE : " + html);
                                             } catch (UnsupportedEncodingException var4) {
                                                 Log.e("example", "failed to decode source", var4);
                                             }

                                             webview.getSettings().setJavaScriptEnabled(false);
                                             return true;
                                         } else {
                                             return false;
                                         }
                                       //  return super.shouldOverrideUrlLoading(view, url);
                                     }
                                 }
        );
        webview.loadData(html, "text/html","UTF-8");







        /*mWebView.getSettings().setJavaScriptEnabled(true);


        mWebView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                mWebView.loadUrl(url);
                return false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon){
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl){
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                mWebView.loadUrl("javascript:window.HTMLOUT.showHTML('<head>'+document.getElementsByTagName('html')[0].innerHTML+'</head>');");

            }
        });
        mWebView.loadUrl(html);*/




       /* web_view.loadUrl(html);
        web_view.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.startsWith("https://")) {
                    try {
                        String html = URLDecoder.decode(url, "UTF-8").substring(9);
                        LogE.printMe("WEBVIEW RESPONSE : " + html);
                    } catch (UnsupportedEncodingException var4) {
                        Log.e("example", "failed to decode source", var4);
                    }

                    web_view.getSettings().setJavaScriptEnabled(false);
                    return true;
                } else {
                    return false;
                }
            }

        });*/



      /*
        web_view.setSoundEffectsEnabled(true);
        String html="<!DOCTYPE html><html><body style= \"width=\"100%\";height=\"100%\";initial-scale=\"1.0\"; maximum-scale=\"1.0\"; user-scalable=\"no\";\"><script type='text/javascript' src='https://stage01.infomo.ws/ads/www/delivery/floatal.php?zoneid=993&width=720&adheight=&sdk=sdk&gaid=&idfa=&background=&systemtype=&macaddress=&location=&vlan=&color=&weight=60&height=165&marital_status=&player=&compad=&cwidth=&cheight=&pwidth=&pheight=&ptype=&pvideo=&pimg=&autoplay=&format=&vast=&mute=&repeat=&stretching=&title_desc=&limit=&oposition=&adstime=&lattitude=0.0&longitude=0.0&ip=&layerstyle=&screenwidth=720&screenheight=1280&displaywidth=720&displayheight=1280&displaytype=Mobile&devicemodel=tiare&devicebrand=Xiaomi&deviceos=Android&deviceosversion=8.1.0&is_js_enabled=true&carrier=airtel&country=&countryname=&region=&city=&useragent=Mozilla%2F5.0+%28Linux%3B+Android+8.1.0%3B+Redmi+Go+Build%2FOPM1.171019.026%3B+wv%29+AppleWebKit%2F537.36+%28KHTML%2C+like+Gecko%29+Version%2F4.0+Chrome%2F80.0.3987.132+Mobile+Safari%2F537.36&language=en&postalcode=&device_appid=com.aimright.admin.demoapp&app_name=&device_ifa=66c8475f-70c3-4d47-b607-2e30aa10d224&device_app_cat=&device_app_sha1=6dadcb0c5252f6a1eaa2cdb746171d2da625b923&device_app_md5=3086ec346895470fb497f22d60e2e04a&device_app_dpidsha1=&device_app_dpidmd5=&device_app_ipv6=&udid=dad20fe43aad8ef0&timezone=Asia/Kolkata&dataspeed=LTE&connection=Keep-Alive&connectiontype=MOBILE&username=&useremail=&userphone=&position=&age=&gender=&sentiment=&site=&keywords=Sports%2CPolitics%2CArt%2CMovies%2CBooks&request_id=&viewerid=&align=left&valign=top&closebutton=f&backcolor=FFFFFF&bordercolor=000000'></script></body></html>";
        Map<String, String> extraHeaders = new HashMap();
        extraHeaders.put("Referer", "https://infomo.net/");
        extraHeaders.put("X-Requested-With", this.getPackageName());

        web_view.setWebChromeClient(new WebChromeClient() {
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                LogE.printMe("WEBVIEW CONSOLE: " + consoleMessage.message());
                return true;
            }
        });
        web_view.loadUrl(html, extraHeaders);*/





        /*Uri uri = Uri.parse(html);
        Intent browserIntent = new Intent(Intent.ACTION_VIEW);
        browserIntent.setDataAndType(uri, "text/html");
        browserIntent.addCategory(Intent.CATEGORY_BROWSABLE);
        startActivity(browserIntent);*/


    }
   /* class MyJavaScriptInterface
    {
        private Context ctx;
        MyJavaScriptInterface(Context ctx)
        {
            this.ctx = ctx;
        }
        public void showHTML(String html)
        {
            final html_ = html;
            handlerForJavascriptInterface.post(new Runnable()
                                               {
                                                   @Override
                                                   public void run()
                                                   {
                                                       Toast toast = Toast.makeText(this, "Page has been loaded in webview. html content :"+html_, Toast.LENGTH_LONG);
                                                       toast.show();
                                                   }
                                               }
            );
        }
    }*/
}