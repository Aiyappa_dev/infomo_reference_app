package com.aimright.admin.demoapp.ui

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.aimright.admin.demoapp.R
import com.aimright.admin.demoapp.io.dto.VIDEO_ITEM_TYPE
import com.squareup.picasso.Picasso
import android.widget.*
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.aimright.admin.demoapp.io.dto.Results
import com.aimright.admin.demoapp.ui.youtube.FullscreenDemoActivity
import com.aimright.admin.demoapp.ui.youtube.YoutubeWebviewActivity
import com.google.android.gms.ads.formats.UnifiedNativeAd
import com.google.android.gms.ads.formats.UnifiedNativeAdView
import java.util.ArrayList
import java.util.concurrent.ThreadLocalRandom


class CategoriesVideos(
    var categoriesResponse: ArrayList<Results>,
    var mNativeAds: ArrayList<UnifiedNativeAd>,
    var context: Context?
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
  //  private var adView: UnifiedNativeAdView? = null

    private var position:Int=0
    // The unified native ad view type.
    private val UNIFIED_NATIVE_AD_VIEW_TYPE = 3
    private val TYPE_ONE = 1
    private val TYPE_TWO = 2
    private val VIEW_TYPE_LOADING = 4
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == UNIFIED_NATIVE_AD_VIEW_TYPE)
        {
            val unifiedNativeLayoutView = LayoutInflater.from(parent.context).inflate(
                R.layout.ad_video,
                parent, false
            )
            return UnifiedNativeAdViewHolder(
                unifiedNativeLayoutView
            )
        }
        else if (viewType === TYPE_ONE) {
            return NewsViewHolderLarge(LayoutInflater.from(parent.context).inflate(R.layout.home_screen_news_list_item, parent, false))

        }
        else if(viewType == VIEW_TYPE_LOADING)
        {
            val view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return  LoadingViewHolder(view);
        }
        /*else if (viewType === TYPE_TWO) {
            return NewsViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_list, parent, false))

        }*/ else {
            throw RuntimeException("The type has to be ONE or TWO") as Throwable
        }


    }



    /*fun getAdView(): UnifiedNativeAdView? {
        return adView
    }
*/







    override fun getItemCount(): Int {
        return categoriesResponse.size
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, pos: Int) {
        when(viewHolder.itemViewType)
        {
            UNIFIED_NATIVE_AD_VIEW_TYPE->
            {
             //   position++
                if (mNativeAds.size!=0 && mNativeAds.size>1)
                {
                    position=ThreadLocalRandom.current().nextInt(1, mNativeAds.size + 1)

                    val nativeAd = mNativeAds.get(position-1)
                    populateNativeAdView(nativeAd, (viewHolder as UnifiedNativeAdViewHolder).adView)
                }

            }


            TYPE_ONE->newsTypeOne(viewHolder, pos)
           // TYPE_TWO -> newsTypeTwo(viewHolder, pos)
            VIEW_TYPE_LOADING->
            {
                showLoadingView(viewHolder as LoadingViewHolder, position)
            }

        }
    }

    private fun populateNativeAdView(nativeAd: UnifiedNativeAd,
        adView: UnifiedNativeAdView

    ) {
        // Some assets are guaranteed to be in every UnifiedNativeAd.
        (adView.headlineView as TextView).text = nativeAd.headline
       // (adView.bodyView as TextView).text = nativeAd.body
        (adView.callToActionView as Button).setText(nativeAd.callToAction)

        // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
        // check before trying to display them.
       /* val icon = nativeAd.icon

        if (icon == null) {
            adView.iconView.visibility = View.INVISIBLE
        } else {
            (adView.iconView as ImageView).setImageDrawable(icon.drawable)
            adView.iconView.visibility = View.VISIBLE
        }*/

       /* if (nativeAd.price == null) {
            adView.priceView.visibility = View.INVISIBLE
        } else {
            adView.priceView.visibility = View.VISIBLE
            (adView.priceView as TextView).text = nativeAd.price
        }*/

        /*if (nativeAd.store == null) {
            adView.storeView.visibility = View.INVISIBLE
        } else {
            adView.storeView.visibility = View.VISIBLE
            (adView.storeView as TextView).text = nativeAd.store
        }*/

       /* if (nativeAd.starRating == null) {
            adView.starRatingView.visibility = View.INVISIBLE
        } else {
            (adView.starRatingView as RatingBar).rating = nativeAd.starRating!!.toFloat()
            adView.starRatingView.visibility = View.VISIBLE
        }*/

      /*  if (nativeAd.advertiser == null) {
            adView.advertiserView.visibility = View.INVISIBLE
        } else {
            (adView.advertiserView as TextView).text = nativeAd.advertiser
            adView.advertiserView.visibility = View.VISIBLE
        }*/
        // Assign native ad object to the native view.
        adView.setNativeAd(nativeAd)
    }

    private fun newsTypeOne(
        viewHolder: RecyclerView.ViewHolder,
        pos: Int
    ) {
        val newsViewHolder = viewHolder as NewsViewHolderLarge
        val newsItem = categoriesResponse.get(pos)
        newsViewHolder.title.text = newsItem.getTitle()
        newsViewHolder.itemView.setOnClickListener() {
            showInterstitialAd(viewHolder.itemView.context)

            val intent = Intent(context
                ,
                YoutubeWebviewActivity::class.java
            )
            intent.putExtra(FullscreenDemoActivity.YOUTUBE_ID, categoriesResponse.get(pos).videoId)
           context!!.startActivity(intent)
            /*val intent = Intent(viewHolder.itemView.context, NewsDetailPagerActivity::class.java)
            intent.putParcelableArrayListExtra(NEWS_LIST, categoriesResponse)
            intent.putExtra(NEWS_SELECTED_POSITION, pos)
            viewHolder.itemView.context.startActivity(intent)*/
        }

        val now = System.currentTimeMillis()

        val relativeTimeSpanString = DateUtils.getRelativeTimeSpanString(
            newsItem.timeStamp?.time!!,
            now,
            DateUtils.MINUTE_IN_MILLIS
        )
        newsViewHolder.timestampTv.text = relativeTimeSpanString


        if (newsItem.getThumbNail().isNotEmpty()) {
            Picasso.with(newsViewHolder.itemView.context)
                .load(newsItem.getThumbNail())
                .fit()
                .centerCrop()
                .placeholder(R.drawable.ic_more_horiz_grey)
                .into(newsViewHolder.newsListImageView)
        } else {
            newsViewHolder.newsListImageView.setImageResource(R.drawable.ic_more_horiz_grey)
        }
        newsViewHolder.moreIv.visibility = View.GONE
        newsViewHolder.headerLayout.visibility = View.GONE
        if (newsItem.Author != null) {
            val authorText = "Reported By - " + newsItem.Author
            newsViewHolder.authorTextView.text = authorText
        }
        newsViewHolder.categoryTextView.visibility = View.GONE

        if (newsItem.getItemViewType() == VIDEO_ITEM_TYPE) {
            newsViewHolder.videoDurationTv.visibility = View.VISIBLE
            newsViewHolder.videoDurationTv.text = newsItem.getDurationString()
        } else {
            newsViewHolder.videoDurationTv.visibility = View.GONE

        }

        newsViewHolder.videoDurationTv.visibility = View.GONE
    }

   /* private fun newsTypeTwo(
        viewHolder: RecyclerView.ViewHolder,
        pos: Int
    ) {
        val newsViewHolder = viewHolder as NewsViewHolder
        val newsItem = categoriesResponse.data.results[pos]
        newsViewHolder.title.text = newsItem.getTitle()
        newsViewHolder.itemView.setOnClickListener() {
            val intent = Intent(viewHolder.itemView.context, NewsDetailPagerActivity::class.java)
            intent.putParcelableArrayListExtra(NEWS_LIST, categoriesResponse.data.results)
            intent.putExtra(NEWS_SELECTED_POSITION, pos)
            viewHolder.itemView.context.startActivity(intent)
        }

        val now = System.currentTimeMillis()

        val relativeTimeSpanString = DateUtils.getRelativeTimeSpanString(
            newsItem.timeStamp?.time!!,
            now,
            DateUtils.MINUTE_IN_MILLIS
        )
        newsViewHolder.timestampTv.text = relativeTimeSpanString


        if (newsItem.getThumbNail().isNotEmpty()) {
            Picasso.with(newsViewHolder.itemView.context)
                .load(newsItem.getThumbNail())
                .fit()
                .centerCrop()
                .placeholder(R.drawable.ic_more_horiz_grey)
                .into(newsViewHolder.newsListImageView)
        } else {
            newsViewHolder.newsListImageView.setImageResource(R.drawable.ic_more_horiz_grey)
        }
        newsViewHolder.moreIv.visibility = View.GONE
        newsViewHolder.headerLayout.visibility = View.GONE
        if (newsItem.Author != null) {
            val authorText = "Reported By - " + newsItem.Author
            newsViewHolder.authorTextView.text = authorText
        }
        newsViewHolder.categoryTextView.visibility = View.GONE

        if (newsItem.getItemViewType() == VIDEO_ITEM_TYPE) {
            newsViewHolder.videoDurationTv.visibility = View.VISIBLE
            newsViewHolder.videoDurationTv.text = newsItem.getDurationString()
        } else {
            newsViewHolder.videoDurationTv.visibility = View.GONE

        }

        newsViewHolder.videoDurationTv.visibility = View.GONE
    }*/

    override fun getItemViewType(position: Int): Int {
       // return super.getItemViewType(position)
        /*val recyclerViewItem = mRecyclerViewItems.get(position)
        if (recyclerViewItem instanceof UnifiedNativeAd) {

        }*/
        if (categoriesResponse.get(position)==null)
        {
            return VIEW_TYPE_LOADING
        }

           if (position==0)
            return TYPE_ONE
       else if ((position+1)%5==0)
        {
            return  UNIFIED_NATIVE_AD_VIEW_TYPE
        }
        else
            return TYPE_ONE

    }

    private fun showLoadingView(viewHolder: LoadingViewHolder, position: Int) {
        //ProgressBar would be displayed

    }
    inner class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    {
        val progress=itemView.findViewById<ProgressBar>(R.id.progressBar)

    }

      /*  inner class NewsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title = itemView.findViewById(R.id.category_title_tv) as TextView
        val timestampTv = itemView.findViewById(R.id.news_timestamp) as TextView
        val newsListImageView = itemView.findViewById(R.id.news_list_IV) as ImageView
        val moreIv: ImageView = itemView.findViewById(R.id.more_IV)
        val videoDurationTv = itemView.findViewById(R.id.videoDuration) as TextView
        val authorTextView = itemView.findViewById(R.id.reportedBy) as TextView
        val categoryTextView = itemView.findViewById(R.id.category) as TextView
        val headerLayout = itemView.findViewById(R.id.header) as LinearLayout

    }*/
    inner class NewsViewHolderLarge(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title = itemView.findViewById(R.id.category_title_tv1) as TextView
        val timestampTv = itemView.findViewById(R.id.news_timestamp1) as TextView
        val newsListImageView = itemView.findViewById(R.id.news_list_IV1) as ImageView
        val moreIv: ImageView = itemView.findViewById(R.id.more_IV1)
        val videoDurationTv = itemView.findViewById(R.id.videoDuration1) as TextView
        val authorTextView = itemView.findViewById(R.id.reportedBy1) as TextView
        val categoryTextView = itemView.findViewById(R.id.category1) as TextView
        val headerLayout = itemView.findViewById(R.id.header1) as LinearLayout

    }
    private fun showInterstitialAd(context: Context) {
      val  mInterstitialAd = InterstitialAd(context)
        mInterstitialAd.adUnitId = "ca-app-pub-1276883186035608/7393283409"
        mInterstitialAd.loadAd(AdRequest.Builder().build())
        mInterstitialAd.adListener = object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                mInterstitialAd.show()
            }
        }
    }

}