package com.aimright.admin.demoapp.ui

import android.os.Bundle
import com.aimright.admin.demoapp.R
import com.aimright.admin.demoapp.base.BaseActivity
import com.aimright.admin.demoapp.io.dto.PhotoEntity

class PhotosDetailActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val photosList = intent.getParcelableArrayListExtra<PhotoEntity>(PHOTOS_LIST)
        val albumTitle = intent.getStringExtra(ALBUM_TITLE)
        val beginTransaction = supportFragmentManager.beginTransaction()
        beginTransaction.replace(R.id.fragment_container, PhotosPagerFragment.newInstance(photosList,albumTitle))
        beginTransaction.commit()
    }

}