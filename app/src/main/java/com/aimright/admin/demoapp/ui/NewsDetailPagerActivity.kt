package com.aimright.admin.demoapp.ui

import android.os.Bundle
import com.aimright.admin.demoapp.R
import com.aimright.admin.demoapp.base.BaseActivity
import com.aimright.admin.demoapp.io.dto.Results
import com.aimright.admin.infomosdk.djax.adserver.ErrorCodes
import com.aimright.admin.infomosdk.djax.adserver.MInterstitial_Interstitial
import com.aimright.admin.infomosdk.interfaces.InterstitialImageAdListener

const val ARTICLE_ID = "article_Id"
const val PUSH_NOTIFICATION_TYPE = "push_notification_type"
const val ITEM_ID ="item_id"
const val IS_FROM_PUSH = "is_from_push"

class NewsDetailPagerActivity : BaseActivity(), InterstitialImageAdListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        val articleId = intent.getStringExtra(ARTICLE_ID)
         val newsSelectedPosition = intent.getIntExtra(NEWS_SELECTED_POSITION,0)


        val newsList = intent.getParcelableArrayListExtra<Results>(NEWS_LIST)
        val beginTransaction = supportFragmentManager.beginTransaction()
        beginTransaction.replace(R.id.fragment_container, NewsDetailPagerFragment.newInstance(newsList,newsSelectedPosition))
        beginTransaction.commit()
    }

    override fun onInterstitialAdShown(p0: MInterstitial_Interstitial?) {
    }

    override fun onInterstitialAdLoaded(p0: MInterstitial_Interstitial?) {
    }

    override fun onInterstitialAdDismissed(p0: MInterstitial_Interstitial?) {
    }

    override fun onInterstitialAdClicked(p0: MInterstitial_Interstitial?) {
    }

    override fun onInterstitialAdFailed(p0: MInterstitial_Interstitial?, p1: ErrorCodes?) {
    }
}