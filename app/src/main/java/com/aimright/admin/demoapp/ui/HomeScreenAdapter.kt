package com.aimright.admin.demoapp.ui

import android.support.v4.view.ViewPager
import android.support.v7.widget.RecyclerView
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.aimright.admin.demoapp.R
import com.aimright.admin.demoapp.io.dto.ResultsEntity
import com.squareup.picasso.Picasso
import me.relex.circleindicator.CircleIndicator

const val NEWS_TYPE = "news"
const val PHOTOS_TYPE = "photoalbum"
const val VIDEO_TYPE = "video"
const val HEADER_TYPE = 0
const val NORMAL_ITEM = 1

class HomeScreenAdapter(
    var homeScreenList: MutableList<ResultsEntity>,
    private val categotyItemClickCallback: CategotyItemClickCallback
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var pagerListData: List<ResultsEntity>

    init {
        pagerListData = ArrayList(homeScreenList.subList(0, 8))
        homeScreenList.add(0, homeScreenList[10])
        homeScreenList.removeAt(1)

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == HEADER_TYPE) NewsPagerHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.home_screen_pager_layout, parent, false)
        )
//        else if (viewType == 1) PhotosViewHolder(
//            (LayoutInflater.from(parent.context).inflate(R.layout.photos_list_item, parent, false))
//        )
        else NewsViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.home_screen_news_list_item, parent, false)
        )
    }

    override fun getItemViewType(position: Int): Int {
        if (position == 0) {
            return HEADER_TYPE
        }
        return NORMAL_ITEM

    }

    override fun getItemCount(): Int {
        return homeScreenList.size
    }


    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, pos: Int) {
        val itemViewType = getItemViewType(pos)
        if (itemViewType == NORMAL_ITEM) {
            val newsItem = homeScreenList[pos]
            viewHolder.itemView.setOnClickListener {

                categotyItemClickCallback.onNewsDetailClicked(newsItem)


//                if (newsItem.itemType == NEWS_TYPE) {
//                    val intent = Intent(viewHolder.itemView.context, NewsDetailActivity::class.java)
//                    intent.putExtra(ARTICLE_ID, newsItem.id)
//                    viewHolder.itemView.context.startActivity(intent)
//                } else if (newsItem.itemType == PHOTOS_TYPE) {
//                    val intent = Intent(viewHolder.itemView.context, PhotosDetailActivity::class.java)
//                    intent.putExtra(ALBUM_TITLE, newsItem.albumTitle)
//
//                    intent.putParcelableArrayListExtra(PHOTOS_LIST, newsItem.photosList)
//                    viewHolder.itemView.context.startActivity(intent)
//                } else {
//                    val intent = Intent(viewHolder.itemView.context, YoutubeWebviewActivity::class.java)
//                    intent.putExtra(FullscreenDemoActivity.YOUTUBE_ID, newsItem.videoId)
//                    viewHolder.itemView.context.startActivity(intent)
//                }
            }
            val newsViewHolder = viewHolder as NewsViewHolder

            newsViewHolder.title.text = newsItem.getTitle()
            val now = System.currentTimeMillis()

            val relativeTimeSpanString = DateUtils.getRelativeTimeSpanString(
                newsItem.timeStamp.time,
                now,
                DateUtils.MINUTE_IN_MILLIS
            )
            newsViewHolder.timestampTv.text = relativeTimeSpanString
            val thumbnailUrl = newsItem.getThumbnailUrl()
            if (thumbnailUrl != null && thumbnailUrl.isNotEmpty()) {
                Picasso.with(newsViewHolder.itemView.context)
                    .load(thumbnailUrl)
                    .fit()
                    .centerCrop()
                    .placeholder(R.drawable.ic_more_horiz_grey)
                    .into(newsViewHolder.newsListImageView)
            } else {
                newsViewHolder.newsListImageView.setImageResource(R.drawable.ic_more_horiz_grey)
            }
            newsViewHolder.moreIv.visibility = View.GONE


            if (newsItem.itemType == VIDEO_TYPE) {
                newsViewHolder.videoDurationTv.visibility = View.VISIBLE
                newsViewHolder.videoDurationTv.text = newsItem.getDurationString()
            } else {
                newsViewHolder.videoDurationTv.visibility = View.GONE
            }
            if (newsItem.author != null) {
                val authorText = "Reported By - " + newsItem.author
                newsViewHolder.authorTextView.text = authorText
            }
            if (newsItem.categoryEntity != null && newsItem.categoryEntity.isNotEmpty())
                newsViewHolder.categoryTextView.text = newsItem.categoryEntity[0].categoryName
        } else {
            val pagerHolder = viewHolder as NewsPagerHolder
            pagerHolder.newsPager.adapter = HomeHeaderPagerAdapter(ArrayList(pagerListData), categotyItemClickCallback)


            pagerHolder.newsPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageScrolled(position: Int, v: Float, i1: Int) {}

                override fun onPageSelected(position: Int) {

                    val currentItem = pagerListData[position]
                    val indexOf = homeScreenList.indexOf(currentItem)
                    val didRemove = homeScreenList.removeAll { currentItem.id == it.id }
                    if (didRemove) {
                        notifyItemRemoved(indexOf)
                    }
                }

                override fun onPageScrollStateChanged(state: Int) {
                    categotyItemClickCallback.onSwiPeDisbleEnable(state == ViewPager.SCROLL_STATE_IDLE)
//                    enableDisableSwipeRefresh( state == ViewPager.SCROLL_STATE_IDLE );
                }
            })
            pagerHolder.leftNav.setOnClickListener {
                pagerHolder.newsPager.currentItem = pagerHolder.newsPager.currentItem - 1
            }
            pagerHolder.rightNav.setOnClickListener {
                pagerHolder.newsPager.currentItem = pagerHolder.newsPager.currentItem + 1
            }

            pagerHolder.circleIndicator.setViewPager(pagerHolder.newsPager)
        }
    }


    inner class NewsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title = itemView.findViewById(R.id.category_title_tv) as TextView
        val timestampTv = itemView.findViewById(R.id.news_timestamp) as TextView
        val newsListImageView = itemView.findViewById(R.id.news_list_IV) as ImageView
        val moreIv: ImageView = itemView.findViewById(R.id.more_IV)
        val videoDurationTv = itemView.findViewById(R.id.videoDuration) as TextView
        val authorTextView = itemView.findViewById(R.id.reportedBy) as TextView
        val categoryTextView = itemView.findViewById(R.id.category) as TextView
    }

    inner class NewsPagerHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val newsPager = itemView.findViewById(R.id.news_pager) as ViewPager
        val circleIndicator = itemView.findViewById(R.id.indicator) as CircleIndicator
        val leftNav = itemView.findViewById(R.id.nav_left) as ImageView
        val rightNav = itemView.findViewById(R.id.nav_right) as ImageView
    }

}