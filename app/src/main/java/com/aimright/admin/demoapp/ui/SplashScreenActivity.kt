package com.aimright.admin.demoapp.ui

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.aimright.admin.demoapp.R
import com.aimright.admin.infomosdk.BuildConfig
import com.aimright.admin.infomosdk.apis.InfomoSDK
import com.aimright.admin.infomosdk.network.NetWorkCall
import com.aimright.admin.infomosdk.utils.*
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.doubleclick.PublisherAdRequest
import com.google.android.gms.ads.doubleclick.PublisherInterstitialAd
import com.google.android.gms.ads.identifier.AdvertisingIdClient
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.iid.FirebaseInstanceId
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.splash_screen.*
import java.io.IOException
import java.util.*
import java.util.concurrent.TimeUnit


class SplashScreenActivity : AppCompatActivity() /*, NotifyOperationComplete*/ {


    private var infomoSDK: InfomoSDK? = null

    //   private var mNativeAds = ArrayList<AdResponse>()
   // private var bannerAd = ArrayList<AdResponse>()
   // public var infomoSDK1: InfomoSDK? = null
    private var mAuth: FirebaseAuth? = null
    var isFirstTime: Boolean? = null
    private var deviceID: String? = null
    // private InfomoSDK infomoSDK11;
   // private var pref: SharedPreference? = null
    private var fcmToken: String? = null
    // private CallReceiver callReceiver;
//   private IntentFilter filter;
    private val mFusedLocationClient: FusedLocationProviderClient? = null
    private val mSettingsClient: SettingsClient? = null
    private val mLocationCallback: LocationCallback? = null
    private val mCurrentLocation: Location? = null
    private val mLocationRequest: LocationRequest? = null
    private val mLocationSettingsRequest: LocationSettingsRequest? = null
    private var isRegistered = false

    // List of native ads that have been successfully loaded.
    private lateinit var mPublisherInterstitialAd: PublisherInterstitialAd


    //package="com.otoblitz.androiddemo"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_screen)
     //   mNativeAds = ArrayList()
       // bannerAd = ArrayList()
        //initialiseInfomo()

        askforPermissions()
        infomoSDK = InfomoSDK.getInstance(this).initialize(this)
        infomoSDK!!.setLogo(R.drawable.inews_logo)
        initialiseDevice()
        //  testFunction1();
        //testFunction2();
        //  testFunction1();
        //testFunction2();
        initialisePublisher()
     //   mPublisherAdView = findViewById(R.id.publisherAdView)
       // val adRequest = PublisherAdRequest.Builder().build()
       // publisherAdView.loadAd(adRequest)





        Handler().postDelayed({
            initialiseUser()
            val netWorkCall = NetWorkCall.getInstance(applicationContext)
            //  netWorkCall.allModuleIssues("wakeUpAlarm", "waking up Alarm at 8 AM", "ERROR", "Exception", "test");
            infomoSDK!!.sync()
            //  netWorkCall.updateEventLog(InfomoConstants.KEY_ALARM_EVENT, getApplicationContext());
                netWorkCall.getContentsForNotification(getApplicationContext(), InfomoConstants.KEY_END_OF_CALL);
            // new AllApisAsyn(getApplicationContext()).execute();
        }, 10000)
//        launchHomeScreen()
        mAuth = FirebaseAuth.getInstance()
/*
        val handler = Handler()
        handler.postDelayed(Runnable {
            if (mAuth!!.currentUser!=null)
            {
                val currentUser = mAuth!!.currentUser
                updateUI(currentUser)
            }
            else
            {
                if (intent.extras != null && intent.extras.getString(ITEM_ID) != null ) {

                  *//*  val subscribe = Completable.timer(0, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                        .subscribe(this::launchFromPush)*//*
                } else {

                    *//*   val intent=Intent(this@SplashScreenActivity, Login::class.java)
                       startActivity(intent)*//*

                    val subscribe = Completable.timer(0, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                        .subscribe(this::launchHomeScreen)
                }
            }


        }, 3000)*/
       /*
       MobileAds.initialize(this)
        loadNativeAds()
*/

        /*val subscribe = Completable.timer(0, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
            .subscribe(this::launchHomeScreen)*/





    }


    private fun initialiseUser() {
        Handler().postDelayed({
            FirebaseInstanceId.getInstance().instanceId
                .addOnCompleteListener(OnCompleteListener { task ->
                    if (!task.isSuccessful) {
                        Log.w(
                            "TAG",
                            "getInstanceId failed",
                            task.exception
                        )
                        return@OnCompleteListener
                    }


                    // Get new Instance ID token
                    val token = task.result!!.token
                    LogE.printMe("token$token")
                    generatedvertisingId(token)
                    // Log and toast
                    // String msg = getString(R.string.msg_token_fmt, token);

                    //  Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                })
        }, 5000)
    }


    fun generatedvertisingId(token: String?) {
        val task: AsyncTask<Void?, Void?, String?> =
            object : AsyncTask<Void?, Void?, String?>() {


                override fun onPostExecute(s: String?) {
                    super.onPostExecute(s)
                }

                override fun doInBackground(vararg p0: Void?): String? {
                    var idInfo: AdvertisingIdClient.Info? =
                        null
                    try {
                        idInfo =
                            AdvertisingIdClient.getAdvertisingIdInfo(
                                applicationContext
                            )
                    } catch (e: GooglePlayServicesNotAvailableException) {
                        e.printStackTrace()
                    } catch (e: GooglePlayServicesRepairableException) {
                        e.printStackTrace()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                    var advertId: String? = null
                    try {
                        advertId = idInfo!!.id
                        LogE.printMe("Advertising id:$advertId")
                        val pref =
                            SharedPreference.getInstance(applicationContext)
                        if (advertId != null) pref.setString(SharedPreference.KEY_API_KEY, advertId)
                        val userInfo =
                            UserInfo.getInstance(
                                applicationContext
                            )
                        userInfo.cityName = "BLR"
                        userInfo.countryCode = "91"
                        userInfo.setAdvertisingId(advertId)
                        userInfo.countryName = "INDIA"
                        userInfo.fcmToken = token
                        userInfo.countryName = "DEV"
                        userInfo.sdkVersion = CommonUtils.getVersionName()
                        userInfo.viewerPhone = "9986173647"
                        userInfo.zipcode = "560072"
                        userInfo.countryName = "INDIA"
                        userInfo.viewerEmail = "test@123.com"
                        userInfo.continentName = "ASIA"
                        userInfo.continentCode = "123"
                        userInfo.device_email = "test@gmail.com"
                        userInfo.height = "5'7"
                        userInfo.weight = "85"
                        userInfo.location = "BLR"
                        userInfo.stateName = "KARNATAKA"
                        infomoSDK!!.saveUserConfig()
                        userInfo.registerUser("")
                    } catch (e: NullPointerException) {
                        e.printStackTrace()
                    }
                    return advertId
                }
            }
        task.execute()
    }

    private fun initialisePublisher() {
        val publisherConfig = PublisherConfig.getInstance(this)
        publisherConfig.adDisplayMode = 0
        publisherConfig.isAlarm = true
        publisherConfig.dailyAdTarget = 100
        publisherConfig.isEndofCall = true
        publisherConfig.dailyPushLimit = 100
        publisherConfig.isMaint = false
        publisherConfig.debugMode = 0
        publisherConfig.versionName = BuildConfig.VERSION_NAME
        publisherConfig.maxNotificationsPullCount = 4
        publisherConfig.notificationExpiry = 1000 * 60 * 60 * 6.toLong()
        val outsideAppZones = OutsideAppZones(this)
        outsideAppZones.templateName = InfomoConstants.KEY_ONE_INTERSTITIAL_BANNER
        outsideAppZones.notificationVideo = "849"
        outsideAppZones.notificationInterstitial = "974"
        outsideAppZones.notificationBannerTop = "993"
        outsideAppZones.immediateVideo = "914"
        outsideAppZones.notificationBannerBottom = "993"
        outsideAppZones.immediateInterstitial = "849"
        publisherConfig.outsideAppZones = outsideAppZones
        publisherConfig.isShowNotification = true
        publisherConfig.internalAdCount=2
        publisherConfig.googleAdCount=2
        publisherConfig.syncInterval = 17
        publisherConfig.notificationChannel = CommonUtils.getApplicationName(this)
        infomoSDK!!.savePublisherConfig(publisherConfig)
    }

    private fun initialiseDevice() {
        val deviceInfo = DeviceInfo.getInstance(this)
        val dInfo =
            Device_settings.getSettings(this)
        deviceInfo.appId = dInfo.app_id
        deviceInfo.carrier = dInfo.carrier
        deviceInfo.connectionType = dInfo.connection_type
        deviceInfo.dataSpeed = dInfo.data_speed
        deviceInfo.deviceType = dInfo.device_type
        deviceInfo.didMd5 = dInfo.didmd5
        deviceInfo.didsha1 = dInfo.didsha1
        deviceInfo.displayheight = dInfo.display_height
        deviceInfo.displaywidth = dInfo.display_width
        deviceInfo.isJs = true
        deviceInfo.language = dInfo.language
        deviceInfo.latitude = dInfo.user_latitude
        deviceInfo.longitude = dInfo.user_longitude
        deviceInfo.make = dInfo.make
        deviceInfo.model = dInfo.model
        deviceInfo.os = dInfo.os
        deviceInfo.deviceType = "MOBILE"
        deviceInfo.osv = dInfo.os_ver
        deviceInfo.screenheight = dInfo.screen_height
        deviceInfo.screenwidth = dInfo.screen_height
        deviceInfo.timeZone = dInfo.timezone
        deviceInfo.userAgent = dInfo.user_account_name
//        deviceInfo.autoDetect(this);
    }



    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        // super.onRequestPermissionsResult(requestCode, permissions, grantResults);
   /*     if (requestCode == 11) {
            val adSettings = SharedPreference.getInstance(applicationContext)
            val prefUpdated =
                adSettings.getBoolean(SharedPreference.KEY_PREF_UPDATED, false)
            var counter = 0
            if (!prefUpdated) {
                adSettings.setBoolean(SharedPreference.KEY_PREF_UPDATED, true)
                CommonUtils.setUserAdSelection(adSettings, this@SplashScreenActivity)
                if (permissions != null && permissions.size > 0) {
                    for (permission in permissions) {
                        if (permission == Manifest.permission.READ_PHONE_STATE && grantResults[counter] == PackageManager.PERMISSION_GRANTED
                        ) {
                            adSettings.setBoolean("phonePermissionUpdated", true)
                        }
                        counter++
                    }
                }
            } else {
                if (permissions != null && permissions.size > 0) {
                    for (permission in permissions) {
                        if (permission == Manifest.permission.READ_PHONE_STATE && grantResults[counter] == PackageManager.PERMISSION_GRANTED
                        ) {
                            adSettings.setBoolean(SharedPreference.KEY_PREF_UPDATED, true)
                            CommonUtils.setUserAdSelection(adSettings, this@SplashScreenActivity)
                            adSettings.setBoolean("phonePermissionUpdated", true)
                        }
                        counter++
                    }
                }
            }
            adSettings.setBoolean(SharedPreference.KEY_PREF_UPDATED_TO_SERVER, false)
        }*/


        val perms: MutableMap<String, Int> =
            HashMap()
        
        perms[Manifest.permission.CALL_PHONE] = PackageManager.PERMISSION_GRANTED
        perms[Manifest.permission.READ_PHONE_STATE] = PackageManager.PERMISSION_GRANTED
        perms[Manifest.permission.WRITE_EXTERNAL_STORAGE] = PackageManager.PERMISSION_GRANTED
        perms[Manifest.permission.READ_EXTERNAL_STORAGE] = PackageManager.PERMISSION_GRANTED
        perms[Manifest.permission.ACCESS_COARSE_LOCATION] = PackageManager.PERMISSION_GRANTED
        perms[Manifest.permission.ACCESS_FINE_LOCATION] = PackageManager.PERMISSION_GRANTED
        perms[Manifest.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS] =
            PackageManager.PERMISSION_GRANTED

        for (i in permissions.indices) perms[permissions[i]] = grantResults[i]
      
// Toast.makeText(this,"REQUEST_IGNORE_BATTERY_OPTIMIZATIONS"+(perms.get(Manifest.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS) == PackageManager.PERMISSION_GRANTED), Toast.LENGTH_SHORT).show();
        if (perms[Manifest.permission.CALL_PHONE] == PackageManager.PERMISSION_GRANTED && 
            perms[Manifest.permission.READ_PHONE_STATE] == PackageManager.PERMISSION_GRANTED &&
            perms[Manifest.permission.WRITE_EXTERNAL_STORAGE] == PackageManager.PERMISSION_GRANTED && 
            perms[Manifest.permission.READ_EXTERNAL_STORAGE] == PackageManager.PERMISSION_GRANTED && perms[Manifest.permission.ACCESS_COARSE_LOCATION] == PackageManager.PERMISSION_GRANTED && perms[Manifest.permission.ACCESS_FINE_LOCATION] == PackageManager.PERMISSION_GRANTED && perms[Manifest.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS] == PackageManager.PERMISSION_GRANTED
        ) { // All Permissions Granted
// Here start the activity
         //   initialiseInfomo()
          //  fcmToken = pref!!.getString(SharedPreference.FCM_SERVER_TOKEN, null)
           /* if (fcmToken == null) {
                FirebaseApp.initializeApp(this@SplashScreenActivity)
                FirebaseInstanceId.getInstance()
                    .instanceId.addOnSuccessListener { instanceIdResult ->
                    fcmToken = instanceIdResult.token
                    LogE.printMe("fcm token :$fcmToken")
                    // registerTheDevice();
                }
                FirebaseInstanceId.getInstance().instanceId.addOnFailureListener {
                    if (fcmToken == null) {
                        fcmToken = "InvalidToken"
                    }
                }
                //If it has failed this time to get the Firebase token, then it has to updated later
//Once you get it by using
//sdk.updateDeviceToken(fcmToken);
            }*/
         //   registerTheDevice()
            Log.e("permission", "permission granted")
            val app_preferences =
                applicationContext.getSharedPreferences("MyPref", 0) // 0 - for private mode
            val editor = app_preferences.edit()
            isFirstTime = app_preferences.getBoolean("isFirstTime", true)
            isRegistered = app_preferences.getBoolean("isRegistered", false)

           /* if (pref!!.getString(SharedPreference.KEY_APIKEY, null) == null) {
                val subscribe = Completable.timer(2000, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                    .subscribe(this::launchHomeScreen)
            }
            else
            {
                val subscribe = Completable.timer(0, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                    .subscribe(this::launchHomeScreen)
            }*/
            val subscribe = Completable.timer(0, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                .subscribe(this::launchHomeScreen)

        } else {

            val subscribe = Completable.timer(0, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                .subscribe(this::launchHomeScreen)
        //    initialiseInfomo()
            /*fcmToken = pref!!.getString(SharedPreference.FCM_SERVER_TOKEN, null)
            if (fcmToken == null) {
                FirebaseApp.initializeApp(this@SplashScreenActivity)
                FirebaseInstanceId.getInstance()
                    .instanceId.addOnSuccessListener { instanceIdResult ->
                    fcmToken = instanceIdResult.token
                    LogE.printMe("fcm token :$fcmToken")
                    // registerTheDevice();
                }
                FirebaseInstanceId.getInstance().instanceId.addOnFailureListener {
                    if (fcmToken == null) {
                        fcmToken = "InvalidToken"
                    }
                }
                //If it has failed this time to get the Firebase token, then it has to updated later
//Once you get it by using
//sdk.updateDeviceToken(fcmToken);
            }*/
            //registerTheDevice()
            Log.e("permission", "permission denied")
           /* val app_preferences =
                applicationContext.getSharedPreferences("MyPref", 0) // 0 - for private mode
              val editor = app_preferences.edit();
            isFirstTime = app_preferences.getBoolean("isFirstTime", true)
            isRegistered = app_preferences.getBoolean("isRegistered", false)*/
           /* if (pref!!.getString(SharedPreference.KEY_APIKEY, null) == null)
            {
                val subscribe = Completable.timer(2000, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                    .subscribe(this::launchHomeScreen)
            }
            else
            {
                val subscribe = Completable.timer(0, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                    .subscribe(this::launchHomeScreen)
            }*/

        }
    }

    private fun askforPermissions() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1 /*&& apiKey != null*/) {
            val allpermissions =
                ArrayList<String>()
            allpermissions.add(Manifest.permission.CALL_PHONE)
            allpermissions.add(Manifest.permission.READ_PHONE_STATE)
          //  allpermissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            allpermissions.add(Manifest.permission.ACCESS_COARSE_LOCATION)
            allpermissions.add(Manifest.permission.ACCESS_FINE_LOCATION)
            allpermissions.add(Manifest.permission.ACCESS_WIFI_STATE)
            //allpermissions.add(Manifest.permission.READ_EXTERNAL_STORAGE)
            allpermissions.add(Manifest.permission.ACCESS_COARSE_LOCATION)
            allpermissions.add(Manifest.permission.ACCESS_FINE_LOCATION)
            allpermissions.add(Manifest.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS)
            val requestpermissions =
                ArrayList<String>()
            for (permission in allpermissions) {
                if (ContextCompat.checkSelfPermission(
                        applicationContext,
                        permission
                    ) === PackageManager.PERMISSION_DENIED
                ) {
                    requestpermissions.add(permission)
                }
            }
            if (requestpermissions.size > 0) {
                ActivityCompat.requestPermissions(this, requestpermissions.toTypedArray(), 11)
            } else {
               /* val prefUpdated: Boolean =
                    pref!!.getBoolean(SharedPreference.KEY_PREF_UPDATED, false)
                if (!prefUpdated) {
                    pref!!.setBoolean(SharedPreference.KEY_PREF_UPDATED, true)
                    CommonUtils.setUserAdSelection(pref, this@SplashScreenActivity)
                }*/
            }
        } else {
           /* val prefUpdated: Boolean =
                pref!!.getBoolean(SharedPreference.KEY_PREF_UPDATED, false)
            if (!prefUpdated) {
                pref!!.setBoolean(SharedPreference.KEY_PREF_UPDATED, true)
                CommonUtils.setUserAdSelection(pref, this@SplashScreenActivity)
            }*/
        }

    }

   /* private fun registerTheDevice() {
        if (pref!!.getString(SharedPreference.KEY_APIKEY, null) == null) {
            deviceID = Settings.Secure.getString(
                contentResolver,
                Settings.Secure.ANDROID_ID
            )
            val handler = Handler()
            handler.postDelayed({
                infomoSDK1!!.register(
                    deviceID,
                    fcmToken
                )
            }, 3000)
            *//*
            if (infomoSDK != null)
                infomoSDK.showAd();*//*
        }
        else {
            val handler = Handler()
            handler.postDelayed({
                infomoSDK1!!.sync()
              //  infomoSDK1!!.getAd("450X450", 1, this)
                //infomoSDK1!!.getAd("320X50", 3, this)
            }, 1000)
        }
        CommonUtils.enableDebugMode(this@SplashScreenActivity)


    }*/


  /*  private fun initialiseInfomo() {

        deviceID = Settings.Secure.getString(
            contentResolver,
            Settings.Secure.ANDROID_ID
        )
        infomoSDK1 = InfomoSDK.instance().init(applicationContext)
        *//*pid 13*//*infomoSDK1!!.setPublisherKey("17a75d7742bcc60b207c178e4eeeec4714b3c767")
        //infomoSDK1.initAppLaunch(SplashScreen.this);
*//*pid 10*//* //  infomoSDK1.setPublisherKey("254e16e0130696a8fd5575454a36cad3dd806135");
*//*pid *//* //        infomoSDK1.setPublisherKey("6785b4f504d91be46575549d8bbba502cfe77942");
// infomoSDK1.setPublisherKey("25b870c18a48612fa7fdc6498d2d0bd978e8e3fd");
// infomoSDK1.setPublisherKey("25b870c18a48612fa7fdc6498d2d0bd978e8e3fd");
//  infomoSDK1.setPublisherKey("6785b4f504d91be46575549d8bbba502cfe77942");
// infomoSDK1.setPublisherKey("9bd09a0db59721aa0719f41894cf2fa697465884");
*//*pid11*//* // infomoSDK1.setPublisherKey("6785b4f504d91be46575549d8bbba502cfe77942");
*//*pid 8*//* //  infomoSDK1.setPublisherKey("946539b707c39dbe4f76d9f59e36e18e269a0a2e");
*//*blanja*//* //  infomoSDK1.setPublisherKey("eccfce76409d2f7579b1ec6f87d63d4a2a6afb1b");
//  pref.getEditor().clear().commit();
//pref.getEditor().remove(SharedPreference.KEY_API_ENDPOINTS).commit();
        infomoSDK1!!.setDomain(InfomoConstants.INFOMO_URL.INFOMO_INDONESIA *//*, SplashScreen.this*//*)
        pref = SharedPreference.getInstance(this@SplashScreenActivity)
        infomoSDK1!!.checkAppUpdateAndRegistration()
        infomoSDK1!!.setOperationCompleteCallback(this)
    }*/


    private fun launchFromPush() {
        val homeIntent = Intent(this, HomeScreenActivity::class.java)
        homeIntent.putExtra(IS_FROM_PUSH, true)
        homeIntent.putExtra(ITEM_ID, intent.extras.getString(ITEM_ID))
        homeIntent.putExtra(IS_FROM_PUSH, intent.extras.getString(PUSH_NOTIFICATION_TYPE) as String)
        startActivity(intent)
    }

    private fun launchHomeScreen() {


        intent.extras
        val intent = Intent(this, HomeScreenActivity::class.java)
        if (intent.extras != null)
            intent.putExtras(intent.extras)

        val bundle=Bundle()
      //  bundle.putSerializable("mNativeAds", mNativeAds as Serializable)
      //  bundle.putSerializable("bannerAd", bannerAd as Serializable)
        intent.putExtra("BUNDLE",bundle);
        startActivity(intent)
        finish()
//        FirebaseMessaging.getInstance().isAutoInitEnabled = true

    }
    fun updateUI(user: FirebaseUser?){
        if(user != null){
            //Do your Stuff
            Toast.makeText(this,"Hello ${user.displayName}", Toast.LENGTH_LONG).show()
            val subscribe = Completable.timer(0, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                .subscribe(this::launchHomeScreen)
        }
    }
}