package com.aimright.admin.demoapp.activity

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.widget.Toast
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.signin.GoogleSignInResult
import com.google.android.gms.common.SignInButton
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.*
import com.aimright.admin.demoapp.R
import com.aimright.admin.demoapp.ui.HomeScreenActivity
import com.aimright.admin.demoapp.ui.IS_FROM_PUSH
import com.aimright.admin.demoapp.ui.ITEM_ID
import com.aimright.admin.demoapp.ui.PUSH_NOTIFICATION_TYPE
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_register_all.*
import java.security.MessageDigest
import java.util.concurrent.TimeUnit

class RegisterAll : AppCompatActivity() {
    private var callbackManager: CallbackManager? = null
    private lateinit var signInButton: SignInButton
    //Google Login Request Code
    private val RC_SIGN_IN = 7
    //Google Sign In Client
    private lateinit var mGoogleSignInClient: GoogleSignInClient
    //Firebase Auth
    private lateinit var mAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_all)
        back.setOnClickListener { finish() }
        sign_up_email.setOnClickListener {
            val intent= Intent(this, Register::class.java)
            startActivity(intent)
        }
        mAuth = FirebaseAuth.getInstance()

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken("974817153880-snrvqtm6gjiubko2443n8mk53ohd36cg.apps.googleusercontent.com")
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this,gso)
      /*  sign_up_google.setOnClickListener {
            signIn()
        }*/
        val sharedPreferences = getSharedPreferences("Reg", Context.MODE_PRIVATE);
        val  uName = sharedPreferences.getBoolean("isFacebookLogged", false);

        if (uName==false)
        {
            LoginManager.getInstance().logOut()
        }
        printKeyHash()
        singin.setOnClickListener {
            val intent=Intent(this, Login::class.java)
            startActivity(intent)
        }

    }

    private fun signIn() {
        val signInIntent = mGoogleSignInClient.getSignInIntent()
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }



    private fun printKeyHash() {

        val info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES)


        for (signature in info.signatures) {
            val md = MessageDigest.getInstance("SHA");
            md.update(signature.toByteArray());
            val hashKey =  String(Base64.encode(md.digest(), 0));
            Log.i("test", "printHashKey() Hash Key: " + hashKey);
        }




        callbackManager = CallbackManager.Factory.create()

    //    sign_up_facebook.setReadPermissions("email", "public_profile")

       // val sign_upfacebook=findViewById<>(R.id.sign_up_facebook)

        sign_up_facebook.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                Log.d("test", "facebook:onSuccess:$loginResult")
                //profilePic= Profile.getCurrentProfile().getProfilePictureUri(200, 200).toString()
                //  userName=Profile.getCurrentProfile().firstName+" "+Profile.getCurrentProfile().lastName


                //  handleFacebookAccessToken(loginResult.accessToken)
                val  sharedPreferences = getApplicationContext().getSharedPreferences("Reg", 0);
                val   editor = sharedPreferences.edit();
                editor.putBoolean("isFacebookLogged",true);
                editor.commit();
                launchHomeScreen()
            }

            override fun onCancel() {
                Log.d("test", "facebook:onCancel")
                // ...
            }

            override fun onError(error: FacebookException) {
                Log.d("test", "facebook:onError", error)
                // ...
            }
        })
       /* LoginManager.getInstance().registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult?) {
                launchHomeScreen()


            }

            override fun onCancel() {

             }

            override fun onError(error: FacebookException?) {

            }

});*/


     /*   sign_up_facebook.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                Log.d("test", "facebook:onSuccess:$loginResult")
                //  handleFacebookAccessToken(loginResult.accessToken)
                launchHomeScreen()
            }

            override fun onCancel() {
                Log.d("test", "facebook:onCancel")
                // ...
            }

            override fun onError(error: FacebookException) {
                Log.d("test", "facebook:onError", error)
                // ...
            }
        })*/
// ..
    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        // Pass the activity result back to the Facebook SDK
        callbackManager!!.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {

            val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            handleSignInResult(result)


            /*val task = GoogleSignIn.getSignedInAccountFromIntent(data)

            try {
                // Google Sign In was successful, authenticate with Firebase
                val account = task.getResult(ApiException::class.java)
                if (account != null) {
                    firebaseAuthWithGoogle(account)
                }
            } catch (e: ApiException) {
                // Google Sign In failed, update UI appropriately
                Log.w("Login", "Google sign in failed", e)
                // ...
            }*/

        }
    }

    private fun handleSignInResult(result: GoogleSignInResult) {
        if (result.isSuccess) {
            val account = result.signInAccount
            val idToken = account!!.idToken
            val name = account.displayName
            val email = account.email
            // you can store user data to SharedPreference
            val credential = GoogleAuthProvider.getCredential(idToken, null)
            firebaseAuthWithGoogle(credential)
        } else {
            // Google Sign In failed, update UI appropriately
            Log.e("test", "Login Unsuccessful. $result")
            Toast.makeText(this, "Login Unsuccessful", Toast.LENGTH_SHORT).show()
        }
    }

    /*private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {
        Log.d("Login", "firebaseAuthWithGoogle:" + acct.id!!)

        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
        mAuth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("Login", "signInWithCredential:success")
                    val user = mAuth.currentUser
                    updateUI(user)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("Login", "signInWithCredential:failure", task.exception)
                    Toast.makeText(this,"Auth Failed",Toast.LENGTH_LONG).show()
                    updateUI(null)
                }

                // ...
            }
    }*/

    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
    }

    private fun firebaseAuthWithGoogle(credential: AuthCredential) {

        mAuth.signInWithCredential(credential)
            .addOnCompleteListener(this,
                OnCompleteListener<AuthResult> { task ->

                    if (task.isSuccessful) {
                        if (intent.extras != null && intent.extras.getString(ITEM_ID) != null ) {
//            launchHomeScreen()
//            launchFromPush()
                            val subscribe = Completable.timer(0, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                                .subscribe(this::launchFromPush)
                        } else {


                            val subscribe = Completable.timer(0, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                                .subscribe(this::launchHomeScreen)
                        }

                    }
                    else {
                        Log.w(
                            "test",
                            "signInWithCredential" + task.exception!!.message
                        )
                        task.exception!!.printStackTrace()
                        Toast.makeText(
                            this, "Authentication failed.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                })
    }


    private fun launchFromPush() {
        val homeIntent = Intent(this, HomeScreenActivity::class.java)
        homeIntent.putExtra(IS_FROM_PUSH, true)
        homeIntent.putExtra(ITEM_ID, intent.extras.getString(ITEM_ID))
        homeIntent.putExtra(IS_FROM_PUSH, intent.extras.getString(PUSH_NOTIFICATION_TYPE) as String)
        startActivity(intent)
    }

    private fun launchHomeScreen() {
        intent.extras
        val intent = Intent(this, HomeScreenActivity::class.java)
        if (intent.extras != null)
            intent.putExtras(intent.extras)
        startActivity(intent)
        finish()
//        FirebaseMessaging.getInstance().isAutoInitEnabled = true

    }

    override fun onStart() {
        super.onStart()
// Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = mAuth.currentUser
        //userName=currentUser.toString()
        updateUI(currentUser)
    }

    override fun onStop() {
        super.onStop()

    }
    fun updateUI(user: FirebaseUser?){
        if(user != null){
            //Do your Stuff
            Toast.makeText(this,"Hello ${user.displayName}",Toast.LENGTH_LONG).show()
        }
    }
}
