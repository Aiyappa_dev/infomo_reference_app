package com.aimright.admin.demoapp.ui

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.Toast
import com.aimright.admin.demoapp.R
import com.aimright.admin.demoapp.base.BaseActivity
import com.aimright.admin.demoapp.io.dto.HomeScreenResponseDto
import com.aimright.admin.demoapp.ui.paging.NewsPagingListFragment
import com.aimright.admin.infomosdk.djax.adserver.ErrorCodes
import com.aimright.admin.infomosdk.djax.adserver.MInterstitial_Interstitial
import com.aimright.admin.infomosdk.interfaces.InterstitialImageAdListener


const val TAG = "Push"

class HomeScreenActivity : BaseActivity(), CategoryFragment.OnFragmentInteractionListener,
    InterstitialImageAdListener/*, NotifyOperationComplete */{

   // private var mNativeAds = ArrayList<AdResponse>()
   // private var bannerAd = ArrayList<AdResponse>()
    // private  var adList=ArrayList<UnifiedNativeAd>()
    var newsData: HomeScreenResponseDto? = null
   // private lateinit var infomoSDK: InfomoSDK
  //  private var pref: SharedPreference? = null
    private var fcmToken: String? = null
    private var doubleBackToExitPressedOnce = false
    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }

        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show()

        Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
    }

    override fun onFragmentInteraction(uri: Uri) {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       setContentView(R.layout.baseactivity)
      //  var infomoSDK1 = InfomoSDK.instance().init(this)
        val make = Build.MANUFACTURER
        val model = Build.MODEL
        val makeModel = "$make $model"
        Log.e("models:",makeModel)

        val intent = intent
        val args = intent.getBundleExtra("BUNDLE")
        //mNativeAds=args.getSerializable("mNativeAds") as ArrayList<AdResponse>
        //bannerAd=args.getSerializable("bannerAd") as ArrayList<AdResponse>

   //      infomoSDK1!!.getAd("450X450", 2, this)
     //   infomoSDK1!!.getAd("320X50", 3, this)
       /* val wm = application.getSystemService(Context.WIFI_SERVICE) as WifiManager
        val wifiinfo: WifiInfo = wm.getConnectionInfo()
        val myIPAddress: ByteArray =
            BigInteger.valueOf(wifiinfo.ipAddress.toLong()).toByteArray()
// you must reverse the byte array before conversion. Use Apache's commons library
        // you must reverse the byte array before conversion. Use Apache's commons library
     //   ByteArray.reverse(myIPAddress)
        myIPAddress.reverse()
        val myInetIP: InetAddress = InetAddress.getByAddress(myIPAddress)
        val myIP: String = myInetIP.getHostAddress()

        val ipInt = wifiinfo.getIpAddress()*/
     /*   val ip = InetAddress.getByAddress(
                ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(ipInt).array())
            .getHostAddress()
        */
       // val ip:String=Utills.getLocalIpAddress()

        Utills.getMACAddress("wlan0")
        Utills.getMACAddress("eth0")
        val ip =   Utills.getIPAddress(true) // IPv4
        Utills.getIPAddress(false) // IPv6
      //  infomoSDK1.getDeviceAtlas(ip, makeModel, this)

        /*https://q7u8qcubw5.execute-api.ap-southeast-1.amazonaws.com/default/deviceAtlasEvaluation */
        /*{ "deviceIp": "x.x.x.x", "makeModel": "samsuing SM-902" }*/
       // Handler().postDelayed({infomoSDK1.getAd("320X50", 3, this)}, 2000)
      /*  val bundle1 = intent.extras
         adList = bundle1.getSerializable("mNativeAds") as ArrayList<UnifiedNativeAd>
        */


        handleOnCreate()

        //   initialiseInfomo()
     //   askforPermissions()
        //registerTheDevice()

    }

    private fun handleOnCreate() {
        val isFromPush = intent.getBooleanExtra(IS_FROM_PUSH, false)
        val itemId = intent.getStringExtra(ITEM_ID) ?: ""
        val pushNotificationType = intent.getStringExtra(PUSH_NOTIFICATION_TYPE) ?: ""

        val beginTransaction = supportFragmentManager.beginTransaction()
        beginTransaction.replace(
            R.id.fragment_container, HomeScreenPagerFragment.newInstance(
                isFromPush,
                itemId, pushNotificationType//,
                /*mNativeAds*//*, bannerAd*/

            )
        )
        beginTransaction.commit()
    }

/*    private fun initialiseInfomo() {
        infomoSDK = InfomoSDK.instance().init(applicationContext)
     //   infomoSDK.setPublisherKey("be366bb25b6a3436f076375fbc97055f7f84b782")
        infomoSDK.setPublisherKey("6785b4f504d91be46575549d8bbba502cfe77942")
        infomoSDK.setDomain(InfomoConstants.INFOMO_URL.INFOMO_INDONESIA)
        pref = SharedPreference.getInstance(applicationContext)
        infomoSDK.checkAppUpdateAndRegistration()
       // infomoSDK!!.showAd()

    }

    private fun registerTheDevice() {
        if (pref!!.getString(SharedPreference.KEY_APIKEY, null) == null) {
            val deviceID = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
            Handler().postDelayed({
                infomoSDK.register(deviceID, fcmToken) }, 3000)
        } else {
            infomoSDK.sync()

            Handler().postDelayed({
                if (infomoSDK != null)
                    infomoSDK.showAd()
            }, 5000)

        }
        CommonUtils.enableDebugMode(applicationContext)
    }



    fun askforPermissions() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1 && apiKey != null) {
            val allpermissions = ArrayList<String>()
            allpermissions.add(android.Manifest.permission.CALL_PHONE)
            allpermissions.add(android.Manifest.permission.READ_PHONE_STATE)
            allpermissions.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
            allpermissions.add(android.Manifest.permission.READ_EXTERNAL_STORAGE)
            allpermissions.add(android.Manifest.permission.ACCESS_COARSE_LOCATION)
            allpermissions.add(android.Manifest.permission.ACCESS_FINE_LOCATION)
            allpermissions.add(android.Manifest.permission.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS)

            val requestpermissions = ArrayList<String>()

            for (permission in allpermissions) {
                if (ContextCompat.checkSelfPermission(
                        applicationContext,
                        permission
                    ) == PackageManager.PERMISSION_DENIED
                ) {
                    requestpermissions.add(permission)
                }
            }
            if (requestpermissions.size > 0) {
                ActivityCompat.requestPermissions(this, requestpermissions.toTypedArray(), 11)
            } else {
                val prefUpdated = pref!!.getBoolean(SharedPreference.KEY_PREF_UPDATED, false)
                if (!prefUpdated) {
                    pref!!.setBoolean(SharedPreference.KEY_PREF_UPDATED, true)
                    CommonUtils.setUserAdSelection(pref, this@HomeScreenActivity)
                }
            }
        } else {
            val prefUpdated = pref!!.getBoolean(SharedPreference.KEY_PREF_UPDATED, false)
            if (!prefUpdated) {
                pref!!.setBoolean(SharedPreference.KEY_PREF_UPDATED, true)
                CommonUtils.setUserAdSelection(pref, this@HomeScreenActivity)
            }
        }

        fcmToken = pref!!.getString(SharedPreference.FCM_SERVER_TOKEN, null)
        if (fcmToken == null) {
            FirebaseApp.initializeApp(applicationContext)
            FirebaseInstanceId.getInstance().instanceId
                .addOnSuccessListener { instanceIdResult -> fcmToken = instanceIdResult.token
                    registerTheDevice()
                }
                .addOnFailureListener { instanceIdResult ->
                    if (fcmToken == null) {
                        fcmToken = "InvalidToken"
                    }

                    //If it has failed this time to get the Firebase token, then it has to updated later
                    //Once you get it by using
                    //sdk.updateDeviceToken(fcmToken);
                }
        }
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode === 11) {
            val adSettings = SharedPreference.getInstance(applicationContext)
            val prefUpdated = adSettings.getBoolean(SharedPreference.KEY_PREF_UPDATED, false)
            var counter = 0
            if (!prefUpdated) {
                adSettings.setBoolean(SharedPreference.KEY_PREF_UPDATED, true)
                CommonUtils.setUserAdSelection(adSettings, this@HomeScreenActivity)
                if (permissions != null && permissions.size > 0) {
                    for (permission in permissions) {
                        if (permission == android.Manifest.permission.READ_PHONE_STATE && grantResults[counter] === PackageManager.PERMISSION_GRANTED) {
                            adSettings.setBoolean("phonePermissionUpdated", true)
                        }
                        counter++
                    }
                }
            } else {
                if (permissions != null && permissions.size > 0) {
                    for (permission in permissions) {
                        if (permission == android.Manifest.permission.READ_PHONE_STATE && grantResults[counter] === PackageManager.PERMISSION_GRANTED) {
                            adSettings.setBoolean(SharedPreference.KEY_PREF_UPDATED, true)
                            CommonUtils.setUserAdSelection(
                                adSettings,
                                this@HomeScreenActivity
                            )
                            adSettings.setBoolean("phonePermissionUpdated", true)
                        }
                        counter++
                    }
                }
            }

            adSettings.setBoolean(SharedPreference.KEY_PREF_UPDATED_TO_SERVER, false)
        }

    }*/




    fun setData(homeScreenResponseDto: HomeScreenResponseDto) {
        this.newsData = homeScreenResponseDto
    }

    fun replaceDetailFragment(position: Int) {
        val beginTransaction = supportFragmentManager.beginTransaction()
        beginTransaction.add(R.id.fragment_container, HomeScreenDetailPagerFragment.newInstance(position))
        beginTransaction.addToBackStack("tag")
        beginTransaction.commit()
    }

    fun replaceNewsPaginationFragment(categoryId: String) {
        val beginTransaction = supportFragmentManager.beginTransaction()
        beginTransaction.add(R.id.fragment_container, NewsPagingListFragment.newInstance(categoryId, false))
        beginTransaction.addToBackStack("tag")
        beginTransaction.commit()
    }


    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        this.intent = intent
      //  handleOnCreate()
    }

   /* override fun onOperationComplete(s: String?, response: Any?, i: Int) {
        if (i == 200 && response != null) {

            if (s.equals("450X450"))
            {
              //  mNativeAds.addAll(response as Collection<AdResponse>)
             //   mNativeAdsStatic.addAll(response as Collection<AdResponse>)
            }
            else if (s.equals("320X50")) {
                //bannerAd.addAll(response as Collection<AdResponse>)
                bannerAdStatic.addAll(response as Collection<AdResponse>)
            }
          *//*  if (mNativeAds.size>0 && bannerAd.size>0)
            {
                val subscribe = Completable.timer(0, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                    .subscribe(this::launchHomeScreen)

            }*//*

            //  handleOnCreate()
        }
        else
        {


        }

       *//* if (i == 200 && response != null) {

            if (s.equals("320X90"))
            {

                mNativeAds = ArrayList()
                mNativeAds.addAll(response as Collection<AdResponse>)
            }
         else if (s.equals("320X50")) {
                bannerAd = ArrayList()
                bannerAd.addAll(response as Collection<AdResponse>)
            }
            else
            {
                data=s.toString()
            }
          //  handleOnCreate()
        }*//*

    }*/

    companion object {
        var data=""
       // var mNativeAdsStatic = ArrayList<AdResponse>()
        // var bannerAdStatic = ArrayList<AdResponse>()
    }

    override fun onInterstitialAdShown(p0: MInterstitial_Interstitial?) {

    }

    override fun onInterstitialAdLoaded(p0: MInterstitial_Interstitial?) {
        TODO("Not yet implemented")
    }

    override fun onInterstitialAdDismissed(p0: MInterstitial_Interstitial?) {
        TODO("Not yet implemented")
    }

    override fun onInterstitialAdClicked(p0: MInterstitial_Interstitial?) {
        TODO("Not yet implemented")
    }

    override fun onInterstitialAdFailed(p0: MInterstitial_Interstitial?, p1: ErrorCodes?) {
        TODO("Not yet implemented")
    }


}