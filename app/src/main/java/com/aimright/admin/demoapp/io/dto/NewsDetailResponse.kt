package com.aimright.admin.demoapp.io.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class NewsDetailResponse(
    @Expose
    @SerializedName("status")
    var status: Boolean,
    @Expose
    @SerializedName("data")
    var data: NewsDetailDataEntity
)

data class NewsDetailDataEntity(
    @Expose
    @SerializedName("id")
    var id: Int,
    @Expose
    @SerializedName("news_title")
    val newsTitle: String,
    @Expose
    @SerializedName("highlights")
    val highlights: String,
    @Expose
    @SerializedName("news_body")
    val newsBody: String,


    @Expose
    @SerializedName("video_body")
    val videoBody:String,

    @Expose
    @SerializedName("tags_name")
    var tagsName: List<String>,
    @Expose
    @SerializedName("category")
    var categoryEntity: List<CategoryEntity>,
    @Expose
    @SerializedName("featured")
    var featured: Boolean,
    @Expose
    @SerializedName("district")
    var districtEntity: List<NewsDetailDistrictEntity>,
    @Expose
    @SerializedName("breaking_news")
    var breakingNews: Boolean,
    @Expose
    @SerializedName("is_visible")
    var isVisible: Boolean,
    @Expose
    @SerializedName("time_stamp")
    val timeStamp: String,
    @Expose
    @SerializedName("is_recommended")
    var isRecommended: Boolean,
    @Expose
    @SerializedName("photos")
    var photos: List<String?>,
    @Expose
    @SerializedName("thumbnails")
    var thumbnails: List<String>,
    @Expose
    @SerializedName("videos")
    var videos: List<String>,
    @Expose
    @SerializedName("share_url")
    var shareUrl: String = ""
)

data class NewsDetailDistrictEntity(
    @Expose
    @SerializedName("id")
    var id: Int,
    @Expose
    @SerializedName("district_name")
    val districtName: String,
    @Expose
    @SerializedName("district_order")
    var districtOrder: Int,
    @Expose
    @SerializedName("is_visible")
    var isVisible: Boolean
)

data class CategoryEntity(
    @Expose
    @SerializedName("id")
    var id: Int,
    @Expose
    @SerializedName("category_name")
    val categoryName: String,
    @Expose
    @SerializedName("icon_url_greyscale")
    val iconUrlGreyscale: String,
    @Expose
    @SerializedName("icon_url_white")
    val iconUrlWhite: String,
    @Expose
    @SerializedName("is_visible")
    var isVisible: Boolean,
    @Expose
    @SerializedName("category_order_number")
    var categoryOrderNumber: Int,
    @Expose
    @SerializedName("subcategory")
    var subcategory: List<String>,
    @Expose
    @SerializedName("is_special")
    var isSpecial: Boolean
)