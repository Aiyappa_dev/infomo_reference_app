package com.aimright.admin.demoapp.ui

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.TextView
import com.aimright.admin.demoapp.R
import com.aimright.admin.demoapp.activity.DistrictList
import com.aimright.admin.demoapp.io.dto.NotificationDataEntity1
import java.util.*
import kotlin.collections.ArrayList

class DistrictAdapter(/*
    private val notificationResponseResponseDto: District,*/
    private val context: Context?,
    private val arraylist1: ArrayList<NotificationDataEntity1>
) :
    RecyclerView.Adapter<DistrictAdapter.NotificationViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationViewHolder {
        return NotificationViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.district_list,
                parent,
                false
            )
        )
    }

    private val arraylist: ArrayList<NotificationDataEntity1>
    init {
        this.arraylist = ArrayList()
        this.arraylist.addAll(arraylist1)

    }

    override fun getItemCount(): Int {
        return arraylist1.size
    }

    override fun onBindViewHolder(viewHolder: NotificationViewHolder, position: Int) {
        viewHolder.titleTextView.text = arraylist1.get(position).pushTitle
        val id=arraylist1.get(position).id
        viewHolder.itemView.setOnClickListener {
            loadDistrict(id)
        }
        viewHolder.titleTextView.setOnClickListener {
            loadDistrict(id)
        }
        viewHolder.radioButton.setOnClickListener{
            loadDistrict(id)
        }

    }
    private fun loadDistrict(id: Int)
    {
        val intent=Intent(context, DistrictList::class.java )
        intent.putExtra("id",id)
        context!!.startActivity(intent)

    }

    // Filter Class
    fun filter(charText: String) {
        var charText = charText
        charText = charText.toLowerCase(Locale.getDefault())
        arraylist1.clear()
        if (charText.length == 0) {
            arraylist1.addAll(arraylist)
        } else {
            for (wp in arraylist) {
                if (wp.pushTitle.toLowerCase(Locale.getDefault()).contains(charText)) {
                    arraylist1.add(wp)
                }
            }
        }
        notifyDataSetChanged()
    }

    inner class NotificationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val titleTextView = itemView.findViewById(R.id.district_name) as TextView
        val radioButton=itemView.findViewById<RadioButton>(R.id.district_type)
       // val decTextView = itemView.findViewById(R.id.notificationDescTv) as TextView
       // val notificationIv = itemView.findViewById(R.id.notificationIv) as ImageView

    }

}
