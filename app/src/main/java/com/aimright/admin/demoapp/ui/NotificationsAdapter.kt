package com.aimright.admin.demoapp.ui

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.aimright.admin.demoapp.R
import com.aimright.admin.demoapp.model.Results
import com.squareup.picasso.Picasso
class NotificationsAdapter(private val notificationResponseResponseDto: Array<Results>) :
    RecyclerView.Adapter<NotificationsAdapter.NotificationViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationViewHolder {
        return NotificationViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.notification_list_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return notificationResponseResponseDto.size
    }

    override fun onBindViewHolder(viewHolder: NotificationViewHolder, position: Int) {
        viewHolder.decTextView.text = notificationResponseResponseDto.get(position).news_body//dataEntity[position].pushMessage
        viewHolder.titleTextView.text = notificationResponseResponseDto.get(position).news_title/*dataEntity[position].pushTitle*/
        if(notificationResponseResponseDto.get(position).thumbnails[0].isNotEmpty())
            Picasso.with(viewHolder.itemView.context)
                .load(notificationResponseResponseDto.get(position).thumbnails[0])
                .fit()
                .centerCrop()
                .placeholder(R.drawable.ic_more_horiz_grey)
                .into(viewHolder.notificationIv)
        viewHolder.itemView.setOnClickListener {
            val intent = Intent(viewHolder.itemView.context, NewsDetailActivity::class.java)
            intent.putExtra(ITEM_TYPE, "news")
            intent.putExtra(ARTICLE_ID, notificationResponseResponseDto.get(position).id)
            viewHolder.itemView.context.startActivity(intent)
        }
    }


    inner class NotificationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val titleTextView = itemView.findViewById(R.id.notificationTitleTv) as TextView
        val decTextView = itemView.findViewById(R.id.notificationDescTv) as TextView
        val notificationIv = itemView.findViewById(R.id.notificationIv) as ImageView

    }

}
