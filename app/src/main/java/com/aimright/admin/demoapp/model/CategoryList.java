package com.aimright.admin.demoapp.model;

public class CategoryList {
    private DataCategory[] data;

    private String status;

    public DataCategory[] getData ()
    {
        return data;
    }

    public void setData (DataCategory[] data)
    {
        this.data = data;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [data = "+data+", status = "+status+"]";
    }
}
