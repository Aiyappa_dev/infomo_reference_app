package com.aimright.admin.demoapp.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.aimright.admin.demoapp.R
import kotlinx.android.synthetic.main.activity_register.*
import android.content.Intent
import com.google.firebase.auth.AuthResult

import android.support.annotation.NonNull
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task





class Register : AppCompatActivity() {

    private var auth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        //Get Firebase auth instance
        auth = FirebaseAuth.getInstance()

        back.setOnClickListener {
            finish()
        }

        sign_in.setOnClickListener {
            startActivity(Intent(this@Register, Login::class.java))
        }
        signUp()


    }

    private fun signUp() {

        sign_up.setOnClickListener {
            val username1=username.text.toString()
            val email1 = email.getText().toString().trim()
            val password1 = pwd.getText().toString().trim()

            if (TextUtils.isEmpty(username1)) {
                username.setError("Please Enter username")
               // Toast.makeText(getApplicationContext(), "Please Enter username", Toast.LENGTH_SHORT).show();
                return@setOnClickListener;
            }
            if (TextUtils.isEmpty(email1)) {
                email.setError("Enter email address!")
                //Toast.makeText(getApplicationContext(), "Enter email address!", Toast.LENGTH_SHORT).show();
                return@setOnClickListener;
            }

            if (TextUtils.isEmpty(password1)) {
                pwd.setError("Enter password!")
                //Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show();
                return@setOnClickListener;
            }

            if (password1.length < 6) {
                pwd.setError("Password too short, enter minimum 6 characters!")

               // Toast.makeText(getApplicationContext(), "Password too short, enter minimum 6 characters!", Toast.LENGTH_SHORT).show();
                return@setOnClickListener;
            }
            progressBar.setVisibility(View.VISIBLE)

            //create user
            auth!!.createUserWithEmailAndPassword(email1, password1)
                .addOnCompleteListener(
                    this@Register,
                    object : OnCompleteListener<AuthResult> {
                        override fun onComplete(@NonNull task: Task<AuthResult>) {
                            Toast.makeText(
                                this@Register,
                                "createUserWithEmail:onComplete:" + task.isSuccessful(),
                                Toast.LENGTH_SHORT
                            ).show()
                            progressBar.visibility = View.GONE
                            // If sign in fails, display a message to the user. If sign in succeeds
                            // the auth state listener will be notified and logic to handle the
                            // signed in user can be handled in the listener.
                            if (!task.isSuccessful()) {
                                Toast.makeText(
                                    this@Register,
                                    "Authentication failed." + task.getException(),
                                    Toast.LENGTH_SHORT
                                ).show()
                            } else {
                                startActivity(Intent(this@Register, Login::class.java))
                                finish()
                            }
                        }
                    })

        }
    }

    override fun onResume() {
        super.onResume()
        progressBar.visibility = View.GONE
    }
}
