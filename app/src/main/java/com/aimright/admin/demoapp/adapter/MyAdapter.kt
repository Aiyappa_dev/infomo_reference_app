package com.aimright.admin.demoapp.adapter

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.aimright.admin.demoapp.io.dto.CategoryDataEntity
import com.aimright.admin.demoapp.ui.CategoryFragment

class MyAdapter(
    fm: FragmentManager,
    internal var totalTabs: Int,
    internal val categoriesList: ArrayList<CategoryDataEntity>,
    /*internal val adList: java.util.ArrayList<AdResponse>,*/
    internal val context: Context?//,
  // internal val bannerAd: java.util.ArrayList<AdResponse>
) : FragmentPagerAdapter(fm) {

    // this is for fragment tabs
    override fun getItem(position: Int): Fragment? {
        if (categoriesList.get(position).categoryName.equals("BUSINESS", true) || categoriesList.get(position).categoryName.equals("CULTURE", true) )
        {
         //   val infomoSDK = InfomoSDK.instance().init(context)
           // infomoSDK.showAd()

        }
              return  CategoryFragment.newInstance(
            categoriesList.get(position).fetchUrl,
            categoriesList.get(position).categoryType.toString()//,
            /*adList*//*, bannerAd*/
        )
//915339095653923
        /* when (position) {
            0 -> {
                //  val homeFragment: HomeFragment = HomeFragment()
                return HomeFragment()
            }
            1 -> {
                return SportFragment()
            }
            2 -> {
                // val movieFragment = MovieFragment()
                return MovieFragment()
            }
            else -> return null
        }*/
    }

    // this counts total number of tabs
    override fun getCount(): Int {
        return totalTabs
    }
}