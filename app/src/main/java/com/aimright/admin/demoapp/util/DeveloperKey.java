// Copyright 2014 Google Inc. All Rights Reserved.

package com.aimright.admin.demoapp.util;

/**
 * Static container class for holding a reference to your YouTube Developer Key.
 */
public class DeveloperKey {

  /**
   * Please replace this with a valid API key which is enabled for the
   * YouTube Data API v3 service. Go to the
   * <a href="https://console.developers.google.com/">Google Developers Console</a>
   * to register a new developer key.
   */
  public static final String DEVELOPER_KEY = "AIzaSyBrrs6Jv8LENjUj_sdXsLdh629iJv9X6Fw";

}
