package com.aimright.admin.demoapp.ui

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.v4.view.ViewPager
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.aimright.admin.demoapp.base.BaseFragment
import com.aimright.admin.demoapp.io.RetrofitApiServiceBuilder
import com.aimright.admin.demoapp.io.dto.CATEGORY_HYBRID_TYPE
import com.aimright.admin.demoapp.io.dto.CategoryResponseVideosEntity
import com.aimright.admin.demoapp.io.dto.NewsEntity
import com.aimright.admin.demoapp.io.dto.Results
import com.aimright.admin.infomosdk.apis.InfomoSDK
import com.aimright.admin.infomosdk.utils.LogE
import com.aimright.admin.infomosdk.utils.OutsideAppZones
import com.aimright.admin.infomosdk.view.InfomoBannerView
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdLoader
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.formats.NativeAdOptions
import com.google.android.gms.ads.formats.UnifiedNativeAd
import com.google.android.gms.ads.reward.RewardedVideoAd
import com.aimright.admin.demoapp.R
import com.aimright.admin.infomosdk.djax.adserver.AdFetcher
import com.google.android.gms.ads.doubleclick.PublisherAdRequest
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_category.*
import java.util.*


const val CATEGORY_ID = "category_id"
const val IS_HOME_CATEGORY = "is_home_category"
const val NEWS_ITEM: String = "clicked_news_item"
const val VIDEO_ITEM: String = "clicked_video_item"
const val CLICKED_ITEM_TYPE = "clicked_item_type"

class CategoryFragment : BaseFragment(), CategotyItemClickCallback, BottomNavigationDialog.Listener {
    private var mPager: ViewPager? = null
    private var lastPage: Boolean = false
    private lateinit var cartegoryData: ArrayList<Results>
    private lateinit var adapter1: CategoriesVideoAdapter
 //   private lateinit var adList: ArrayList<UnifiedNativeAd>
    private lateinit var adapter: CategoriesVideoAdapter
    private val mAd: RewardedVideoAd? = null
    private var page:Int=2
    private var pageSize:Int=5
    private var listener: OnFragmentInteractionListener? = null
    private var categoryId: String? = null
    private var categoryType: String = "hybrid"
    private var currentPage:Int=0
    private var isStarted:Boolean=false
    private var Visible:Boolean = false

   /*Native Ads*/
   // The number of native ads to load.
   val NUMBER_OF_ADS = 5

    // The AdLoader used to load ads.
    private var adLoader: AdLoader? = null

    // List of MenuItems and native ads that populate the RecyclerView.
    private val mRecyclerViewItems = ArrayList<Any>()

    // List of native ads that have been successfully loaded.
  //  private var mNativeAds = ArrayList<AdResponse>()
   // private var bannerAd = ArrayList<AdResponse>()


    companion object {
        @JvmStatic
        fun newInstance(
            categoryId: String,
            isHomeCategory: String/*,
            adList: ArrayList<UnifiedNativeAd>*///,
           /* adList: ArrayList<AdResponse>*///,
           // bannerAd: ArrayList<AdResponse>
        ) =
            CategoryFragment().apply {
                arguments = Bundle().apply {
                    putString(CATEGORY_ID, categoryId)
                    putString(IS_HOME_CATEGORY, isHomeCategory)
                   // putSerializable("mNativeAds", adList)
                 //   putSerializable("bannerAd", bannerAd)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        categoryId = arguments?.getString(CATEGORY_ID)
        categoryType = arguments?.getString(IS_HOME_CATEGORY)!!
       // mNativeAds = arguments?.getSerializable("mNativeAds") as java.util.ArrayList<AdResponse>
      //  bannerAd = arguments?.getSerializable("bannerAd") as java.util.ArrayList<AdResponse>
      //  var infomoSDK1 = InfomoSDK.instance().init(activity)
       // infomoSDK1.getAd("450X450", 2, activity, mNativeAds)
       // infomoSDK1.getAd("320X50", 3, activity, bannerAd)
        loadNativeAds()

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_category, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Initialize the Google Mobile Ads SDK

        //MobileAds.initialize(context)
       /* if (mNativeAds.size==0 && HomeScreenActivity.mNativeAdsStatic.size>0)
        {
            mNativeAds.clear()
            mNativeAds.addAll(HomeScreenActivity.mNativeAdsStatic)
        }
        if (bannerAd.size==0 && HomeScreenActivity.bannerAdStatic.size>0)
        {
            bannerAd.clear()
            bannerAd.addAll(HomeScreenActivity.bannerAdStatic)
        }*/
        // mPager =  view.findViewById<ViewPager>(R.id.slider)

        swipeLayout.setOnRefreshListener(
            SwipeRefreshLayout.OnRefreshListener {
                loadData()
            }
        )

        swipeLayout.setColorSchemeResources(
            R.color.colorPrimary
        )

       /* val adRequest = PublisherAdRequest.Builder().build()
         publisherAdView1.loadAd(adRequest)*/
        try {
            val infomoAdView1 = InfomoBannerView(activity)
            val outsideAppZones: OutsideAppZones =
                InfomoSDK.getInstance(activity).getPublisherInfo().getOutsideAppZones()
           val notification_banner_bottom = outsideAppZones.getNotificationBannerBottom();
            infomoAdView1.zoneid = "993"
            val userInfo =
                InfomoSDK.getInstance(activity).userInfo
            if (userInfo != null) {
                val userProfileInfo = userInfo.profileInfo
                if (userProfileInfo != null && !userProfileInfo.isEmpty()) {
                    infomoAdView1.profileInfo = userProfileInfo
                } else {
                    infomoAdView1.profileInfo = "age=25&gender=Male&height=165&weight=60"
                }
            } else {
                infomoAdView1.profileInfo = "age=25&gender=Male&height=165&weight=60"
            }
            infomoAdView1.keywords = "Sports,Politics,Art,Movies,Books"
            infomoAdView1.LoadAd()
            banner_ad_category.bringToFront()
            banner_ad_category.addView(infomoAdView1)
        }
        catch (var6: java.lang.Exception) {
            LogE.printMe(var6)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        cartegoryData= ArrayList<Results>()


        initScroller()
        loadData()




        /*
    Handler().postDelayed({
        adapter1.notifyDataSetChanged()
    },8000)*/
    }




    private fun initScroller() {
        categoryRv.addOnScrollListener(object: RecyclerView.OnScrollListener(){
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (lastPage==false)
                {
                    val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
                    if (!isLoading) {
                        if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == cartegoryData.size - 1) {
                            //bottom of list!
                            progressBar.visibility = View.VISIBLE
                            loadMore()
                            isLoading = true
                        }
                    }
                }

            }
        })

    }
    private fun loadMore() {
      //  cartegoryData.clear()
        //adapter1.notifyItemInserted(cartegoryData.size - 1)


        val handler = Handler()
        handler.postDelayed(Runnable {
           // cartegoryData.removeAt(cartegoryData.size - 1)
           // val scrollPosition = cartegoryData.size
          //  adapter1.notifyItemRemoved(scrollPosition)
         //   var currentSize = scrollPosition
         //   val nextLimit = currentSize + 10

            try {
                compsiteDisposable.add(
                    RetrofitApiServiceBuilder.getService().fetchNewsByCategory(categoryId, page, pageSize )
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({
                            if (activity != null) {
                                //   loadNativeAds(it)
                                cartegoryData.addAll(it.data.results)
                                adapter1.notifyItemRangeChanged(0, adapter1.getItemCount());
                               // adapter1.notifyDataSetChanged()
                                lastPage=it.data.meta.lastPage
                                page++
                                progressBar.visibility = View.GONE

                            }
                        }, Throwable::printStackTrace)
                )
            } catch (e: Exception) {
                e.printStackTrace()
            }


            isLoading = false
        }, 500)
        progressBar.visibility = View.GONE


    }

    var isLoading = false

    override fun onStart() {
        super.onStart()
        isStarted = true
        if (Visible && isStarted) {
            viewDidAppear()
        }
    }


    private fun viewDidAppear() {
        Handler().postDelayed(
            {
                //imageSlider()

            },2000
        )

    }


    override fun setUserVisibleHint(visible: Boolean) {
        super.setUserVisibleHint(visible)
        Visible = visible
        if (isStarted && Visible) {
            viewDidAppear()
        }

        if (visible && isResumed) {
            //Only manually call onResume if fragment is already visible
            //Otherwise allow natural fragment lifecycle to call onResume
            onResume()
        } else {
            onPause()
        }
    }

/*private fun imageSlider() {

    if (bannerAd.size>0)
    {

        mPager!!.setAdapter(ImageSliderAdapter(context, bannerAd))
        // Auto start of viewpager
        val handler =  Handler()
        val Update =  Runnable() {
            fun run() {
                if (currentPage >= bannerAd.size) {
                    currentPage = 0
                }
                mPager!!.setCurrentItem(currentPage++, true)

            }
        }

        Timer().schedule(object : TimerTask() {
            override fun run() {
                handler.post {
                    if (currentPage >= bannerAd.size) {
                        currentPage = 0
                    }
                    mPager!!.setCurrentItem(currentPage++, true)

                }
                // handler.post(Update)
            }
        }, 2500,2500)
    }

    }*/

    override fun onStop() {
        super.onStop()
        isStarted = false
    }
    private fun loadData() {
        cartegoryData.clear()
        compsiteDisposable.add(
            RetrofitApiServiceBuilder.getService().fetchNewsByCategory(categoryId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    if (activity != null) {
                        //   loadNativeAds(it)


                        swipeLayout.isRefreshing = false
                        categoryRv.setHasFixedSize(false)
                        categoryRv.layoutManager = LinearLayoutManager(context)
                        Log.e("hybrid value", it.toString())
                        if (categoryType == CATEGORY_HYBRID_TYPE){
                        //  categoryRv.adapter = CategoriesHybridAdapter(it)
                            cartegoryData.addAll(it.data.results)
                        adapter1 = CategoriesVideoAdapter(cartegoryData/*, mNativeAds*/)
                        categoryRv.adapter = adapter1

                            adapter1.notifyDataSetChanged()
                        progressBar.visibility = View.GONE
                    } else
                        {
                            cartegoryData.addAll(it.data.results)
                            adapter1  = CategoriesVideoAdapter(cartegoryData/*, mNativeAds*/)
                            categoryRv.adapter=adapter1
                            adapter1.notifyDataSetChanged()
                            progressBar.visibility = View.GONE
                        }


                    }
                }, Throwable::printStackTrace)
        )

    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }


    private fun loadNativeAds() {
    //    adapter=CategoriesVideoAdapter(it, mNativeAds)
      /*  try {
            val builder = AdLoader.Builder(context, getString(R.string.ad_unit_id))
            adLoader = builder.forUnifiedNativeAd(
                UnifiedNativeAd.OnUnifiedNativeAdLoadedListener { unifiedNativeAd ->




                    // A native ad loaded successfully, check if the ad loader has finished loading
                    // and if so, insert the ads into the list.
                    mNativeAds.add(unifiedNativeAd)



                }).withAdListener(
                object : AdListener() {
                    override fun onAdFailedToLoad(errorCode: Int) {
                        // A native ad failed to load, check if the ad loader has finished loading
                        // and if so, insert the ads into the list.
                        Log.e(
                            "MainActivity",
                            "The previous native ad failed to load. Attempting to" + " load another."
                        )

                    }
                }).build()

            // Load the Native ads.
            adLoader!!.loadAds(AdRequest.Builder().build(), NUMBER_OF_ADS)
        } catch (e: Exception) {
            e.printStackTrace()
        }*/

        val adLoader = AdLoader.Builder(context, "ca-app-pub-1276883186035608/5602916941")
            .forUnifiedNativeAd { ad : UnifiedNativeAd ->
                // Show the ad.
                //mNativeAds.add(ad)
                //adapter!!.notifyDataSetChanged()


            }
            .withAdListener(object : AdListener() {
                override fun onAdFailedToLoad(errorCode: Int) {
                    // Handle the failure by logging, altering the UI, and so on.
                }
            })
            .withNativeAdOptions(NativeAdOptions.Builder()
                // Methods in the NativeAdOptions.Builder class can be
                // used here to specify individual options settings.
                .build())
            .build()
        adLoader.loadAds(AdRequest.Builder().build(), 10)



    }




    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

    lateinit var newsEntity: NewsEntity
    lateinit var videosEntity: CategoryResponseVideosEntity

    override fun onNewsItemClicked(newsEntity: NewsEntity) {
        this.newsEntity = newsEntity
        BottomNavigationDialog.newInstance(NEWS_ITEM).show(childFragmentManager, "")
    }

    override fun onVideoItemClicked(videosEntity: CategoryResponseVideosEntity) {
        this.videosEntity = videosEntity
        BottomNavigationDialog.newInstance(VIDEO_ITEM).show(childFragmentManager, "")
    }

    override fun onItemClicked(position: Int, clickedItemType: String) {
        if (position == 1)
            when (clickedItemType) {
                NEWS_ITEM -> {
                    val sendIntent: Intent = Intent().apply {
                        action = Intent.ACTION_SEND
                        putExtra(Intent.EXTRA_TEXT, newsEntity.newsTitle)
                        type = "text/plain"

//                        putExtra(Intent.EXTRA_TEXT, "url to send")
                    }
                    startActivity(Intent.createChooser(sendIntent, "Share via"))
                }
                VIDEO_ITEM -> {

                    val sendIntent: Intent = Intent().apply {
                        action = Intent.ACTION_SEND
                        putExtra(Intent.EXTRA_TEXT, videosEntity.videoTitle)
                        type = "text/plain"

                        putExtra(Intent.EXTRA_TEXT, videosEntity.videoUrl)
                    }
                    startActivity(Intent.createChooser(sendIntent, "Share via"))
                }
            }
    }

    override fun onShowMoreClickHandle(categoryId: String) {
        val homeScreenActivity = activity as HomeScreenActivity
        homeScreenActivity.replaceNewsPaginationFragment(categoryId)
    }

}
