package com.aimright.admin.demoapp.model.search;

public class News {
    private String time_stamp;

    private String author;

    private String id;

    private String news_title;

    private String[] thumbnails;

    public String getTime_stamp ()
    {
        return time_stamp;
    }

    public void setTime_stamp (String time_stamp)
    {
        this.time_stamp = time_stamp;
    }

    public String getAuthor ()
    {
        return author;
    }

    public void setAuthor (String author)
    {
        this.author = author;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getNews_title ()
    {
        return news_title;
    }

    public void setNews_title (String news_title)
    {
        this.news_title = news_title;
    }

    public String[] getThumbnails ()
    {
        return thumbnails;
    }

    public void setThumbnails (String[] thumbnails)
    {
        this.thumbnails = thumbnails;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [time_stamp = "+time_stamp+", author = "+author+", id = "+id+", news_title = "+news_title+", thumbnails = "+thumbnails+"]";
    }
}
