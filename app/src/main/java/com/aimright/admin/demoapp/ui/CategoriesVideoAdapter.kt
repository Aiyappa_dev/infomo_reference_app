package com.aimright.admin.demoapp.ui

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import com.bumptech.glide.Glide
import com.aimright.admin.demoapp.R
import com.aimright.admin.demoapp.io.dto.Results
import com.aimright.admin.demoapp.io.dto.VIDEO_ITEM_TYPE
import java.util.*


class CategoriesVideoAdapter(
    var categoriesResponse:ArrayList<Results>//,
  /*  var mNativeAds: ArrayList<AdResponse>*/
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
  //  private var adView: UnifiedNativeAdView? = null

    private var position:Int=0
    private var pos:Int=0
    // The unified native ad view type.
    private val UNIFIED_NATIVE_AD_VIEW_TYPE = 3
    private val TYPE_ONE = 1
    private val TYPE_TWO = 2
    private val VIEW_TYPE_LOADING = 4
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == UNIFIED_NATIVE_AD_VIEW_TYPE /*&& mNativeAds.size>0*/)
        {
            val unifiedNativeLayoutView = LayoutInflater.from(parent.context).inflate(
                R.layout.ad_unified,
                parent, false
            )
          //  return  UnifiedNativeAdViewHolder(unifiedNativeLayoutView)
            return NewsViewHolderAd(LayoutInflater.from(parent.context).inflate(R.layout.ad_unified, parent, false))
        }
        else if (viewType === TYPE_ONE) {
            return NewsViewHolderLarge(LayoutInflater.from(parent.context).inflate(R.layout.home_screen_news_list_item, parent, false))

        } else if (viewType === TYPE_TWO) {
            return NewsViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_list, parent, false))

        }  else if(viewType == VIEW_TYPE_LOADING)
        {
            val view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return  LoadingViewHolder(view);
        }

      else  {
            throw RuntimeException("The type has to be ONE or TWO") as Throwable
        }


    }



    /*fun getAdView(): UnifiedNativeAdView? {
        return adView
    }
*/







    override fun getItemCount(): Int {
        return categoriesResponse.size
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, pos: Int) {
        when(viewHolder.itemViewType)
        {
            UNIFIED_NATIVE_AD_VIEW_TYPE->
            {

               /* if (mNativeAds.size!=0 && mNativeAds.size>0)
                {
                    position=ThreadLocalRandom.current().nextInt(1, mNativeAds.size + 1)

                    val nativeAd = mNativeAds.get(position-1)
                    populateNativeAdView(mNativeAds, viewHolder*//*, (viewHolder as UnifiedNativeAdViewHolder).adView*//*)
                }*/

            }


            TYPE_ONE->{
                newsTypeOne(viewHolder, pos)
            }
            TYPE_TWO -> {
                newsTypeTwo(viewHolder, pos)
            }
            VIEW_TYPE_LOADING->
            {
                showLoadingView(viewHolder as LoadingViewHolder, position)
            }

        }
    }

    /*private fun populateNativeAdView(
        nativeAd: ArrayList<AdResponse>*//*,
        adView: UnifiedNativeAdView*//*, viewHolder: RecyclerView.ViewHolder

    )
    {
        try {

            if (pos >= nativeAd.size) {
                pos = 0
            }
            val newsView = viewHolder as NewsViewHolderAd
            newsView.thumbnail.setOnClickListener {
                val browserIntent =
                    Intent(Intent.ACTION_VIEW, Uri.parse(nativeAd.get(pos).clickUrl))
                startActivity(newsView.itemView.context, browserIntent, null)
            }
            if (nativeAd.get(pos).imageUrl.isNotEmpty()) {
                Picasso.with(newsView.itemView.context)
                    .load(nativeAd.get(pos).imageUrl)
                    .fit()
                    .placeholder(R.drawable.ic_more_horiz_grey)
                    .into(newsView.thumbnail)
            }
            if (nativeAd.get(pos).adDetail != null && nativeAd.get(pos).adDetail.isNotEmpty()) {
                newsView.title.setText(nativeAd.get(pos).adDetail)
            }
            val infomoSDK1 = InfomoSDK.instance().init(newsView.itemView.context)
            infomoSDK1.impression(nativeAd.get(pos).impressionId)
            pos++
        }catch (e:Exception)
        {

        }


    }*/

    private fun newsTypeOne(
        viewHolder: RecyclerView.ViewHolder,
        pos: Int
    ) {
        val newsViewHolder = viewHolder as NewsViewHolderLarge
        val newsItem = categoriesResponse.get(pos)
        newsViewHolder.title.text = newsItem.getTitle()
        newsViewHolder.itemView.setOnClickListener() {
            val intent = Intent(viewHolder.itemView.context, NewsDetailPagerActivity::class.java)
            intent.putParcelableArrayListExtra(NEWS_LIST, categoriesResponse)
            intent.putExtra(NEWS_SELECTED_POSITION, pos)
            viewHolder.itemView.context.startActivity(intent)
        }

        val now = System.currentTimeMillis()

        val relativeTimeSpanString = DateUtils.getRelativeTimeSpanString(
            newsItem.timeStamp?.time!!,
            now,
            DateUtils.MINUTE_IN_MILLIS
        )
        newsViewHolder.timestampTv.text = relativeTimeSpanString


        if (newsItem.getThumbNail().isNotEmpty()) {
            /*Picasso.with(newsViewHolder.itemView.context)
                .load(newsItem.getThumbNail())
                .fit()
                .centerCrop()
                .placeholder(R.drawable.ic_more_horiz_grey)
                .into(newsViewHolder.newsListImageView)*/
            Glide
                .with(newsViewHolder.itemView.context)
                .load(newsItem.getThumbNail())
                .into(newsViewHolder.newsListImageView)
        } else {
            newsViewHolder.newsListImageView.setImageResource(R.drawable.ic_more_horiz_grey)
        }
        newsViewHolder.moreIv.visibility = View.GONE
        newsViewHolder.headerLayout.visibility = View.GONE
        if (newsItem.Author != null) {
            val authorText = "Reported By - " + newsItem.Author
            newsViewHolder.authorTextView.text = authorText
        }
        newsViewHolder.categoryTextView.visibility = View.GONE

        if (newsItem.getItemViewType() == VIDEO_ITEM_TYPE) {
            newsViewHolder.videoDurationTv.visibility = View.VISIBLE
            newsViewHolder.videoDurationTv.text = newsItem.getDurationString()
        } else {
            newsViewHolder.videoDurationTv.visibility = View.GONE

        }

        newsViewHolder.videoDurationTv.visibility = View.GONE
    }

    private fun newsTypeTwo(
        viewHolder: RecyclerView.ViewHolder,
        pos: Int
    ) {
        val newsViewHolder = viewHolder as NewsViewHolder
        val newsItem = categoriesResponse.get(pos)
        newsViewHolder.title.text = newsItem.getTitle()
        newsViewHolder.itemView.setOnClickListener() {
            val intent = Intent(viewHolder.itemView.context, NewsDetailPagerActivity::class.java)
            intent.putParcelableArrayListExtra(NEWS_LIST, categoriesResponse)
            intent.putExtra(NEWS_SELECTED_POSITION, pos)
            viewHolder.itemView.context.startActivity(intent)
        }

        val now = System.currentTimeMillis()

        val relativeTimeSpanString = DateUtils.getRelativeTimeSpanString(
            newsItem.timeStamp?.time!!,
            now,
            DateUtils.MINUTE_IN_MILLIS
        )
        newsViewHolder.timestampTv.text = relativeTimeSpanString


        if (newsItem.getThumbNail().isNotEmpty()) {
            Glide.with(newsViewHolder.itemView.context)
                .load(newsItem.getThumbNail())
                .into(newsViewHolder.newsListImageView)
            /*
            Picasso.with(newsViewHolder.itemView.context)
                .load(newsItem.getThumbNail())
                .fit()
                .centerCrop()
                .placeholder(R.drawable.ic_more_horiz_grey)
                .into(newsViewHolder.newsListImageView)*/
        } else {
            newsViewHolder.newsListImageView.setImageResource(R.drawable.ic_more_horiz_grey)
        }
        newsViewHolder.moreIv.visibility = View.GONE
        newsViewHolder.headerLayout.visibility = View.GONE
        if (newsItem.Author != null) {
            val authorText = "Reported By - " + newsItem.Author
            newsViewHolder.authorTextView.text = authorText
        }
        newsViewHolder.categoryTextView.visibility = View.GONE

        if (newsItem.getItemViewType() == VIDEO_ITEM_TYPE) {
            newsViewHolder.videoDurationTv.visibility = View.VISIBLE
            newsViewHolder.videoDurationTv.text = newsItem.getDurationString()
        } else {
            newsViewHolder.videoDurationTv.visibility = View.GONE

        }

        newsViewHolder.videoDurationTv.visibility = View.GONE
    }

    override fun getItemViewType(position: Int): Int {
       // return super.getItemViewType(position)
        /*val recyclerViewItem = mRecyclerViewItems.get(position)
        if (recyclerViewItem instanceof UnifiedNativeAd) {

        }*/
        if (categoriesResponse.get(position)==null)
        {
            return VIEW_TYPE_LOADING
        }

        if (position==0)
            return TYPE_ONE
       /*else if ((position+1)%3==0*//* && mNativeAds.size>0*//*)
        {
            return  UNIFIED_NATIVE_AD_VIEW_TYPE
        }*/
        else if (position<categoriesResponse.size/* || mNativeAds.size==0*/)
            return TYPE_TWO
        else
            return 0
    }

    private fun showLoadingView(viewHolder: LoadingViewHolder, position: Int) {
        //ProgressBar would be displayed

    }

    inner class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    {
        val progress=itemView.findViewById<ProgressBar>(R.id.progressBar)

    }


        inner class NewsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title = itemView.findViewById(R.id.category_title_tv) as TextView
        val timestampTv = itemView.findViewById(R.id.news_timestamp) as TextView
        val newsListImageView = itemView.findViewById(R.id.news_list_IV) as ImageView
        val moreIv: ImageView = itemView.findViewById(R.id.more_IV)
        val videoDurationTv = itemView.findViewById(R.id.videoDuration) as TextView
        val authorTextView = itemView.findViewById(R.id.reportedBy) as TextView
        val categoryTextView = itemView.findViewById(R.id.category) as TextView
        val headerLayout = itemView.findViewById(R.id.header) as LinearLayout

    }
    inner class NewsViewHolderLarge(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title = itemView.findViewById(R.id.category_title_tv1) as TextView
        val timestampTv = itemView.findViewById(R.id.news_timestamp1) as TextView
        val newsListImageView = itemView.findViewById(R.id.news_list_IV1) as ImageView
        val moreIv: ImageView = itemView.findViewById(R.id.more_IV1)
        val videoDurationTv = itemView.findViewById(R.id.videoDuration1) as TextView
        val authorTextView = itemView.findViewById(R.id.reportedBy1) as TextView
        val categoryTextView = itemView.findViewById(R.id.category1) as TextView
        val headerLayout = itemView.findViewById(R.id.header1) as LinearLayout

    }

    inner class NewsViewHolderAd(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val thumbnail=itemView.findViewById(R.id.ad_media) as ImageView
        val title=itemView.findViewById(R.id.ad_headline) as TextView
   // val image=itemView.findViewById(R.id.banner_img)  as ImageView

    }

}