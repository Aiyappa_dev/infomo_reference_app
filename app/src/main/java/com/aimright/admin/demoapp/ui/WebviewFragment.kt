package com.aimright.admin.demoapp.ui

import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.aimright.admin.demoapp.R
import com.aimright.admin.demoapp.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_webview.*

class WebviewFragment : BaseFragment() {
    lateinit var url: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_webview, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        url = arguments?.getString(URL_KEY)!!
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        electionWebview.webChromeClient = WebChromeClient()


        electionWebview.settings.domStorageEnabled = true
        electionWebview.settings.javaScriptEnabled = true
        electionWebview.settings.loadWithOverviewMode = true
        electionWebview.settings.useWideViewPort = true


        electionWebview.webViewClient = object : WebViewClient() {

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                electionProgressBar?.visibility = View.VISIBLE
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                electionProgressBar?.visibility = View.GONE

            }

            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                view?.loadUrl(request?.url.toString())
                return true
            }
        }
        electionWebview.visibility = View.VISIBLE
        electionWebview.loadUrl(url)
    }

    companion object {
        private const val URL_KEY = "url"

        fun newInstance(url: String): WebviewFragment {
            val args: Bundle = Bundle()
            args.putString(URL_KEY, url)
            val fragment = WebviewFragment()
            fragment.arguments = args
            return fragment
        }
    }
}