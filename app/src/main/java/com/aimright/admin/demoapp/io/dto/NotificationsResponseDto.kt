package com.aimright.admin.demoapp.io.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class NotificationsResponseDto(
    @Expose
    @SerializedName("status")
    var status: Boolean,
    @Expose
    @SerializedName("data")
    var dataEntity: List<NotificationDataEntity>
)

data class NotificationDataEntity(
    @Expose
    @SerializedName("id")
    var id: Int,
    @Expose
    @SerializedName("news_id")
    var newsId: String,
    @Expose
    @SerializedName("push_title")
    val pushTitle: String,
    @Expose
    @SerializedName("push_message")
    val pushMessage: String,
    @Expose
    @SerializedName("thumbnail")
    val thumbnail: String,
    @Expose
    @SerializedName("time_stamp")
    val timeStamp: String
)