package com.aimright.admin.demoapp.model;


public class Data {
    private Results[] results;

    /* public Meta getMeta ()
     {
         return meta;
     }

     public void setMeta (Meta meta)
     {
         this.meta = meta;
     }
 */
    public Results[] getResults ()
    {
        return results;
    }

    public void setResults (Results[] results)
    {
        this.results = results;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [meta = "+""+", results = "+results+"]";
    }
}
