package com.aimright.admin.demoapp.io.dto

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

data class HomeScreenResponseDto(
    @Expose
    @SerializedName("status")
    var status: Boolean,
    @Expose
    @SerializedName("data")
    var data: DataEntity
)

data class DataEntity(
    @Expose
    @SerializedName("results")
    var resultsEntity: List<ResultsEntity>,
    @Expose
    @SerializedName("meta")
    var meta: MetaEntity
)

data class MetaEntity(
    @Expose
    @SerializedName("page_count")
    var pageCount: Int,
    @Expose
    @SerializedName("total_results")
    var totalResults: Int,
    @Expose
    @SerializedName("current_page_no")
    var currentPageNo: Int,
    @Expose
    @SerializedName("limit")
    val limit: String,
    @Expose
    @SerializedName("last_page")
    var lastPage: Boolean
)

data class ResultsEntity(
    @Expose
    @SerializedName("id")
    var id: String,
    @Expose
    @SerializedName("item_type")
    val itemType: String,
    @Expose
    @SerializedName("news_title")
    val newsTitle: String,

//album_title
    @Expose
    @SerializedName("video_title")
    val videoTitle: String,

    //video_thumbnail
    @Expose
    @SerializedName("video_thumbnail")
    val videoThumbnail: String,

//video_id
    @Expose
    @SerializedName("video_id")
    val videoId: String,

    @Expose
    @SerializedName("album_title")
    val albumTitle: String,

    @Expose
    @SerializedName("highlights")
    val highlights: String,
    @Expose
    @SerializedName("tags")
    var tags: List<String>,
    @Expose
    @SerializedName("featured")
    var featured: Boolean,
    @Expose
    @SerializedName("district")
    var districtEntity: List<HomeSCreenDistrictEntity>,
    @Expose
    @SerializedName("breaking_news")
    var breakingNews: Boolean,
    @Expose
    @SerializedName("is_visible")
    var isVisible: Boolean,
    @Expose
    @SerializedName("time_stamp")
    val timeStamp: Date,
    @Expose
    @SerializedName("lifespan")
    val lifespan: String,
    @Expose
    @SerializedName("is_recommended")
    var isRecommended: Boolean,
    @Expose
    @SerializedName("category")
    var categoryEntity: List<HomeSCreenCategoryEntity>,
    @Expose
    @SerializedName("photos")
    var photos: List<String>,
    @Expose
    @SerializedName("thumbnails")
    var thumbnails: List<String>,
    @Expose
    @SerializedName("videos")
    var videos: List<String>,
    @Expose
    @SerializedName("share_url")
    val shareUrl: String,


    @Expose
    @SerializedName("tag")
    var tag: List<String>,

    @Expose
    @SerializedName("thumbnail")
    val thumbnail: String,

    @Expose
    @SerializedName("photo")
    var photosList: ArrayList<PhotoEntity>,

    @Expose
    @SerializedName("duration")
    val duration: String,

    @Expose
    @SerializedName("author")
    val author :String

) {


    fun getTitle(): String {
        if (itemType == "video")
            return videoTitle
        else if (itemType == "photoalbum") {
            return albumTitle
        }
        return newsTitle
    }

    fun getThumbnailUrl(): String {
        if (itemType == "video")
            return videoThumbnail
        else if (itemType == "photoalbum") {
            return thumbnail
        }
        return thumbnails[0]
    }

    fun getDurationString(): String {
        return " $duration "
    }
}

data class HomeSCreenCategoryEntity(
    @Expose
    @SerializedName("id")
    var id: Int,
    @Expose
    @SerializedName("category_name")
    val categoryName: String,
    @Expose
    @SerializedName("icon_url_greyscale")
    val iconUrlGreyscale: String,
    @Expose
    @SerializedName("icon_url_white")
    val iconUrlWhite: String,
    @Expose
    @SerializedName("is_visible")
    var isVisible: Boolean,
    @Expose
    @SerializedName("category_order_number")
    var categoryOrderNumber: Int,
    @Expose
    @SerializedName("is_special")
    var isSpecial: Boolean
)

data class HomeSCreenDistrictEntity(
    @Expose
    @SerializedName("id")
    var id: Int,
    @Expose
    @SerializedName("district_name")
    val districtName: String,
    @Expose
    @SerializedName("district_order")
    var districtOrder: Int,
    @Expose
    @SerializedName("is_visible")
    var isVisible: Boolean
)

data class PhotoItem(
    @Expose
    @SerializedName("id")
    var id: Int,
    @Expose
    @SerializedName("item_type")
    val itemType: String,
    @Expose
    @SerializedName("album_title")
    val albumTitle: String,
    @Expose
    @SerializedName("tag")
    var tag: List<String>,
    @Expose
    @SerializedName("time_stamp")
    val timeStamp: String,
    @Expose
    @SerializedName("thumbnail")
    val thumbnail: String,
    @Expose
    @SerializedName("is_visible")
    var isVisible: Boolean,
    @Expose
    @SerializedName("photo")
    var photosList: ArrayList<HomeFeedPhotoEntity>
)

data class HomeFeedPhotoEntity(
    @Expose
    @SerializedName("id")
    var id: Int,
    @Expose
    @SerializedName("album_id")
    var albumId: Int,
    @Expose
    @SerializedName("photo_urls")
    val photoUrls: String
) : Parcelable {
    constructor(source: Parcel) : this(
        source.readInt(),
        source.readInt(),
        source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt(id)
        writeInt(albumId)
        writeString(photoUrls)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<HomeFeedPhotoEntity> = object : Parcelable.Creator<HomeFeedPhotoEntity> {
            override fun createFromParcel(source: Parcel): HomeFeedPhotoEntity = HomeFeedPhotoEntity(source)
            override fun newArray(size: Int): Array<HomeFeedPhotoEntity?> = arrayOfNulls(size)
        }
    }
}