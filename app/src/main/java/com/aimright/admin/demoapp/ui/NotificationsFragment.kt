package com.aimright.admin.demoapp.ui

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.aimright.admin.demoapp.R
import com.aimright.admin.demoapp.base.BaseFragment
import com.aimright.admin.demoapp.io.RetrofitApiServiceBuilder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_notification_list.*
import android.support.v7.widget.SearchView
import com.aimright.admin.demoapp.io.dto.NotificationDataEntity1
import kotlin.collections.ArrayList


class NotificationsFragment : BaseFragment(),  SearchView.OnQueryTextListener {
    private lateinit var adapter1: DistrictAdapter
    private lateinit var list: ArrayList<NotificationDataEntity1>
    override fun onQueryTextSubmit(p0: String?): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

    }

    override fun onQueryTextChange(p0: String?): Boolean {
        if (p0 != null) {
            adapter1!!.filter(p0)
        }
        return false
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_notification_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        notificationRv.layoutManager = LinearLayoutManager(context)
        notificationRv.setHasFixedSize(true)
        search.setOnQueryTextListener(this)
        list=ArrayList<NotificationDataEntity1>()
        loadData()



    }

    private fun loadData() {
        progressBar.visibility = View.VISIBLE
        compsiteDisposable.add(
            RetrofitApiServiceBuilder.getService().getdistictList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    if (activity != null) {
                        list.addAll(it.dataEntity)
                        progressBar.visibility = View.GONE
                      adapter1 = DistrictAdapter(/*it,*/ context, list)
                        notificationRv.adapter=adapter1
                    }
                }, Throwable::printStackTrace)
        )
    }

}