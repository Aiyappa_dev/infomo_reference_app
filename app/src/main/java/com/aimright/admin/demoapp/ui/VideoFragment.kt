package com.aimright.admin.demoapp.ui

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdLoader
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.formats.NativeAdOptions
import com.google.android.gms.ads.formats.UnifiedNativeAd
import com.aimright.admin.demoapp.R
import com.aimright.admin.demoapp.base.BaseFragment
import com.aimright.admin.demoapp.interfaces.ApiInterface
import com.aimright.admin.demoapp.io.RetrofitApiServiceBuilder
import com.aimright.admin.demoapp.io.dto.Results
import com.aimright.admin.demoapp.model.CategoryList
import com.aimright.admin.demoapp.util.ApiClient1
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_search_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class VideoFragment : BaseFragment() {
    private var lastPage: Boolean = false
    var isLoading = false
    private var page:Int=2

    private lateinit var adapter1: CategoriesVideos
    // List of native ads that have been successfully loaded.
    private var mNativeAds = ArrayList<UnifiedNativeAd>()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_search_list, container, false)
    }
    private lateinit var cartegoryData: ArrayList<Results>
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cartegoryData= ArrayList()
        loadNativeAds()
        initScroller()
        LoadNavigation()
        back.setOnClickListener {
            this.activity!!.finish()
        }

      /*  search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(newText: String): Boolean {

                progressBar.visibility = View.VISIBLE
                compsiteDisposable.add(
                    RetrofitApiServiceBuilder.getService().searchFromServer(newText)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            if (activity != null) {
//                            Log.d(TAG, it.data.toString())

                                searchRv.layoutManager = LinearLayoutManager(context)
                                searchRv.adapter = SearchAdapter(it, object : CategotyItemClickCallback {

                                    override fun onNewsItemClicked(newsEntity: NewsEntity) {


                                    }

                                    override fun onVideoItemClicked(videosEntity: CategoryResponseVideosEntity) {
                                    }

                                })
                            }
                        }, Throwable::printStackTrace)

                )
                progressBar.visibility = View.GONE

                return true
            }

            override fun onQueryTextChange(query: String): Boolean {

                return true
            }

        })*/
    }

    private fun initScroller() {
        searchRv.addOnScrollListener(object: RecyclerView.OnScrollListener(){
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (lastPage==false)
                {
                    val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
                    if (!isLoading) {
                        if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == cartegoryData.size - 1) {
                            //bottom of list!
                            progressBar.visibility = View.VISIBLE
                            loadMore()
                            isLoading = true
                        }
                    }
                }

            }
        })

    }

    private fun loadMore() {
       // cartegoryData.clear()
        //adapter1.notifyItemInserted(cartegoryData.size - 1)
        val apiInterface = ApiClient1.getApiClient().create(
            ApiInterface::class.java)


        val call: Call<CategoryList>
        // call = apiInterface.getNewsCC("in",countryVal, API_KEY);

        call = apiInterface.getCategoryList()
        call.enqueue(object : Callback<CategoryList> {
            override fun onResponse(call: Call<CategoryList>, response: Response<CategoryList>) {
                if (response.body() != null) {
                    val data1= response.body()!!.data as Array
                    for (i in 0 until data1.size)
                    {
                        if (data1[i].category_name.equals("Videos")) {
                            compsiteDisposable.add(
                                RetrofitApiServiceBuilder.getService().fetchNewsByCategoryLoad(data1[i].fetch_url, page)//fetchNewsByCategory(data1[i].fetch_url)
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribeOn(Schedulers.io())
                                    .subscribe({
                                        if (activity != null) {
                                            cartegoryData.addAll(it.data.results)
                                            adapter1.notifyItemRangeChanged(0, adapter1.getItemCount());
                                            // adapter1.notifyDataSetChanged()
                                            lastPage=it.data.meta.lastPage
                                            page++
                                            progressBar.visibility = View.GONE

                                        }
                                    }, Throwable::printStackTrace)
                            )
                        }
                    }


                }
            }

            override fun onFailure(call: Call<CategoryList>, t: Throwable) {
                Toast.makeText(context, "No Result: " + t.message, Toast.LENGTH_SHORT)
                    .show()

            }
        })




       /* val handler = Handler()
        handler.postDelayed(Runnable {
            // cartegoryData.removeAt(cartegoryData.size - 1)
            // val scrollPosition = cartegoryData.size
            //  adapter1.notifyItemRemoved(scrollPosition)
            //   var currentSize = scrollPosition
            //   val nextLimit = currentSize + 10

            try {
                compsiteDisposable.add(
                    RetrofitApiServiceBuilder.getService().getCategoryList()//fetchNewsByCategoryLoad(categoryId, page)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({
                            if (activity != null) {
                                //   loadNativeAds(it)



                            }
                        }, Throwable::printStackTrace)
                )
            } catch (e: Exception) {
                e.printStackTrace()
            }


            isLoading = false
        }, 500)*/


    }
     private fun LoadNavigation() {
        val apiInterface = ApiClient1.getApiClient().create(
            ApiInterface::class.java)


        val call: Call<CategoryList>
        // call = apiInterface.getNewsCC("in",countryVal, API_KEY);

        call = apiInterface.getCategoryList()
        call.enqueue(object : Callback<CategoryList> {
            override fun onResponse(call: Call<CategoryList>, response: Response<CategoryList>) {
                if (response.body() != null) {
                    val data1= response.body()!!.data as Array
                    for (i in 0 until data1.size)
                    {
                      if (data1[i].category_name.equals("Videos")) {
                          compsiteDisposable.add(
                              RetrofitApiServiceBuilder.getService().fetchNewsByCategory(data1[i].fetch_url)
                                  .observeOn(AndroidSchedulers.mainThread())
                                  .subscribeOn(Schedulers.io())
                                  .subscribe({
                                      if (activity != null) {

                                          searchRv.layoutManager = LinearLayoutManager(context)
                                          cartegoryData.addAll(it.data.results)
                                          adapter1 = CategoriesVideos(cartegoryData, mNativeAds, context)
                                          searchRv.adapter = adapter1
                                          adapter1.notifyDataSetChanged()
                                          progressBar.visibility = View.GONE

                                      }
                                  }, Throwable::printStackTrace)
                          )
                      }
                    }


                }
            }

            override fun onFailure(call: Call<CategoryList>, t: Throwable) {
                Toast.makeText(context, "No Result: " + t.message, Toast.LENGTH_SHORT)
                    .show()

            }
        })



    }

        /*compsiteDisposable.add(
            RetrofitApiServiceBuilder.getService().fetchNewsByCategory(categoryId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    if (activity != null) {
                        //   loadNativeAds(it)


                        swipeLayout.isRefreshing = false
                        categoryRv.setHasFixedSize(false)
                        categoryRv.layoutManager = LinearLayoutManager(context)
                        Log.e("hybrid value",it.toString())
                        if (categoryType == CATEGORY_HYBRID_TYPE)
                            categoryRv.adapter = CategoriesHybridAdapter(it)
                        else
                            categoryRv.adapter  = CategoriesVideoAdapter(it, mNativeAds)

                        progressBar.visibility = View.GONE

                    }
                }, Throwable::printStackTrace)
        )*/

    private fun loadNativeAds() {

        val adLoader = AdLoader.Builder(context, "ca-app-pub-1276883186035608/5602916941")
            .forUnifiedNativeAd { ad : UnifiedNativeAd ->
                // Show the ad.
                mNativeAds.add(ad)


                //adapter!!.notifyDataSetChanged()

            }
            .withAdListener(object : AdListener() {
                override fun onAdFailedToLoad(errorCode: Int) {
                    // Handle the failure by logging, altering the UI, and so on.
                }
            })
            .withNativeAdOptions(
                NativeAdOptions.Builder()
                    // Methods in the NativeAdOptions.Builder class can be
                    // used here to specify individual options settings.
                    .build())
            .build()
        adLoader.loadAds(AdRequest.Builder().build(), 10)



    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }
}