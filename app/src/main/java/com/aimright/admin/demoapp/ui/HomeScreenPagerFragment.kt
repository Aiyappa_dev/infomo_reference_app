package com.aimright.admin.demoapp.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.annotation.NonNull
import android.support.design.bottomnavigation.LabelVisibilityMode
import android.support.design.widget.BottomNavigationView
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.view.*
import android.widget.Toast
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.messaging.FirebaseMessaging
import com.aimright.admin.demoapp.R
import com.aimright.admin.demoapp.adapter.ItemArrayAdapter
import com.aimright.admin.demoapp.adapter.MyAdapter
import com.aimright.admin.demoapp.base.BaseFragment
import com.aimright.admin.demoapp.interfaces.ApiInterface
import com.aimright.admin.demoapp.interfaces.RecyclerViewClickListener
import com.aimright.admin.demoapp.io.RetrofitApiServiceBuilder
import com.aimright.admin.demoapp.io.dto.CategoryDataEntity
import com.aimright.admin.demoapp.model.CategoryList
import com.aimright.admin.demoapp.model.DataCategory
import com.aimright.admin.demoapp.ui.youtube.FullscreenDemoActivity
import com.aimright.admin.demoapp.ui.youtube.YoutubeWebviewActivity
import com.aimright.admin.demoapp.util.ApiClient1
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.home_screen_fragment.*
import kotlinx.android.synthetic.main.home_screen_fragment.progressBar
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

const val WEBVIEW_TYPE: String = "WEBVIEW"
var viewPager: ViewPager? = null

class HomeScreenPagerFragment : BaseFragment(),
    RecyclerViewClickListener/*, SearchView.OnQueryTextListener */ {
    /*override fun onQueryTextChange(p0: String?): Boolean {

        return false
    }

    override fun onQueryTextSubmit(p0: String?): Boolean {
        val intent=Intent(context, Searching::class.java)
        intent.putExtra("text", p0)
        startActivity(intent)


       *//* val apiInterface = ApiClient1.getApiClient().create(ApiInterface::class.java)


        val call: Call<searchData>
        // call = apiInterface.getNewsCC("in",countryVal, API_KEY);

        call = apiInterface.fetchSearch(p0)
        call.enqueue(object : Callback<searchData> {
            override fun onResponse(call: Call<searchData>, response: Response<searchData>) {

                val  data=response.body()!!.data.news as Array<News>
                val intent=Intent(context, SearchResult::class.java)
                intent.putExtra("list", data)
                startActivity(intent)
            }



            override fun onFailure(call: Call<searchData>, t: Throwable) {

            }
        })*//*
        return false
    }*/


   // private lateinit var adList: java.util.ArrayList<AdResponse>
   // private lateinit var bannerAd: java.util.ArrayList<AdResponse>
    //private lateinit var adList: java.util.ArrayList<UnifiedNativeAd>
    private var tab1: TabLayout.Tab? = null
    private lateinit var itemArrayAdapter: ItemArrayAdapter
    private lateinit var itemList: java.util.ArrayList<String>
    //private var infomoSDK: InfomoSDK? = null
   // lateinit var mAdView: InfomoAdView

  //  lateinit var facebookAdView: com.facebook.ads.AdView
    var isFromPush: Boolean = false
    lateinit var notificationType: String
    lateinit var itemId: String
    private val categoriesList1: ArrayList<DataCategory> = ArrayList()

    private val categoriesList: ArrayList<CategoryDataEntity> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        isFromPush = arguments?.getBoolean(IS_FROM_PUSH)!!
        notificationType = arguments?.getString(PUSH_NOTIFICATION_TYPE)!!
        //adList = arguments?.getSerializable("mNativeAds") as java.util.ArrayList<AdResponse>
      //  bannerAd = arguments?.getSerializable("bannerAd") as java.util.ArrayList<AdResponse>

        itemId = arguments?.getString(ITEM_ID)!!


    }

/*
    val transformation = object : Transformation {

      */
/*  override fun transform(source: Bitmap): Bitmap {
            //val targetWidth = adImageView.width

            val aspectRatio = source.height.toDouble() / source.width.toDouble()
            //val targetHeight = (targetWidth * aspectRatio).toInt()
          //  val result = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false)
            if (result != source) {
                // Same bitmap is returned if sizes are the same
                source.recycle()
            }
            return result
        }*//*


        override fun key(): String {
            return "transformation" + " desiredWidth"
        }
    }
*/


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.home_screen_base_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val tabs = view.findViewById(R.id.tabs) as TabLayout
       // infomoSDK = InfomoSDK.instance().init(context)
      //  adView.refreshAd()
        navigation.itemIconTintList = null
       viewPager = view.findViewById<ViewPager>(R.id.viewPager1)
        // rv_list=view.findViewById(R.id.rv_list)
        //initiliseNavigationMenu()
       /* search.setOnQueryTextListener(this)
        search.setOnClickListener {
            search.isIconified=false
        }*/

//        tabs.setupWithViewPager(home_pager_VW)

        /*nav_hamburger_menu.setOnClickListener {
            drawer_layout.openDrawer(Gravity.START)
        }*/



        tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(p0: TabLayout.Tab?) {


            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                viewPager!!.currentItem = tab!!.position
                sendAnalytics(tab)
             //   val beginTransaction = childFragmentManager.beginTransaction()

                //                if (tab?.text == "Home") {
                //                    beginTransaction.replace(
                //                        R.id.category_holder,
                //                        HomeFragment.newInstance((tab.tag as CategoryDataEntity).id, false)
                //                    )
                //                } else
                if (WEBVIEW_TYPE == (tab?.tag as? CategoryDataEntity)?.type) {
                    /*beginTransaction.replace(
                        R.id.category_holder,
                        WebviewFragment.newInstance((tab.tag as? CategoryDataEntity)!!.url)
                    )*/
                } else
                 /*   beginTransaction.replace(
                        R.id.category_holder,
                        CategoryFragment.newInstance(
                            (tab?.tag as? CategoryDataEntity)?.fetchUrl.toString(),
                            (tab?.tag as? CategoryDataEntity)?.categoryType.toString(),
                            adList
                        )
                    )*/
               // news/fetchhydbrid/1

                if (tab?.text?.equals("BUSINESS")!!)
                {
                //   val infomoSDK = InfomoSDK.instance().init(context)
                //    infomoSDK.getContextAds("900X1600", 1, context, "IAB3-0")
                }
                else if (tab?.text?.equals("FASHION")!!)
                {
                  //  val infomoSDK = InfomoSDK.instance().init(context)
                  //  infomoSDK.getContextAds("900X1600", 1, context, "IAB18-3")

                }

                loadAd((tab?.tag as? CategoryDataEntity)?.id, view)
               // beginTransaction.commit()

            }

        })
        home.setOnClickListener {
            val intent = Intent(context, HomeScreenActivity::class.java)
            if (intent.extras != null)
                intent.putExtras(intent.extras)
            /* val bundle = Bundle()
             bundle.putSerializable("mNativeAds", mNativeAds)
             intent.putExtra("mNativeAds",bundle)*/
            startActivity(intent)
        }

        live_tv.setOnClickListener {
            launchLiveTv()
        }
        /*livetv_text.setOnClickListener {
            launchLiveTv()
        }*/
        profile.setOnClickListener {
            sendAnalytics("Profile")
            val intent=Intent(context, ProfilePage::class.java)
            context!!.startActivity(intent)
        }
        /*profile_text.setOnClickListener {
            sendAnalytics("Profile")
            val intent=Intent(context, ProfilePage::class.java)
            context!!.startActivity(intent)
        }*/

        navigation.setLabelVisibilityMode(LabelVisibilityMode.LABEL_VISIBILITY_LABELED)
        navigation.setOnNavigationItemSelectedListener(object :
            BottomNavigationView.OnNavigationItemSelectedListener {
            override fun onNavigationItemSelected(@NonNull item: MenuItem): Boolean {



                when {
                    item.itemId == R.id.navigation_live_tv -> launchLiveTv()
                   /* item.itemId == R.id.navigation_search -> {
                        sendAnalytics("Search")
                        val intent = Intent(
                            context,
                            VideoActivity::class.java
                        )
                        context?.startActivity(intent)
                    }*/
                   /* item.itemId == R.id.navigation_notifications -> {
                        sendAnalytics("Notification")
                        val intent = Intent(
                            context,
                            NotificationsActivity::class.java
                        )
                        context?.startActivity(intent)
                    }*/
                    item.itemId == R.id.navigation_profile ->
                    {
                        sendAnalytics("Profile")
                        val intent=Intent(context, ProfilePage::class.java)
                        context!!.startActivity(intent)
                    }
                }
                return true
            }
        })

      //  LoadNavigation()


        val disposable = RetrofitApiServiceBuilder
            .getService()
            .fetchCategories()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({ categoryListResponse ->
                if (activity != null) {

                    compsiteDisposable.add(
                        RetrofitApiServiceBuilder
                            .getService()
                            .fetchElectionUrl()
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribe({

                                categoriesList.clear()
                                categoriesList.addAll(categoryListResponse.categoriesList)
                                for (i in 0 until categoriesList.size)
                                {
                                    tabs!!.addTab(tabs!!.newTab().setText(categoriesList.get(i).categoryName))

                                }
                                val adapter =
                                    MyAdapter(
                                        activity!!.supportFragmentManager,
                                        tabs!!.tabCount,
                                        categoriesList, /*adList,*/
                                        context/*, bannerAd*/
                                    )
                                viewPager!!.adapter = adapter

                                viewPager!!.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))
                                /*  val itemArrayAdapter =  ItemArrayAdapter(R.layout.navigation_parent_item, categoriesList, context)
                                  rv_list.setAdapter(itemArrayAdapter)
                                  itemArrayAdapter.notifyDataSetChanged()*/


                                if (it.status) {
                                    val categoryDataEntity = CategoryDataEntity(
                                        emptyList(),
                                        it.data.id,
                                        it.data.title,
                                        "",
                                        "",
                                        true,
                                        it.data.displayOrder,
                                        true,
                                        WEBVIEW_TYPE,
                                        it.data.url
                                    )
                                    categoriesList.add(0, categoryDataEntity)

                                }
                                /*categoriesList.forEach {

                                    val newTab = tabs.newTab()
                                    newTab.text = it.categoryName
                                    newTab.tag = it
                                    tabs.addTab(newTab)
                                }*/

//                    home_pager_VW.adapter = HomeScreenPagerAdapter(childFragmentManager,
//                        categoryListResponse.categoriesList.sortedBy { it.categoryOrderNumber })
                                progressBar.visibility = View.GONE
//                    Log.d("CATEGORY", "${categoryListResponse.categoriesList}")
//                    this.categoryListResponse.categoriesList =
                                categoriesList.sortedBy { it.categoryOrderNumber }

//                    mGoogleAdView = view.findViewById(R.id.adView)
//                    val adRequest = AdRequest.Builder().build()
//                    mGoogleAdView.loadAd(adRequest)
                                handlePush()

                            }, Throwable::printStackTrace)
                    )

                }
            }, Throwable::printStackTrace)
        compsiteDisposable.add(disposable)
        progressBar.visibility = View.GONE

    }




    override fun recyclerViewListClicked(v: View?, position: Int) {


    }


    private fun LoadNavigation() {
        val apiInterface = ApiClient1.getApiClient().create(
            ApiInterface::class.java)


        val call: Call<CategoryList>
        // call = apiInterface.getNewsCC("in",countryVal, API_KEY);

        call = apiInterface.getCategoryList()
        call.enqueue(object : Callback<CategoryList> {
            override fun onResponse(call: Call<CategoryList>, response: Response<CategoryList>) {
                if (response.body() != null) {
                    val data1= response.body()!!.data as Array
                    for (i in 0 until data1.size)
                    {
                        categoriesList1.add(data1[i])
                    }



                }
            }

            override fun onFailure(call: Call<CategoryList>, t: Throwable) {
                Toast.makeText(context, "No Result: " + t.message, Toast.LENGTH_SHORT)
                    .show()

            }
        })



    }
    /*private fun initiliseNavigationMenu() {
  // Initializing list view with the custom adapter
        itemList =  ArrayList<String>()

       // recyclerView = (RecyclerView) findViewById(R.id.item_list);
        rv_list.setLayoutManager( LinearLayoutManager(context))
        rv_list.setItemAnimator( DefaultItemAnimator())
        // Populating list items



        *//*rv_list.layoutManager = LinearLayoutManager(context)
        recursivePopulateFakeData(0, 12)*//*
    }
*/
    private fun loadAd(categoryId: String?, view: View) {
        var disposable: Disposable? = null
        /*mAdView = view.findViewById(R.id.adViewFixed)
        mAdView.visibility = View.VISIBLE
        mAdView.refreshAd()*/
        MobileAds.initialize(context)
      //  val  mAdView = view!!.findViewById<AdView>(R.id.adView)
        val adRequest = AdRequest.Builder().build()
       // mAdView.loadAd(adRequest)


        // infomoSDK!!.showWebAd()
        /*disposable?.dispose()
        compsiteDisposable.add(
            RetrofitApiServiceBuilder
                .getService()
                .fetchAdsForcategory(categoryId, "Banner")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())

                .subscribe({
                    if (it.status) {


                        disposable =
                            Helper.subscribeAndReturn(it.data.resultsEntity[0].banner.photoEntity)
                                .repeat()
                                .subscribe {
                                    activity?.runOnUiThread {
                                        //adImageView.visibility = View.VISIBLE
                                        val adsItem = it
                                      *//*  adImageView.setOnClickListener {
                                            val intent = Intent(Intent.ACTION_VIEW)
                                            intent.data = Uri.parse(adsItem.adUrl)
                                            startActivity(intent)
                                        }*//*
                                        sdk.showWebAd()

                                        Log.d("custom ads", it.adPhoto)
                                        Picasso.with(this.context)
                                            .load(it.adPhoto)
                                            .error(android.R.drawable.stat_notify_error)
                                            .into(adImageView, object : Callback {
                                                override fun onSuccess() {
                                                }

                                                override fun onError() {
                                                    Log.e(com.google.ads.AdRequest.LOGTAG, "error")
                                                }
                                            })
                                        sendAnalytics("nativeAdLoaded", it.adPhoto)
                                    }
                                }


                    } else {

                        banner_container.removeAllViews()
                        facebookAdView =
                            com.facebook.ads.AdView(
                                context,
                                "122154748214193_710112169418445",
                                AdSize.BANNER_HEIGHT_50
                            )
                        banner_container.addView(facebookAdView)
                        facebookAdView.setAdListener(object : AdListener {
                            override fun onAdClicked(p0: Ad?) {

                            }

                            override fun onError(p0F: Ad?, p1: AdError?) {
                                Log.d(TAG, "" + p1.toString())
                                banner_container.visibility = View.GONE

//                                RelativeLayout.LayoutParams params =(RelativeLayout.LayoutParams) viewToLayout . getLayoutParams ()
//                                params.addRule(RelativeLayout.BELOW, R.id.below_id);
//
                                val params: RelativeLayout.LayoutParams =
                                    category_holder.layoutParams as (RelativeLayout.LayoutParams)
                                params.addRule(RelativeLayout.ABOVE, R.id.adView)
                                adImageView.visibility = View.GONE
                                val adRequest = AdRequest.Builder().build()
                                mAdView.visibility = View.VISIBLE
//                                mGoogleAdView.adSize = AdSize.SMART_BANNER
                                mAdView.loadAd(adRequest)
                            }

                            override fun onAdLoaded(p0: Ad?) {
                            }

                            override fun onLoggingImpression(p0: Ad?) {
                            }

                        })
                        facebookAdView.loadAd()


                    }
                }, Throwable::printStackTrace)
        )
        disposable?.let {
            compsiteDisposable.add(it)
        }*/

    }

    private fun launchLiveTv() {
        try {
           // infomoSDK!!.showAd()

            sendAnalytics("Live Tv")
            val intent = Intent(
                context,
                YoutubeWebviewActivity::class.java
            )
            intent.putExtra(FullscreenDemoActivity.YOUTUBE_ID, "")
            context?.startActivity(intent)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun sendAnalytics(tab: TabLayout.Tab?) {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, (tab?.tag as? CategoryDataEntity)?.id)
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, tab?.text.toString())
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle)
        firebaseAnalytics.setCurrentScreen(
            activity as Activity,
            tab?.text.toString(),
            null /* class override */
        );

    }

    private fun sendAnalytics(screenName: String, id: String = "") {
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, screenName)
        if (id.isNotBlank())
            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, id)
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle)
        firebaseAnalytics.setCurrentScreen(
            activity as Activity,
            screenName,
            null /* class override */
        );

    }


    override fun onResume() {
        super.onResume()
//        AdSettings.addTestDevice("531c76d7-c258-4aa6-91ee-67d156cea2e6")
        navigation.selectedItemId = R.id.navigation_home
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
//        FirebaseInstanceId.getInstance().instanceId
//            .addOnCompleteListener(OnCompleteListener { task ->
//                if (!task.isSuccessful) {
//                    Log.w(TAG, "getInstanceId failed", task.exception)
//                    return@OnCompleteListener
//                }
//
//                // Get new Instance ID token
//                val token = task.result?.token
//
//                // Log and toast
//                val msg = getString(R.string.msg_token_fmt, token)
//                Log.d(TAG, msg)
////                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
//            })

        FirebaseMessaging.getInstance().subscribeToTopic("androidNewsProduction")
//            .addOnCompleteListener { task ->
////                var msg = getString(R.string.msg_subscribed)
////                if (!task.isSuccessful) {
////                    msg = getString(R.string.msg_subscribe_failed)
////                }
////                Log.d(TAG, msg)
////                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
//            }

        //unsubscribeFromTopic
        FirebaseMessaging.getInstance().unsubscribeFromTopic("news")
//            .addOnCompleteListener { task ->
//                var msg = getString(R.string.msg_subscribed)
//                if (!task.isSuccessful) {
//                    msg = getString(R.string.msg_subscribe_failed)
//                }
//                Log.d(TAG, msg)
////                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
//            }
    }

    private fun handlePush() {
        if (isFromPush) {
            when (notificationType) {
                "Categories" -> {
                    categoriesList.forEachIndexed { index, categoryItem ->
                        if (categoryItem.id == itemId) {
                            val tab = tabs.getTabAt(index)
                            tab?.select()
                        }
                    }
                }
                "Livestream" -> {
                    launchLiveTv()
                }
                "Videos" -> {
                    val intent = Intent(context, NewsDetailActivity::class.java)
                    intent.putExtra(ARTICLE_ID, itemId)
                    intent.putExtra(ITEM_TYPE, "video")
                    context?.startActivity(intent)
                }
                "News" -> {
                    val intent = Intent(context, NewsDetailActivity::class.java)
                    intent.putExtra(ARTICLE_ID, itemId)
                    intent.putExtra(ITEM_TYPE, "news")
                    context?.startActivity(intent)
                }
            }
        }
    }

    companion object {
        fun newInstance(
            isFromPush: Boolean,
            itemId: String,
            notificationType: String/*,
            adList: java.util.ArrayList<UnifiedNativeAd>*///,
          /*  mNativeAds: java.util.ArrayList<AdResponse>*//*,
            bannerAd: java.util.ArrayList<AdResponse>*/
        ): HomeScreenPagerFragment =
            HomeScreenPagerFragment().apply {
                arguments = Bundle().apply {
                    putString(ITEM_ID, itemId)
                    putString(PUSH_NOTIFICATION_TYPE, notificationType)
                    putBoolean(IS_FROM_PUSH, isFromPush)
                   // putSerializable("mNativeAds", mNativeAds)
                  //  putSerializable("bannerAd", bannerAd)
                }
            }
    }

    override fun onDestroy() {
       /* if (::facebookAdView.isInitialized && facebookAdView != null) {
            facebookAdView.destroy()
        }*/

        super.onDestroy()
    }

    /*private fun recursivePopulateFakeData(level: Int, depth: Int): List<Any> {
        val list = ArrayList<NavItemDto>()
        var title: String = ""
        when (level) {
            1 -> title = "PQRD %d"
            2 -> title = "XYZ %d"
            3 -> title = "ABCDE %d"
        }

        for (i in 0..depth) {
            val item: NavItemDto =
                NavItemDto(level, String.format(title, i), String.format(title.toLowerCase(), i))
            if (depth % 2 == 0) {
                item.addChildren(
                    recursivePopulateFakeData(
                        level + 1,
                        depth / 2
                    ) as MutableList<RecyclerViewItem>?
                )
            }
            list.add(item)
        }
        return list
    }*/

}