package com.aimright.admin.demoapp

import android.app.Application
import com.aimright.admin.infomosdk.apis.InfomoSDK
import com.google.firebase.FirebaseApp

class PublicTvApplication :Application() {

    override fun onCreate() {
        super.onCreate()
//        if (LeakCanary.isInAnalyzerProcess(this)) {
//            // This process is dedicated to LeakCanary for heap analysis.
//            // You should not init your app in this process.
//            return
//        }
//        LeakCanary.install(this)
//        VidgyorStatusInit.init(this)
        FirebaseApp.initializeApp(this)
        InfomoSDK.getInstance(this).initialize(this)
    }
}