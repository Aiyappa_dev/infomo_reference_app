package com.aimright.admin.demoapp.ui

import android.support.v4.view.PagerAdapter
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.aimright.admin.demoapp.R
import com.aimright.admin.demoapp.io.dto.ResultsEntity
import com.squareup.picasso.Picasso


class HomeHeaderPagerAdapter(
    private val newsEntity: ArrayList<ResultsEntity>,
    private val categotyItemClickCallback: CategotyItemClickCallback
) : PagerAdapter() {

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val newsItem = newsEntity[position]
        val layout = LayoutInflater.from(container.context).inflate(R.layout.home_pager_list_item, container, false)

        val headerImageView: ImageView = layout.findViewById(R.id.header_IV)

        val thumbnailUrl = newsEntity[position].getThumbnailUrl()

        if (thumbnailUrl != null && thumbnailUrl.isNotEmpty())
            Picasso.with(container.context)
                .load(thumbnailUrl)
                .fit()
                .centerCrop()
                .placeholder(R.drawable.ic_more_horiz_grey)
                .into(headerImageView)

        val headerTitleTextView: TextView = layout.findViewById(R.id.header_TV)
        headerTitleTextView.text = newsEntity[position].getTitle()
        val now = System.currentTimeMillis()

        val relativeTimeSpanString = DateUtils.getRelativeTimeSpanString(
            newsItem.timeStamp.time,
            now,
            DateUtils.MINUTE_IN_MILLIS
        )
        (layout.findViewById(R.id.news_timestamp) as TextView).text = relativeTimeSpanString
        layout.setOnClickListener {
            categotyItemClickCallback.onNewsDetailClicked(newsItem)
        }
        if (newsItem.author != null) {
            val authorText = "Reported By - " + newsItem.author
            (layout.findViewById(R.id.reportedBy) as TextView).text = authorText
        }
        if (newsItem.categoryEntity != null && newsItem.categoryEntity.isNotEmpty())
            (layout.findViewById(R.id.category) as TextView).text = newsItem.categoryEntity[0].categoryName

        if (newsItem.itemType == VIDEO_TYPE) {
            (layout.findViewById(R.id.videoDuration) as TextView).visibility = View.VISIBLE
            (layout.findViewById(R.id.videoDuration) as TextView).text = newsItem.getDurationString()
        } else {
            (layout.findViewById(R.id.videoDuration) as TextView).visibility = View.GONE
        }

        container.addView(layout)
        return layout
    }

    override fun destroyItem(container: ViewGroup, position: Int, view: Any) {
        container.removeView(view as View)
    }

    override fun isViewFromObject(view: View, p1: Any): Boolean {
        return view == p1
    }

    override fun getCount(): Int {
        return newsEntity.size
    }


}