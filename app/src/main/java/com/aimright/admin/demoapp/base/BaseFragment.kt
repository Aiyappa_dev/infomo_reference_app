package com.aimright.admin.demoapp.base

import android.os.Bundle
import android.support.v4.app.Fragment
import com.google.firebase.analytics.FirebaseAnalytics
import io.reactivex.disposables.CompositeDisposable

open class BaseFragment : Fragment() {
     lateinit var firebaseAnalytics: FirebaseAnalytics

    val compsiteDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        firebaseAnalytics = context?.let { FirebaseAnalytics.getInstance(it) }!!

    }
    override fun onDestroy() {
        compsiteDisposable.clear()
        super.onDestroy()
    }

}

