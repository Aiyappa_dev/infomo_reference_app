package com.aimright.admin.demoapp.push

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.media.AudioAttributes
import android.net.Uri
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.aimright.admin.demoapp.R
import com.aimright.admin.demoapp.ui.*
import org.json.JSONObject


class PulicTvPushMessagingService : FirebaseMessagingService() {

 //   private lateinit var mInterstitialAd: InterstitialAd

    override fun onMessageReceived(message: RemoteMessage?) {
        val jsonObject = JSONObject(message?.data)
        val articleId: String = jsonObject.get("news_id") as String
        val pushNotificationType: String = jsonObject.getString("push_notification_type")
        Log.d(TAG, jsonObject.toString())
        sendNotification(jsonObject.getString("body"), jsonObject.getString("title"), jsonObject)
        super.onMessageReceived(message)

    }



    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private fun sendNotification(messageBody: String, messageTitle: String, jsonObject: JSONObject) {

       // showInterstitialAd1()

        val intent = Intent(this, HomeScreenActivity::class.java)
//        intent.putExtra(ARTICLE_ID, articleId)
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.putExtra(PUSH_NOTIFICATION_TYPE, jsonObject.getString("push_notification_type"))
        intent.putExtra(ITEM_ID, jsonObject.getString("item_id"))
        intent.putExtra(IS_FROM_PUSH, true)


        val pendingIntent = PendingIntent.getActivity(
            this, System.currentTimeMillis().toInt() /* Request code */, intent,
            PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_ONE_SHOT
        )
        val sound =
            Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + this.applicationContext?.packageName + "/" + R.raw.public_tv_notification)

        val channelId = getString(R.string.default_notification_channel_id)
//        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.drawable.inews_logo)
            .setContentTitle(messageTitle)
            .setContentText(messageBody)
            .setAutoCancel(true)
            .setSound(sound)
            .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                "Public TV",
                NotificationManager.IMPORTANCE_HIGH
            )

            val audioAttributes = AudioAttributes.Builder()
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .build()


            channel.setSound(sound, audioAttributes)
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(
            System.currentTimeMillis().toInt(),/* ID of notification */
            notificationBuilder.build()
        )
    }
}