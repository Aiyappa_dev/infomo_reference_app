package com.aimright.admin.demoapp.io

import com.google.gson.GsonBuilder
import com.aimright.admin.demoapp.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


object RetrofitApiServiceBuilder {

//       private const val BASE_URL = "http://52.66.207.138/"
//    private const val BASE_URL = "https://apis.publictv.in/"

    //private const val BASE_URL = "https://apis.nuntium.in/"
   // private const val BASE_URL = "https://apis.publictv.in/"
//    private const val BASE_URL = "https://aapis.nuntium.in/"
    private const val BASE_URL = "https://cmsapis.infomo.ws/"


    private const val CONNECT_TIMEOUT_SECONDS = 60L
    private const val READ_TIMEOUT_SECONDS = 60L

    private val gson = GsonBuilder().setDateFormat("MM-dd-yyyy HH:mm:ss z").create()

    private val httpInterceptor: HttpLoggingInterceptor = HttpLoggingInterceptor()
        .setLevel(if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE)

    private val okHttpClientBuilder = OkHttpClient.Builder()
        .connectTimeout(CONNECT_TIMEOUT_SECONDS, TimeUnit.SECONDS)
        .readTimeout(READ_TIMEOUT_SECONDS, TimeUnit.SECONDS)
        .addInterceptor(httpInterceptor)
        .addInterceptor { chain ->
            val request = chain.request().newBuilder()
                .addHeader("Content-Type", "application/json")
                .addHeader("User-Agent", "AndroidMobile")

                .build()
            chain.proceed(request)
        }

    private var builder = Retrofit.Builder().baseUrl(BASE_URL)
        .client(okHttpClientBuilder.build())
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())


    private var retrofitService = builder.build()
        .create(RetrofitService::class.java)

    fun getService(): RetrofitService {
        return retrofitService
    }

}




