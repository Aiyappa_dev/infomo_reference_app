package com.aimright.admin.demoapp.interfaces;

import android.view.View;

public interface RecyclerViewClickListener {

    public void recyclerViewListClicked(View v, int position);
}
