package com.aimright.admin.demoapp.ui

import android.content.Intent
import android.support.v4.view.ViewPager
import android.support.v7.widget.RecyclerView
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.aimright.admin.demoapp.R
import com.aimright.admin.demoapp.io.dto.CategoryResponse
import com.aimright.admin.demoapp.io.dto.CategoryResponseVideosEntity
import com.aimright.admin.demoapp.io.dto.NewsEntity
import com.aimright.admin.demoapp.io.dto.PhotosEntity
import com.aimright.admin.demoapp.ui.youtube.FullscreenDemoActivity
import com.aimright.admin.demoapp.ui.youtube.YoutubeWebviewActivity
import com.squareup.picasso.Picasso
import me.relex.circleindicator.CircleIndicator


class HomeCategoryAdapter(
    private val categoryResponse: CategoryResponse,
    private val isHomeCategory: Boolean, private val categotyItemClickCallback: CategotyItemClickCallback
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val list =
        categoryResponse.data.newsEntity + categoryResponse.data.videosEntity + categoryResponse.data.photosEntity


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            0 -> NewsViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.news_list_item, parent, false))
            1 -> PhotosViewHolder(
                (LayoutInflater.from(parent.context).inflate(R.layout.photos_list_item, parent, false))
            )
            3 -> NewsPagerViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.home_category_pager_item,
                    parent,
                    false
                )
            )
            else -> NewsViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.news_list_item, parent, false)
            )
        }
//        )
//        return NewsViewHolder(
//            LayoutInflater.from(parent.context).inflate(R.layout.category_list_item, parent, false)
//        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun getItemViewType(position: Int): Int {

        return when {
            position == 0 -> 3
            list[position] is NewsEntity -> 0
            list[position] is PhotosEntity -> 1
            else -> 2
        }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, pos: Int) {
        when (getItemViewType(pos)) {
            0 -> {
                val newsItem = list[pos] as NewsEntity
                viewHolder.itemView.setOnClickListener {
                    val intent = Intent(viewHolder.itemView.context, NewsDetailPagerActivity::class.java)
                    intent.putParcelableArrayListExtra(NEWS_LIST, categoryResponse.data.newsEntity)
                    intent.putExtra(NEWS_SELECTED_POSITION, pos)
                    viewHolder.itemView.context.startActivity(intent)
                }
                val newsViewHolder = viewHolder as NewsViewHolder

                newsViewHolder.title.text = newsItem.newsTitle
                val now = System.currentTimeMillis()

                val relativeTimeSpanString = DateUtils.getRelativeTimeSpanString(
                    newsItem.timeStamp.time,
                    now,
                    DateUtils.MINUTE_IN_MILLIS
                )
                newsViewHolder.timestampTv.text = relativeTimeSpanString

                if (newsItem.thumbnails.isNotEmpty() && newsItem.thumbnails[0].isNotEmpty()) {
                    Picasso.with(newsViewHolder.itemView.context)
                        .load(newsItem.thumbnails[0])
                        .fit()
                        .centerCrop()
                        .placeholder(R.drawable.ic_more_horiz_grey)
                        .into(newsViewHolder.newsListImageView)
                } else {
                    newsViewHolder.newsListImageView.setImageResource(R.drawable.ic_more_horiz_grey)
                }

                if (isHomeCategory) {
                    newsViewHolder.moreIv.visibility = View.VISIBLE
                    newsViewHolder.moreIv.setOnClickListener {
                        categotyItemClickCallback.onNewsItemClicked(newsItem)
                    }


                } else {
                    newsViewHolder.moreIv.visibility = View.GONE
                }

            }
            1 -> {
                val photosViewHolder = viewHolder as PhotosViewHolder
                val photoItem = list[pos] as PhotosEntity


                viewHolder.itemView.setOnClickListener {
                    val intent = Intent(viewHolder.itemView.context, PhotosDetailActivity::class.java)
                    intent.putParcelableArrayListExtra(PHOTOS_LIST, photoItem.photoEntity)
                    viewHolder.itemView.context.startActivity(intent)
                }
                photosViewHolder.phototitle.text = photoItem.albumTitle
                Picasso.with(viewHolder.itemView.context)
                    .load(photoItem.thumbnail)
                    .fit()
                    .centerCrop()
                    .placeholder(R.drawable.ic_more_horiz_grey)
                    .into(photosViewHolder.photoImage)
            }
            2 -> {
                val catviewHolder = viewHolder as NewsViewHolder
                val videoItem = list[pos] as CategoryResponseVideosEntity

                catviewHolder.itemView.setOnClickListener {

                    val intent = Intent(
                        catviewHolder.itemView.context,
                        YoutubeWebviewActivity::class.java
                    )
                    intent.putExtra(FullscreenDemoActivity.YOUTUBE_ID, videoItem.videoId)
                    catviewHolder.itemView.context.startActivity(intent)
                }
                catviewHolder.title.text = videoItem.videoTitle
                val now = System.currentTimeMillis()

                val relativeTimeSpanString = DateUtils.getRelativeTimeSpanString(
                    videoItem.timeStamp.time,
                    now,
                    DateUtils.SECOND_IN_MILLIS, DateUtils.FORMAT_ABBREV_ALL
                )
                catviewHolder.timestampTv.text = relativeTimeSpanString

                Picasso.with(catviewHolder.itemView.context)
                    .load(videoItem.videoThumbnail)
                    .fit()
                    .centerCrop()
                    .placeholder(R.drawable.ic_more_horiz_grey)
                    .into(catviewHolder.newsListImageView)

//                catviewHolder.moreIv.setOnClickListener {
//                    categotyItemClickCallback.onVideoItemClicked(videoItem)
//                }
                if (isHomeCategory) {
                    catviewHolder.moreIv.setOnClickListener {
                        categotyItemClickCallback.onVideoItemClicked(videoItem)
                    }
                    catviewHolder.moreIv.visibility = View.VISIBLE
                } else {
                    catviewHolder.moreIv.visibility = View.GONE
                }
            }
//            3 -> {
//                val newsPagerViewHolder: NewsPagerViewHolder = viewHolder as NewsPagerViewHolder
//                newsPagerViewHolder.viewPager.adapter = HomeHeaderPagerAdapter(searchResponseDto.data.newsEntity)
////                newsPagerViewHolder.viewPager.clearOnPageChangeListeners()
//
//                newsPagerViewHolder.pagerIndicator.setViewPager(newsPagerViewHolder.viewPager)
////                newsPagerViewHolder.viewPager.addOnAdapterChangeListener(object : ViewPager.OnPageChangeListener,
////                    ViewPager.OnAdapterChangeListener {
////                    override fun onAdapterChanged(p0: ViewPager, p1: PagerAdapter?, p2: PagerAdapter?) {
////
////                    }
////
////                    override fun onPageScrolled(
////                        position: Int,
////                        positionOffset: Float,
////                        positionOffsetPixels: Int
////                    ) {/*empty*/
////                    }
////
////                    override fun onPageSelected(position: Int) {
////                        newsPagerViewHolder.pagerIndicator.selection = position
////                    }
////
////                    override fun onPageScrollStateChanged(state: Int) {/*empty*/
////                    }
////                })
//            }
        }
    }


    inner class NewsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title = itemView.findViewById(R.id.category_title_tv) as TextView
        val timestampTv = itemView.findViewById(R.id.news_timestamp) as TextView
        val newsListImageView = itemView.findViewById(R.id.news_list_IV) as ImageView
        val moreIv: ImageView = itemView.findViewById(R.id.more_IV)
    }

    inner class NewsPagerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val viewPager: ViewPager = itemView.findViewById(R.id.news_pager)
        val pagerIndicator: CircleIndicator = itemView.findViewById(R.id.indicator)

    }

    inner class PhotosViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val photoImage = itemView.findViewById(R.id.photo_IV) as ImageView
        val phototitle = itemView.findViewById(R.id.photoTv) as TextView
    }

}