package com.aimright.admin.demoapp.ui

import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.aimright.admin.demoapp.R
import com.aimright.admin.demoapp.base.BaseFragment
import com.aimright.admin.demoapp.io.dto.PhotoEntity
import kotlinx.android.synthetic.main.fragment_pager_photos_detail.*

const val PHOTOS_LIST = "photos_list"
const val ALBUM_TITLE = "album_title"


class PhotosPagerFragment : BaseFragment() {
    lateinit var photosList: ArrayList<PhotoEntity>
    lateinit var albumTitle :String
  //  lateinit var facebookAdView: com.facebook.ads.AdView
  //  lateinit var facebookInterstitialAd: InterstitialAd
    //lateinit var mGoogleAdView: com.google.android.gms.ads.AdView

    companion object {
        fun newInstance(photosList: ArrayList<PhotoEntity>, albumTitle: String): PhotosPagerFragment =
            PhotosPagerFragment().apply {
                arguments = Bundle().apply {
                    putParcelableArrayList(PHOTOS_LIST, photosList)
                    putString(ALBUM_TITLE, albumTitle)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        photosList = arguments?.getParcelableArrayList<PhotoEntity>(PHOTOS_LIST)!!
        albumTitle = arguments?.getString(ALBUM_TITLE)!!
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_pager_photos_detail, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        loadFacebookAd()
     //   mGoogleAdView = view.findViewById(R.id.NewsDetailAdView)


        photos_detail_pager.adapter = PhotoPagerAdapter(albumTitle,photosList, childFragmentManager)
        val toolbar: Toolbar = view.findViewById(R.id.toolbar)
        toolbar.setNavigationIcon(R.drawable.ic_back)
        toolbar.setNavigationOnClickListener { activity!!.onBackPressed() }
       /* facebookInterstitialAd = InterstitialAd(context, "122154748214193_710112042751791")
        facebookInterstitialAd.loadAd()
        facebookInterstitialAd.setAdListener(object : AdListener, InterstitialAdListener {
            override fun onAdClicked(p0: Ad?) {
                Log.d("Facebook ad", p0.toString())
            }

            override fun onError(p0: Ad?, p1: AdError?) {
                Log.d("Facebook ad", p0.toString() + p1.toString())
                facebookInterstitialAd.loadAd()


            }

            override fun onAdLoaded(p0: Ad?) {
                Log.d("Facebook ad", p0.toString())

            }

            override fun onLoggingImpression(p0: Ad?) {
            }

            override fun onInterstitialDisplayed(p0: Ad?) {
            }

            override fun onInterstitialDismissed(p0: Ad?) {
                facebookInterstitialAd.loadAd()
            }
        })
        photos_detail_pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) {

            }

            override fun onPageScrolled(position: Int, p1: Float, p2: Int) {

            }

            override fun onPageSelected(position: Int) {


                loadFacebookAd()
                if (position % 4 == 0) {
                    if (facebookInterstitialAd.isAdLoaded) {
                        facebookInterstitialAd.show()

                    }
                }
            }


        })*/
        super.onViewCreated(view, savedInstanceState)
    }
    private fun loadFacebookAd() {
        //news_detail_banner_container.removeAllViews()
      /*  facebookAdView =
            com.facebook.ads.AdView(context, "122154748214193_710112169418445", AdSize.BANNER_HEIGHT_50)
        news_detail_banner_container.addView(facebookAdView)
        facebookAdView.setAdListener(object : AdListener {
            override fun onAdClicked(p0: Ad?) {
            }

            override fun onError(p0: Ad?, p1: AdError?) {
                val adRequest = AdRequest.Builder().build()
                mGoogleAdView?.visibility = View.VISIBLE
                mGoogleAdView?.loadAd(adRequest)
            }

            override fun onAdLoaded(p0: Ad?) {
            }

            override fun onLoggingImpression(p0: Ad?) {
            }

        })
        facebookAdView.loadAd()*/

    }

    override fun onDestroy() {
      /*  if(::facebookInterstitialAd.isInitialized && facebookInterstitialAd != null){
            facebookInterstitialAd.destroy()
        }
        if(::facebookAdView.isInitialized && facebookAdView != null){
            facebookAdView.destroy()
        }*/
        super.onDestroy()
    }
}