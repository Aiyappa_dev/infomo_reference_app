package com.aimright.admin.demoapp.io.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class AdsResponse(
    @Expose
    @SerializedName("status")
    var status: Boolean,
    @Expose
    @SerializedName("data")
    var data: AdsDataEntity
)

data class AdsDataEntity(
    @Expose
    @SerializedName("results")
    var resultsEntity: List<AdsResultsEntity>,
    @Expose
    @SerializedName("meta")
    var meta: AdsMetaEntity
)

data class AdsMetaEntity(
    @Expose
    @SerializedName("page_count")
    var pageCount: Int,
    @Expose
    @SerializedName("total_results")
    var totalResults: Int,
    @Expose
    @SerializedName("current_page_no")
    var currentPageNo: Int,
    @Expose
    @SerializedName("limit")
    var limit: Int,
    @Expose
    @SerializedName("last_page")
    var lastPage: Boolean
)

data class AdsResultsEntity(
    @Expose
    @SerializedName("Fullscreen")
    var fullscreen: FullscreenEntity,

    @Expose
    @SerializedName("Banner")
    var banner: FullscreenEntity
)

data class FullscreenEntity(
    @Expose
    @SerializedName("ad_refresh_time")
    val adRefreshTime: String,
    @Expose
    @SerializedName("photo")
    var photoEntity: List<AdsPhotoEntity>
)

data class AdsPhotoEntity(
    @Expose
    @SerializedName("ad_photo")
    val adPhoto: String,
    @Expose
    @SerializedName("ad_url")
    val adUrl: String
)