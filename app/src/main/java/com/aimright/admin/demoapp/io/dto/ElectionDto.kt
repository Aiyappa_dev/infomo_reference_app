package com.aimright.admin.demoapp.io.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ElectionDto(
    @Expose
    @SerializedName("status")
    var status: Boolean,
    @Expose
    @SerializedName("data")
    var data: ElectionDataEntity
)

data class ElectionDataEntity(
    @Expose
    @SerializedName("id")
    var id: String,
    @Expose
    @SerializedName("title")
    val title: String,
    @Expose
    @SerializedName("url")
    val url: String,
    @Expose
    @SerializedName("display_order")
    var displayOrder: Int
)