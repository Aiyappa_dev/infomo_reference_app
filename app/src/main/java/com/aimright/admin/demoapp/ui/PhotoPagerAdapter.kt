package com.aimright.admin.demoapp.ui

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.aimright.admin.demoapp.io.dto.PhotoEntity

class PhotoPagerAdapter(
    private val albumTitle: String,
    private val photoList: List<PhotoEntity>,
    fragmentManager: FragmentManager
) :
    FragmentPagerAdapter(fragmentManager) {


    override fun getItem(position: Int): Fragment =
        PhotosDetailFragment.newInstance(photoList[position].photoUrls, albumTitle)

    override fun getCount(): Int = photoList.size

}