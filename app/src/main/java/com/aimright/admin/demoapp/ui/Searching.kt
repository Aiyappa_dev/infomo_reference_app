package com.aimright.admin.demoapp.ui

import android.os.Bundle
import com.aimright.admin.demoapp.R
import com.aimright.admin.demoapp.base.BaseActivity

class Searching : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_searching)
        val p0=intent.getStringExtra("text")
        val beginTransaction = supportFragmentManager.beginTransaction()
        beginTransaction.replace(R.id.fragment_container, SearchFragment(p0))
        beginTransaction.commit()
    }
}
