package com.aimright.admin.demoapp.ui

import android.os.Bundle
import com.aimright.admin.demoapp.R
import com.aimright.admin.demoapp.base.BaseActivity

class VideoActivity : BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val beginTransaction = supportFragmentManager.beginTransaction()
        beginTransaction.replace(R.id.fragment_container, VideoFragment())
        beginTransaction.commit()
    }
}