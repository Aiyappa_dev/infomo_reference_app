package com.aimright.admin.demoapp.ui

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.aimright.admin.demoapp.R
import com.aimright.admin.demoapp.io.dto.*
import com.aimright.admin.demoapp.ui.youtube.FullscreenDemoActivity
import com.aimright.admin.demoapp.ui.youtube.YoutubeWebviewActivity
import com.squareup.picasso.Picasso

const val CATEGORY_NEWS_TYPE = 0
const val CATEGORY_PHOTOS_TYPE = 1
const val CATEGORY_VIDEOS_TYPE = 2
const val CATEGORY_HEADER_TYPE = 3
const val SHOW_MORE_TYPE = 4

class CategoryAdapter(
    var categoryResponse: CategoryResponse,
    private val isHomeCategory: Boolean,
    private val categoryId: String,
    private val categotyItemClickCallback: CategotyItemClickCallback
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    private val list = if (categoryResponse.data.newsEntity.isNotEmpty())
        categoryResponse.data.newsEntity + ShowMoreItem("") + categoryResponse.data.videosEntity +
                categoryResponse.data.photosEntity
    else
        categoryResponse.data.newsEntity + categoryResponse.data.videosEntity +
                categoryResponse.data.photosEntity


    private val showMorePosition =
        if (categoryResponse.data.newsEntity.isNotEmpty()) (categoryResponse.data.newsEntity.size)
        else -999


    private val videoHeaderPosition = if (categoryResponse.data.newsEntity.isNotEmpty())
        (categoryResponse.data.newsEntity.size + 1)
    else categoryResponse.data.newsEntity.size


    private val photoHeaderPosition =if (categoryResponse.data.newsEntity.isNotEmpty())
        (categoryResponse.data.newsEntity.size + 1) + categoryResponse.data.videosEntity.size
    else
        (categoryResponse.data.newsEntity.size ) + categoryResponse.data.videosEntity.size


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == CATEGORY_NEWS_TYPE) NewsViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.news_list_item, parent, false)
        )
        else if (viewType == CATEGORY_PHOTOS_TYPE) PhotosViewHolder(
            (LayoutInflater.from(parent.context).inflate(R.layout.photos_list_item, parent, false))
        ) else if (viewType == CATEGORY_HEADER_TYPE) {
            NewsViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.home_screen_news_list_item, parent, false)
            )
        } else if (viewType == SHOW_MORE_TYPE) {
            ShowMoreViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.show_more_list_item, parent, false))
        } else NewsViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.home_screen_news_list_item, parent, false)
        )
//        )
//        return NewsViewHolder(
//            LayoutInflater.from(parent.context).inflate(R.layout.category_list_item, parent, false)
//        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun getItemViewType(position: Int): Int {

        return when {
            showMorePosition == position -> SHOW_MORE_TYPE
            position == 0 && list[position] is NewsEntity -> CATEGORY_HEADER_TYPE
            list[position] is NewsEntity -> CATEGORY_NEWS_TYPE
            list[position] is PhotosEntity -> CATEGORY_PHOTOS_TYPE
            else -> CATEGORY_VIDEOS_TYPE
        }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, pos: Int) {
        when (getItemViewType(pos)) {
            CATEGORY_NEWS_TYPE, CATEGORY_HEADER_TYPE -> {
                val newsItem = list[pos] as NewsEntity
                viewHolder.itemView.setOnClickListener {
                    val intent = Intent(viewHolder.itemView.context, NewsDetailPagerActivity::class.java)
                    intent.putParcelableArrayListExtra(NEWS_LIST, categoryResponse.data.newsEntity)
                    intent.putExtra(NEWS_SELECTED_POSITION, pos)
                    viewHolder.itemView.context.startActivity(intent)
                }
                val newsViewHolder = viewHolder as NewsViewHolder

                newsViewHolder.title.text = newsItem.newsTitle
                val now = System.currentTimeMillis()

                val relativeTimeSpanString = DateUtils.getRelativeTimeSpanString(
                    newsItem.timeStamp.time,
                    now,
                    DateUtils.MINUTE_IN_MILLIS
                )
                newsViewHolder.timestampTv.text = relativeTimeSpanString

                if (newsItem.thumbnails.isNotEmpty() && newsItem.thumbnails[0].isNotEmpty()) {
                    Picasso.with(newsViewHolder.itemView.context)
                        .load(newsItem.thumbnails[0])
                        .fit()
                        .centerCrop()
                        .placeholder(R.drawable.ic_more_horiz_grey)
                        .into(newsViewHolder.newsListImageView)
                } else {
                    newsViewHolder.newsListImageView.setImageResource(R.drawable.ic_more_horiz_grey)
                }

                if (isHomeCategory) {
                    newsViewHolder.moreIv.visibility = View.VISIBLE
                    newsViewHolder.moreIv.setOnClickListener {
                        categotyItemClickCallback.onNewsItemClicked(newsItem)
                    }
                } else {
                    newsViewHolder.moreIv.visibility = View.GONE
                }

                newsViewHolder.headerLayout.visibility = View.GONE

                if (newsItem.author != null) {
                    val authorText = "Reported By - " + newsItem.author
                    newsViewHolder.authorTextView.text = authorText
                }
                newsViewHolder.categoryTextView.visibility = View.GONE
            }
            CATEGORY_PHOTOS_TYPE -> {
                val photosViewHolder = viewHolder as PhotosViewHolder
                val photoItem = list[pos] as PhotosEntity


                viewHolder.itemView.setOnClickListener {
                    val intent = Intent(viewHolder.itemView.context, PhotosDetailActivity::class.java)
                    intent.putParcelableArrayListExtra(PHOTOS_LIST, photoItem.photoEntity)
                    intent.putExtra(ALBUM_TITLE, photoItem.albumTitle)
                    viewHolder.itemView.context.startActivity(intent)
                }
                photosViewHolder.phototitle.text = photoItem.albumTitle
                Picasso.with(viewHolder.itemView.context)
                    .load(photoItem.thumbnail)
                    .fit()
                    .centerCrop()
                    .placeholder(R.drawable.ic_more_horiz_grey)
                    .into(photosViewHolder.photoImage)
                photosViewHolder.albumCount.text = "1/${photoItem.photoEntity.size}"

                if (photoHeaderPosition == pos) {
                    photosViewHolder.headerLayout.visibility = View.VISIBLE
                } else
                    photosViewHolder.headerLayout.visibility = View.GONE
            }
            CATEGORY_VIDEOS_TYPE -> {
                val catviewHolder = viewHolder as NewsViewHolder
                val videoItem = list[pos] as CategoryResponseVideosEntity

                catviewHolder.videoDurationTv.visibility = View.VISIBLE
                catviewHolder.videoDurationTv.text = videoItem.getDurationString()

                catviewHolder.itemView.setOnClickListener {

                    val intent = Intent(
                        catviewHolder.itemView.context,
                        YoutubeWebviewActivity::class.java
                    )
                    intent.putExtra(FullscreenDemoActivity.YOUTUBE_ID, videoItem.videoId)
                    catviewHolder.itemView.context.startActivity(intent)
                }
                catviewHolder.title.text = videoItem.videoTitle
                val now = System.currentTimeMillis()

                val relativeTimeSpanString = DateUtils.getRelativeTimeSpanString(
                    videoItem.timeStamp.time,
                    now,
                    DateUtils.SECOND_IN_MILLIS, DateUtils.FORMAT_ABBREV_ALL
                )
                catviewHolder.timestampTv.text = relativeTimeSpanString

                Picasso.with(catviewHolder.itemView.context)
                    .load(videoItem.videoThumbnail)
                    .fit()
                    .centerCrop()
                    .placeholder(R.drawable.ic_more_horiz_grey)
                    .into(catviewHolder.newsListImageView)

//                catviewHolder.moreIv.setOnClickListener {
//                    categotyItemClickCallback.onVideoItemClicked(videoItem)
//                }
                if (isHomeCategory) {
                    catviewHolder.moreIv.setOnClickListener {
                        categotyItemClickCallback.onVideoItemClicked(videoItem)
                    }
                    catviewHolder.moreIv.visibility = View.VISIBLE
                } else {
                    catviewHolder.moreIv.visibility = View.GONE
                }

                if (videoHeaderPosition == pos) {
                    catviewHolder.headerLayout.visibility = View.VISIBLE
                } else
                    catviewHolder.headerLayout.visibility = View.GONE

            }
            SHOW_MORE_TYPE -> {
                val showMoreViewHolder = viewHolder as ShowMoreViewHolder
                showMoreViewHolder.itemView.setOnClickListener {
                    categotyItemClickCallback.onShowMoreClickHandle(categoryId)
                }
            }
        }
    }


//    inner class NewsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
//        val title = itemView.findViewById(R.id.category_title_tv) as TextView
//        val timestampTv = itemView.findViewById(R.id.news_timestamp) as TextView
//        val newsListImageView = itemView.findViewById(R.id.news_list_IV) as ImageView
//        val moreIv: ImageView = itemView.findViewById(R.id.more_IV)
//        val headerLayout = itemView.findViewById(R.id.header) as LinearLayout
//    }

    inner class NewsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title = itemView.findViewById(R.id.category_title_tv) as TextView
        val timestampTv = itemView.findViewById(R.id.news_timestamp) as TextView
        val newsListImageView = itemView.findViewById(R.id.news_list_IV) as ImageView
        val moreIv: ImageView = itemView.findViewById(R.id.more_IV)
        val videoDurationTv = itemView.findViewById(R.id.videoDuration) as TextView
        val authorTextView = itemView.findViewById(R.id.reportedBy) as TextView
        val categoryTextView = itemView.findViewById(R.id.category) as TextView
        val headerLayout = itemView.findViewById(R.id.header) as LinearLayout

    }


    inner class PhotosViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val photoImage = itemView.findViewById(R.id.photo_IV) as ImageView
        val phototitle = itemView.findViewById(R.id.photoTv) as TextView
        val albumCount = itemView.findViewById(R.id.albumCount) as TextView
        val headerLayout = itemView.findViewById(R.id.header) as LinearLayout

    }

    inner class ShowMoreViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textView = itemView.findViewById(R.id.showMoreBtn) as TextView
        val showMoreLayout = itemView.findViewById(R.id.showMoreLayout) as LinearLayout
    }


    fun setData(categoryResponse: CategoryResponse) {
        this.categoryResponse = categoryResponse
    }
}