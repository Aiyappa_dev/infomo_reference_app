package com.aimright.admin.demoapp.ui.youtube

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.aimright.admin.demoapp.R
import com.aimright.admin.demoapp.base.BaseFragment
import kotlinx.android.synthetic.main.activity_youtube_webview.*


class YoutubeWebviewVideoFragment : BaseFragment() {


    companion object {
        fun newInstance(videoId: String): YoutubeWebviewVideoFragment =
            YoutubeWebviewVideoFragment().apply {
                arguments = Bundle().apply {
                    putString(FullscreenDemoActivity.YOUTUBE_ID, videoId)
                }
            }
    }

    lateinit var videoId: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return layoutInflater.inflate(R.layout.activity_youtube_webview, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        videoId = arguments?.getString(FullscreenDemoActivity.YOUTUBE_ID)!!

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        liveTvProgressBar.visibility = View.GONE

        youtubeWV.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                return false
            }

            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                return super.shouldOverrideUrlLoading(view, request)
            }
        }


        val webSettings = youtubeWV.settings
        webSettings.javaScriptEnabled = true
        webSettings.loadWithOverviewMode = true
        webSettings.useWideViewPort = true
        liveTvProgressBar.visibility = View.GONE

        youtubeWV.loadUrl("https://www.youtube.com/embed/$videoId")
    }


    override fun onPause() {
        super.onPause()
        if (youtubeWV != null)
            youtubeWV.onPause()
    }

    override fun onResume() {
        super.onResume()
        if (youtubeWV != null)
            youtubeWV.onResume()
    }

    override fun setUserVisibleHint(visible: Boolean) {
        super.setUserVisibleHint(visible)
        if (visible && isResumed) {
            //Only manually call onResume if fragment is already visible
            //Otherwise allow natural fragment lifecycle to call onResume
            onResume()
        } else {
            onPause()
        }
    }
}