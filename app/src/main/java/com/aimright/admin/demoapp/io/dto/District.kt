package com.aimright.admin.demoapp.io.dto

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class District(
    @Expose
    @SerializedName("status")
    var status: Boolean,
    @Expose
    @SerializedName("data")
    var dataEntity: List<NotificationDataEntity1>
)

data class NotificationDataEntity1(
    @Expose
    @SerializedName("id")
    var id: Int,
    @Expose
    @SerializedName("district_name")
    val pushTitle: String,
    @Expose
    @SerializedName("is_visible")
    val isVisible:Boolean

)