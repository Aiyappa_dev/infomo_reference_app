package com.aimright.admin.demoapp.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.aimright.admin.demoapp.base.BaseFragment
import com.aimright.admin.demoapp.io.RetrofitApiServiceBuilder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_news_detail.*


const val ITEM_TYPE = "item_type"

class NewsDetailFragment : BaseFragment() {
    private var html: String = ""
//    lateinit var mGoogleAdView: AdView
    private var isStarted:Boolean=false
    private var Visible:Boolean = false
    private var blankurlLoaded: Boolean = false
    private lateinit var articleId: String
    private lateinit var itemType: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        articleId = arguments?.getString(ARTICLE_ID)!!
        itemType = arguments?.getString(ITEM_TYPE)!!

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(com.aimright.admin.demoapp.R.layout.fragment_news_detail, container, false)

    }

    //about:blank
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        mGoogleAdView = view.findViewById(R.id.NewsDetailAdView)
//        val adRequest = AdRequest.Builder().build()
////                mGoogleAdView.adSize = AdSize.SMART_BANNER
//        mGoogleAdView.loadAd(adRequest)
        back.setOnClickListener {
            this.activity!!.finish()
        }

        news_detail_wv.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {

                if (view?.url == "about:blank") {
                    blankurlLoaded = true
                }

                return if (view?.url != null && (view.url.startsWith("http://") || view.url.startsWith("https://"))) {
                    view.context?.startActivity(
                        Intent(Intent.ACTION_VIEW, Uri.parse(view.url))
                    )
                    true
                } else {
                    false
                }
            }

//            override fun onPageFinished(view: WebView, url: String) {
//                println("onPageFinished: $url")
//                if ("about:blank" == view.tag) {
//                    view.tag =""
//                    view.loadDataWithBaseURL(
//                        "https://twitter.com",
//                        html,
//                        "text/html; charset=utf-8", "utf-8"
//
//                        , ""
//                    )
//                }
//            }

            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                return if (url != null && (url.startsWith("http://") || url.startsWith("https://"))) {
                    context?.startActivity(
                        Intent(Intent.ACTION_VIEW, Uri.parse(url))
                    )
                    true
                } else {
                    false
                }
            }
        }
        news_detail_wv.webChromeClient = WebChromeClient()
        news_detail_wv.settings.javaScriptEnabled = true
        news_detail_wv.settings.setSupportZoom(false)

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            val webView=view.findViewById<WebView>(R.id.news_detail_wv) as WebView
            webView.settings.setWebContentsDebuggingEnabled(true)
        }*/

//        news_detail_wv.settings.
        news_detail_wv.settings.setAppCacheEnabled(true)

        news_detail_wv.settings.loadWithOverviewMode = true
        news_detail_wv.settings.useWideViewPort = true
        news_detail_wv.settings.domStorageEnabled = true
//        news_detail_wv.settings.cacheMode = WebSettings.LOAD_NO_CACHE


//        news_detail_wv.setOnKeyListener { _, _, keyEvent ->
//            if (keyEvent.keyCode == KeyEvent.KEYCODE_BACK && !news_detail_wv.canGoBack()) {
//                false
//            } else if (keyEvent.keyCode == KeyEvent.KEYCODE_BACK && keyEvent.action == MotionEvent.ACTION_UP) {
//                news_detail_wv.goBack()
//                true
//            } else true
//        }
        super.onViewCreated(view, savedInstanceState)
    }
  /*  private fun showInterstitialAd() {
        val  mInterstitialAd = com.google.android.gms.ads.InterstitialAd(context)
        mInterstitialAd.adUnitId = "ca-app-pub-1276883186035608/7393283409"
        mInterstitialAd.loadAd(AdRequest.Builder().build())
        mInterstitialAd.adListener = object : com.google.android.gms.ads.AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                mInterstitialAd.show()
            }
        }
    }*/

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        compsiteDisposable.add(
            RetrofitApiServiceBuilder.getService().fetchNewsDetail(articleId, 2, itemType)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    if (activity != null) {
                        val shareUrl = it.data.shareUrl

//                        if (it.data.photos != null) {
//                            val url: String? =
//                                if (it.data.photos.isNotEmpty() && it.data.photos[0] != null) it.data.photos[0]
//                                else ""
//
////                        else it.data.thumbnails[0]
//                            if (url?.isNotEmpty()!!) {
//                                Picasso.with(context)
//                                    .load(url)
//                                    .fit()
//                                    .centerCrop()
//                                    .placeholder(R.drawable.ic_more_horiz_grey)
//                                    .into(news_detail_IV)
//                            } else {
//                                news_detail_IV.visibility = View.GONE
//                            }
//                        }
//                        news_detail_TV.text = it.data.newsTitle
//                    Log.d("NEWS", "" + it.data.newsBody)
                        html = if (itemType == "news") it.data.newsBody else it.data.videoBody
                        news_detail_wv.loadDataWithBaseURL(
                            "https://twitter.com",
                            html,
                            "text/html; charset=utf-8", "utf-8"

                            , ""
                        )


                        share.setOnClickListener {
                            val sendIntent: Intent = Intent().apply {
                                action = Intent.ACTION_SEND
                                putExtra(Intent.EXTRA_TEXT, shareUrl)
                                type = "text/plain"
                            }
                            startActivity(sendIntent)
                        }
                    }
                }, Throwable::printStackTrace)
        )
    }

    companion object {
        fun newInstance(articleId: String, itemType: String): NewsDetailFragment =
            NewsDetailFragment().apply {
                arguments = Bundle().apply {
                    putString(ARTICLE_ID, articleId)
                    putString(ITEM_TYPE, itemType)
                }
            }
    }

    override fun onPause() {
        super.onPause()
        if (news_detail_wv != null)
            news_detail_wv.onPause()
    }



    override fun onResume() {
        super.onResume()
        if (news_detail_wv != null) {
            news_detail_wv.onResume()

            if (blankurlLoaded) {
                if (html.isNotBlank())
                    news_detail_wv.loadDataWithBaseURL(
                        "https://twitter.com",
                        html,
                        "text/html; charset=utf-8", "utf-8"

                        , ""
                    )
                blankurlLoaded = false
            }
        }
    }

    override fun onStart() {
        super.onStart()
        isStarted = true
        if (Visible && isStarted) {
            viewDidAppear()
        }
    }

    override fun setUserVisibleHint(visible: Boolean) {
        super.setUserVisibleHint(visible)
        Visible = visible
        if (isStarted && Visible) {
            viewDidAppear()
        }

        if (visible && isResumed) {
            //Only manually call onResume if fragment is already visible
            //Otherwise allow natural fragment lifecycle to call onResume
            onResume()
        } else {
            onPause()
        }
    }

    private fun viewDidAppear() {
        //NewsDetailAdView.refreshAd(NewsDetailAdView, "320X50")
    }

    override fun onStop() {
        super.onStop()
        isStarted = false
    }
}