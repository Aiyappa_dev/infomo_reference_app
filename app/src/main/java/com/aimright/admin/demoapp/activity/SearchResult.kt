package com.aimright.admin.demoapp.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.aimright.admin.demoapp.R
import android.support.v7.widget.LinearLayoutManager
import com.aimright.admin.demoapp.interfaces.ApiInterface
import com.aimright.admin.demoapp.model.search.News
import com.aimright.admin.demoapp.model.search.searchData
import com.aimright.admin.demoapp.util.ApiClient1
import kotlinx.android.synthetic.main.activity_search_result.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SearchResult : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_result)
        val text = intent.getStringExtra("text")

        categoryR.setHasFixedSize(false)
        categoryR.layoutManager = LinearLayoutManager(this)


        loadData(text)


    }

    private fun loadData(text: String?) {
        val apiInterface = ApiClient1.getApiClient().create(
            ApiInterface::class.java)


        val call: Call<searchData>
        // call = apiInterface.getNewsCC("in",countryVal, API_KEY);

        call = apiInterface.fetchSearch(text)
        call.enqueue(object : Callback<searchData> {
            override fun onResponse(call: Call<searchData>, response: Response<searchData>) {

                val data = response.body()!!.data.news as Array<News>


            }


            override fun onFailure(call: Call<searchData>, t: Throwable) {

            }
        })
    }
}
