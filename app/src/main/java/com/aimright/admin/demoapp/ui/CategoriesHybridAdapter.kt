package com.aimright.admin.demoapp.ui

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.aimright.admin.demoapp.R
import com.aimright.admin.demoapp.io.dto.CategoriesResponse
import com.squareup.picasso.Picasso

class CategoriesHybridAdapter(var categoriesResponse: CategoriesResponse) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return NewsViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.news_list_item, parent, false))

    }

    override fun getItemCount(): Int {
        return categoriesResponse.data.results.size
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, pos: Int) {
        val newsViewHolder = viewHolder as NewsViewHolder
        val newsItem = categoriesResponse.data.results[pos]
        newsViewHolder.title.text = newsItem.getTitle()
        newsViewHolder.itemView.setOnClickListener() {
            val intent = Intent(viewHolder.itemView.context, NewsDetailPagerActivity::class.java)
            intent.putParcelableArrayListExtra(NEWS_LIST, categoriesResponse.data.results)
            intent.putExtra(NEWS_SELECTED_POSITION, pos)
            viewHolder.itemView.context.startActivity(intent)
        }

        val now = System.currentTimeMillis()

        val relativeTimeSpanString = DateUtils.getRelativeTimeSpanString(
            newsItem.timeStamp?.time!!,
            now,
            DateUtils.MINUTE_IN_MILLIS
        )
        newsViewHolder.timestampTv.text = relativeTimeSpanString


        if (newsItem.getThumbNail().isNotEmpty()) {
            Picasso.with(newsViewHolder.itemView.context)
                .load(newsItem.getThumbNail())
                .fit()
                .centerCrop()
                .placeholder(R.drawable.ic_more_horiz_grey)
                .into(newsViewHolder.newsListImageView)
        } else {
            newsViewHolder.newsListImageView.setImageResource(R.drawable.ic_more_horiz_grey)
        }
        newsViewHolder.moreIv.visibility = View.GONE
        newsViewHolder.headerLayout.visibility = View.GONE
        if (newsItem.Author != null) {
            val authorText = "Reported By - " + newsItem.Author
            newsViewHolder.authorTextView.text = authorText
        }
        newsViewHolder.categoryTextView.visibility = View.GONE

//        if(newsItem.itemType == VIDEO_ITEM_TYPE) {
//            newsViewHolder.videoDurationTv.visibility = View.VISIBLE
//            newsViewHolder.videoDurationTv.text = newsItem.getDurationString()
//        }else{
//            newsViewHolder.videoDurationTv.visibility = View.GONE
//
//        }

        newsViewHolder.videoDurationTv.visibility = View.GONE

    }

    inner class NewsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title = itemView.findViewById(R.id.category_title_tv) as TextView
        val timestampTv = itemView.findViewById(R.id.news_timestamp) as TextView
        val newsListImageView = itemView.findViewById(R.id.news_list_IV) as ImageView
        val moreIv: ImageView = itemView.findViewById(R.id.more_IV)
        val videoDurationTv = itemView.findViewById(R.id.videoDuration) as TextView
        val authorTextView = itemView.findViewById(R.id.reportedBy) as TextView
        val categoryTextView = itemView.findViewById(R.id.category) as TextView
        val headerLayout = itemView.findViewById(R.id.header) as LinearLayout

    }

}
