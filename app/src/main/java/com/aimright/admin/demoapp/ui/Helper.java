package com.aimright.admin.demoapp.ui;

import com.aimright.admin.demoapp.io.dto.AdsPhotoEntity;
import io.reactivex.Observable;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class Helper {


    public static Observable<AdsPhotoEntity> subscribeAndReturn(List<AdsPhotoEntity> adsPhotoList) {

        return Observable.fromIterable(adsPhotoList)
                .concatMap(item -> Observable.interval(4, TimeUnit.SECONDS)
                        .take(1)
                        .map(second -> item));
    }
}
