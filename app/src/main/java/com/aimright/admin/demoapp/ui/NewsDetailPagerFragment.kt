package com.aimright.admin.demoapp.ui

import android.os.Bundle
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.aimright.admin.demoapp.R
import com.aimright.admin.demoapp.base.BaseFragment
import com.aimright.admin.demoapp.io.dto.Results
import com.aimright.admin.infomosdk.apis.InfomoSDK
import com.aimright.admin.infomosdk.utils.LogE
import com.aimright.admin.infomosdk.utils.OutsideAppZones
import com.aimright.admin.infomosdk.view.InfomoBannerView
import com.aimright.admin.infomosdk.view.InfomoInterstitialAd
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.doubleclick.PublisherAdRequest
import com.google.android.gms.ads.doubleclick.PublisherInterstitialAd
import kotlinx.android.synthetic.main.fragment_pager_photos_detail.*


const val NEWS_LIST = "news_list"
const val NEWS_SELECTED_POSITION = "news_selected_position"


class NewsDetailPagerFragment : BaseFragment() {
  //  lateinit var facebookAdView: com.facebook.ads.AdView
  //  lateinit var mGoogleAdView: com.google.android.gms.ads.AdView

  //  private lateinit var  mPublisherInterstitialAd: PublisherInterstitialAd
    lateinit var newsList: ArrayList<Results>
  //  lateinit var facebookInterstitialAd: InterstitialAd

    companion object {
        fun newInstance(
            newsList: ArrayList<Results>,
            newsSelectedPosition: Int
        ): NewsDetailPagerFragment =
            NewsDetailPagerFragment().apply {
                arguments = Bundle().apply {
                    putParcelableArrayList(NEWS_LIST, newsList)
                    putInt(NEWS_SELECTED_POSITION, newsSelectedPosition)
                }
            }
    }

    private var newsSelectedPosition: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        newsList = arguments?.getParcelableArrayList(NEWS_LIST)!!
        newsSelectedPosition = arguments?.getInt(NEWS_SELECTED_POSITION)!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view= inflater.inflate(R.layout.fragment_pager_photos_detail, container, false)
       /* val toolbar = view.findViewById(R.id.toolbar) as Toolbar



        //for crate home button
        val activity = activity as AppCompatActivity?
        activity!!.setSupportActionBar(toolbar)
        activity.supportActionBar!!.setDisplayHomeAsUpEnabled(true)*/

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadFacebookAd()
      /*  back.setOnClickListener {
            activity!!.onBackPressed()
        }*/

       // mGoogleAdView = view.findViewById(R.id.NewsDetailAdView)
        MobileAds.initialize(context)
       // val  mAdView = view!!.findViewById<com.google.android.gms.ads.AdView>(R.id.adView)
     //   mAdView.visibility=View.VISIBLE
        //adView.refreshAd()
        val adRequest = AdRequest.Builder().build()
       // mAdView.loadAd(adRequest)
        /*val  mAdView = view!!.findViewById<AdView>(R.id.NewsDetailAdView)
        val adRequest = AdRequest.Builder().build()
        mAdView.loadAd(adRequest)*/


        photos_detail_pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) {

            }

            override fun onPageScrolled(position: Int, p1: Float, p2: Int) {

            }

            override fun onPageSelected(position: Int) {
               /* if ((position+1)%5==0 && position>0)
                {
                    showInterstitialAd()
                }

                loadFacebookAd()*/
                if (position % 4 == 0) {
                  //  val infomoSDK = InfomoSDK.instance().init(activity)
                   // infomoSDK.showAd()

                  /*  mPublisherInterstitialAd = PublisherInterstitialAd(activity)
                    mPublisherInterstitialAd.adUnitId = "/13406045/Infomo_APP/Infomo_APP_Interestial"
                    mPublisherInterstitialAd.loadAd(PublisherAdRequest.Builder().build())
                    mPublisherInterstitialAd.adListener = object: AdListener() {
                        override fun onAdLoaded() {
                            // Code to be executed when an ad finishes loading.
                            if (mPublisherInterstitialAd.isLoaded) {
                                mPublisherInterstitialAd.show()
                            } else {
                                Log.d("TAG", "The interstitial wasn't loaded yet.")
                            }

                        }



                        override fun onAdOpened() {
                            // Code to be executed when the ad is displayed.
                        }

                        override fun onAdClicked() {
                            // Code to be executed when the user clicks on an ad.
                        }

                        override fun onAdLeftApplication() {
                            // Code to be executed when the user has left the app.
                        }

                        override fun onAdClosed() {
                            // Code to be executed when the interstitial ad is closed.
                        }
                    }*/

                    val infomoAdView = InfomoInterstitialAd(activity)
                    val outsideAppZones: OutsideAppZones =
                        InfomoSDK.getInstance(activity).getPublisherInfo().getOutsideAppZones()
                    val notification_interstitial = outsideAppZones.notificationInterstitial;
                    infomoAdView.zoneid = notification_interstitial
                    val userInfo =
                        InfomoSDK.getInstance(activity).userInfo
                    if (userInfo != null) {
                        val userProfileInfo = userInfo.profileInfo
                        if (userProfileInfo != null && !userProfileInfo.isEmpty()) {
                            infomoAdView.profileInfo = userProfileInfo
                        } else {
                            infomoAdView.profileInfo = "age=25&gender=Male&height=165&weight=60"
                        }
                    } else {
                        infomoAdView.profileInfo = "age=25&gender=Male&height=165&weight=60"
                    }

                    infomoAdView.keywords = "Sports,Politics,Art,Movies,Books"
                    infomoAdView.LoadAd()
                }
            }


        })
        photos_detail_pager.adapter = NewsPagerAdapter(newsList, childFragmentManager)
        photos_detail_pager.currentItem = newsSelectedPosition
      /*  val toolbar: Toolbar = view.findViewById(R.id.toolbar)
        toolbar.setNavigationIcon(R.drawable.ic_back)
        toolbar.setNavigationOnClickListener { activity!!.onBackPressed() }*/
        //facebookInterstitialAd = InterstitialAd(context, "122154748214193_710112042751791")
        //facebookInterstitialAd.loadAd()
       /* facebookInterstitialAd.setAdListener(object : AdListener, InterstitialAdListener {
            override fun onAdClicked(p0: Ad?) {
                Log.d("Facebook ad", p0.toString())
            }

            override fun onError(p0: Ad?, p1: AdError?) {
                Log.d("Facebook ad", p0.toString() + p1.toString())
//                facebookInterstitialAd.loadAd()


            }

            override fun onAdLoaded(p0: Ad?) {
                Log.d("Facebook ad", p0.toString())

            }

            override fun onLoggingImpression(p0: Ad?) {
            }

            override fun onInterstitialDisplayed(p0: Ad?) {
            }

            override fun onInterstitialDismissed(p0: Ad?) {
                facebookInterstitialAd.loadAd()
            }
        })

       
        photos_detail_pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) {

            }

            override fun onPageScrolled(position: Int, p1: Float, p2: Int) {

            }

            override fun onPageSelected(position: Int) {
                if ((position+1)%5==0 && position>0)
                {
                    showInterstitialAd()
                }

                loadFacebookAd()
                if (position % 4 == 0) {
                    if (facebookInterstitialAd.isAdLoaded) {
                        facebookInterstitialAd.show()

                    }
                }
            }


        })*/
    }

    private fun showInterstitialAd() {
        val  mInterstitialAd = com.google.android.gms.ads.InterstitialAd(context)
        mInterstitialAd.adUnitId = "ca-app-pub-1276883186035608/7393283409"
        mInterstitialAd.loadAd(AdRequest.Builder().build())
        mInterstitialAd.adListener = object : com.google.android.gms.ads.AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                mInterstitialAd.show()
            }
        }
    }

    private fun loadFacebookAd() {
        /*val adRequest = PublisherAdRequest.Builder().build()
         publisherAdView.loadAd(adRequest)*/
        if (news_detail_banner_container != null) {
            try {
                val infomoAdView1 = InfomoBannerView(activity)
                val outsideAppZones: OutsideAppZones =
                    InfomoSDK.getInstance(activity).getPublisherInfo().getOutsideAppZones()
                val notification_banner_bottom = outsideAppZones.getNotificationBannerBottom();
                infomoAdView1.zoneid ="993"
                val userInfo =
                    InfomoSDK.getInstance(activity).userInfo
                if (userInfo != null) {
                    val userProfileInfo = userInfo.profileInfo
                    if (userProfileInfo != null && !userProfileInfo.isEmpty()) {
                        infomoAdView1.profileInfo = userProfileInfo
                    } else {
                        infomoAdView1.profileInfo = "age=25&gender=Male&height=165&weight=60"
                    }
                } else {
                    infomoAdView1.profileInfo = "age=25&gender=Male&height=165&weight=60"
                }
                infomoAdView1.keywords = "Sports,Politics,Art,Movies,Books"
                infomoAdView1.LoadAd()
                news_detail_banner_container.bringToFront()
                news_detail_banner_container.addView(infomoAdView1)
            }
            catch (var6: java.lang.Exception) {
                LogE.printMe(var6)
            }

            //news_detail_banner_container.removeAllViews()
          /*  facebookAdView =
                AdView(context, "122154748214193_710112169418445", AdSize.BANNER_HEIGHT_50)
            news_detail_banner_container.addView(facebookAdView)
            facebookAdView.setAdListener(object : AdListener {
                override fun onAdClicked(p0: Ad?) {
                }

                override fun onError(p0: Ad?, p1: AdError?) {
                    val adRequest = AdRequest.Builder().build()
                    mGoogleAdView.visibility = View.VISIBLE
                    mGoogleAdView.loadAd(adRequest)
                }

                override fun onAdLoaded(p0: Ad?) {
                }

                override fun onLoggingImpression(p0: Ad?) {
                }

            })
            facebookAdView.loadAd()*/
        }


    }

    override fun onDestroy() {
       /* if (::facebookAdView.isInitialized && facebookAdView != null)
            facebookAdView.destroy()
        if (::facebookInterstitialAd.isInitialized && facebookInterstitialAd != null) {
            facebookInterstitialAd.destroy()
        }*/
        super.onDestroy()
    }
}