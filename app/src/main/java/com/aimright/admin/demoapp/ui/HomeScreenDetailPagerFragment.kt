package com.aimright.admin.demoapp.ui

import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.aimright.admin.demoapp.R
import com.aimright.admin.demoapp.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_pager_photos_detail.*

const val HOME_SCREEN_CLICK_POSITION = "home_screen_click_position"

class HomeScreenDetailPagerFragment : BaseFragment() {

    //lateinit var facebookAdView: com.facebook.ads.AdView
   // lateinit var facebookInterstitialAd: InterstitialAd
  //  lateinit var mGoogleAdView: com.google.android.gms.ads.AdView

    var position: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        position = arguments?.getInt(HOME_SCREEN_CLICK_POSITION)!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_pager_photos_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
      //  mGoogleAdView = view.findViewById(R.id.NewsDetailAdView)

        photos_detail_pager.adapter =
            HomeScreenDetailPagerAdapter((activity as HomeScreenActivity).newsData!!, childFragmentManager)
        photos_detail_pager.currentItem = position
        val toolbar: Toolbar = view.findViewById(R.id.toolbar)
        toolbar.setNavigationIcon(R.drawable.ic_back)
        toolbar.setNavigationOnClickListener { activity!!.onBackPressed() }
//        photos_detail_pager.currentItem = 0
        loadFacebookAd()
       /* facebookInterstitialAd = InterstitialAd(context, "122154748214193_710112042751791")
        facebookInterstitialAd.loadAd()
        facebookInterstitialAd.setAdListener(object : AdListener, InterstitialAdListener {
            override fun onAdClicked(p0: Ad?) {
                Log.d("Facebook ad", p0.toString())
            }

            override fun onError(p0: Ad?, p1: AdError?) {
                Log.d("Facebook ad", p0.toString() + p1.toString())
//                facebookInterstitialAd.loadAd()


            }

            override fun onAdLoaded(p0: Ad?) {
                Log.d("Facebook ad", p0.toString())

            }

            override fun onLoggingImpression(p0: Ad?) {
            }

            override fun onInterstitialDisplayed(p0: Ad?) {
            }

            override fun onInterstitialDismissed(p0: Ad?) {
                facebookInterstitialAd.loadAd()
            }
        })
        photos_detail_pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) {

            }

            override fun onPageScrolled(position: Int, p1: Float, p2: Int) {

            }

            override fun onPageSelected(position: Int) {


                loadFacebookAd()
                if (position % 4 == 0) {
                    if (facebookInterstitialAd.isAdLoaded) {
                        facebookInterstitialAd.show()

                    }
                }
            }


        })*/

    }

    private fun loadFacebookAd() {

        //news_detail_banner_container.removeAllViews()
       /* facebookAdView =
            AdView(context, "122154748214193_710112169418445", AdSize.BANNER_HEIGHT_50)
        news_detail_banner_container.addView(facebookAdView)


        facebookAdView.setAdListener(object : AdListener {
            override fun onAdClicked(p0: Ad?) {
            }

            override fun onError(p0: Ad?, p1: AdError?) {
                val adRequest = AdRequest.Builder().build()
                mGoogleAdView.visibility = View.VISIBLE
                mGoogleAdView.loadAd(adRequest)
            }

            override fun onAdLoaded(p0: Ad?) {
            }

            override fun onLoggingImpression(p0: Ad?) {
            }
        })
        facebookAdView.loadAd()*/

    }


    companion object {
        fun newInstance(position: Int): HomeScreenDetailPagerFragment =
            HomeScreenDetailPagerFragment().apply {
                arguments = Bundle().apply {
                    putInt(HOME_SCREEN_CLICK_POSITION, position)
                }
            }
    }

    override fun onDestroy() {
       /* if (::facebookAdView.isInitialized && facebookAdView != null)
            facebookAdView.destroy()
        if(::facebookInterstitialAd.isInitialized && facebookInterstitialAd != null){
            facebookInterstitialAd.destroy()
        }*/
        super.onDestroy()
    }

}