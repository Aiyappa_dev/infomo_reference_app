package com.aimright.admin.demoapp.ui

import com.aimright.admin.demoapp.io.dto.CategoryResponseVideosEntity
import com.aimright.admin.demoapp.io.dto.NewsEntity
import com.aimright.admin.demoapp.io.dto.ResultsEntity

interface CategotyItemClickCallback {
    fun onNewsItemClicked(newsEntity: NewsEntity)
    fun onVideoItemClicked(videosEntity: CategoryResponseVideosEntity)

    fun onNewsDetailClicked(newsItem: ResultsEntity) {}

    fun onSwiPeDisbleEnable(enable: Boolean) {}

    fun onShowMoreClickHandle(categoryId: String) {

    }
}