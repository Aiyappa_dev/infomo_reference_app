package com.aimright.admin.demoapp.ui.youtube

import android.content.res.Configuration
import android.content.res.Resources
import android.graphics.Bitmap
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import com.facebook.ads.*
import com.google.android.gms.ads.AdRequest
import com.aimright.admin.demoapp.R
import com.aimright.admin.demoapp.io.RetrofitApiServiceBuilder
import com.aimright.admin.demoapp.ui.youtube.FullscreenDemoActivity.YOUTUBE_ID
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_youtube_webview.*
import android.webkit.*
import android.webkit.WebChromeClient
import android.webkit.WebViewClient
import com.aimright.admin.infomosdk.apis.InfomoSDK
import com.aimright.admin.infomosdk.djax.adserver.ErrorCodes
import com.aimright.admin.infomosdk.djax.adserver.MInterstitial_Interstitial
import com.aimright.admin.infomosdk.interfaces.InterstitialImageAdListener
import com.aimright.admin.infomosdk.utils.OutsideAppZones
import com.aimright.admin.infomosdk.view.InfomoInterstitialAd
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.doubleclick.PublisherAdRequest
import com.google.android.gms.ads.doubleclick.PublisherInterstitialAd


class YoutubeWebviewActivity : AppCompatActivity(), InstreamVideoAdListener, InterstitialImageAdListener {

   // private lateinit var mPublisherInterstitialAd: PublisherInterstitialAd
    lateinit var facebookAdView: com.facebook.ads.AdView
   // lateinit var m
    // InterstitialAd: InterstitialAd

    lateinit var videoId: String
  //  private var mInstreamVideoAdView: InstreamVideoAdView? = null
    private var mAdViewContainer: RelativeLayout? = null
    private val DENSITY = Resources.getSystem().displayMetrics.density.toDouble()
   // lateinit var mGoogleAdView: com.google.android.gms.ads.AdView

    val compsiteDisposable = CompositeDisposable()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        videoId = intent.getStringExtra(YOUTUBE_ID)

        setContentView(R.layout.activity_youtube_webview)
     //   mGoogleAdView = findViewById(R.id.youtubeAdView)

        youtubeWV.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                return false
            }

            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                return super.shouldOverrideUrlLoading(view, request)
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                liveTvProgressBar.visibility = View.VISIBLE
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                liveTvProgressBar.visibility = View.GONE
            }
        }


        val webSettings = youtubeWV.settings
        webSettings.javaScriptEnabled = true
        webSettings.loadWithOverviewMode = true
        webSettings.useWideViewPort = true
        youtubeWV.settings.mediaPlaybackRequiresUserGesture = false
        youtubeWV.webChromeClient = WebChromeClient()

//        loadData()
        loadInStreamVideo()
        mAdViewContainer = findViewById(R.id.facebookAdViewContainer)

       /* if (mInstreamVideoAdView != null) {
            mInstreamVideoAdView?.destroy()
            mAdViewContainer?.removeAllViews()
        }
        val width = (mAdViewContainer!!.measuredWidth * DENSITY).toInt()
        val height = (mAdViewContainer!!.measuredHeight * DENSITY).toInt()
        mInstreamVideoAdView = InstreamVideoAdView(
            this,
            "122154748214193_710112252751770",
            AdSize(
                (mAdViewContainer!!.measuredWidth * DENSITY).toInt(),
                (mAdViewContainer!!.measuredHeight * DENSITY).toInt()
            )
        )

        mInstreamVideoAdView?.setAdListener(this)
        mInstreamVideoAdView?.loadAd()*/

        showInterstitialAd()
    }

    private fun showInterstitialAd() {
       /* mPublisherInterstitialAd = PublisherInterstitialAd(this)
        mPublisherInterstitialAd.adUnitId = "/13406045/Infomo_APP/Infomo_APP_Interestial"
        mPublisherInterstitialAd.loadAd(PublisherAdRequest.Builder().build())
        mPublisherInterstitialAd.adListener = object: AdListener() {
            override fun onAdLoaded() {
                // Code to be executed when an ad finishes loading.
                if (mPublisherInterstitialAd.isLoaded) {
                    mPublisherInterstitialAd.show()
                } else {
                    Log.d("TAG", "The interstitial wasn't loaded yet.")
                }

            }



            override fun onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            override fun onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            override fun onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            override fun onAdClosed() {
                // Code to be executed when the interstitial ad is closed.
            }
        }*/
        val infomoAdView = InfomoInterstitialAd(this)
        val outsideAppZones: OutsideAppZones =
            InfomoSDK.getInstance(this).getPublisherInfo().getOutsideAppZones()
        val notification_interstitial = outsideAppZones.notificationInterstitial;
        infomoAdView.zoneid = notification_interstitial
        val userInfo =
            InfomoSDK.getInstance(this).userInfo
        if (userInfo != null) {
            val userProfileInfo = userInfo.profileInfo
            if (userProfileInfo != null && !userProfileInfo.isEmpty()) {
                infomoAdView.profileInfo = userProfileInfo
            } else {
                infomoAdView.profileInfo = "age=25&gender=Male&height=165&weight=60"
            }
        } else {
            infomoAdView.profileInfo = "age=25&gender=Male&height=165&weight=60"
        }

        infomoAdView.keywords = "Sports,Politics,Art,Movies,Books"
        infomoAdView.LoadAd()
       // val infomoSDK = InfomoSDK.instance().init(this@YoutubeWebviewActivity)
         //       infomoSDK.showAd()
        /*   mInterstitialAd = InterstitialAd(this)
        mInterstitialAd.adUnitId = "ca-app-pub-1276883186035608/9858988172"
        mInterstitialAd.loadAd(AdRequest.Builder().build())
        mInterstitialAd.adListener = object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                mInterstitialAd.show()
                if (mInstreamVideoAdView!!.isAdLoaded)
                {
                    finish()
                }
            }
        }*/
    }

    private fun loadInStreamVideo() {
        loadData()
    }


    private fun loadData() {
        loadFacebookAd()
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val height = displayMetrics.heightPixels - 100
        val width = displayMetrics.widthPixels
        if (videoId.isEmpty()) {
            liveTvProgressBar.visibility = View.VISIBLE
            compsiteDisposable.add(
                RetrofitApiServiceBuilder.getService().fetchLiveTvUrl()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({
                        if (!isDestroyed) {
                            liveTvProgressBar.visibility = View.GONE
                            youtubeWV.visibility = View.VISIBLE
                            videoId = it.data.liveStreamVideoId
                            if (it.data.liveStreamVideoId.isNotBlank()) {


                                val frameVideo = "<html><body style='margin:0px;padding:0px;'>\n" +
                                        "        <script type='text/javascript' src='http://www.youtube.com/iframe_api'></script><script type='text/javascript'>\n" +
                                        "                var player;\n" +
                                        "        function onYouTubeIframeAPIReady()\n" +
                                        "        {player=new YT.Player('playerId',{events:{onReady:onPlayerReady}})}\n" +
                                        "        function onPlayerReady(event){player.playVideo();}\n" +
                                        "        </script>\n" +
                                        "        <iframe id='playerId' type='text/html' width='$width' height='$height'\n" +
                                        "        src='https://www.youtube.com/embed/" + videoId + "?enablejsapi=1&autoplay=1' frameborder='0'>\n" +
                                        "        </body></html>"
                                youtubeWV.loadDataWithBaseURL(null, frameVideo, "text/html", "utf8", "")
                            } else
                            {
                                    /*"https://content.vidgyor.com/live/midroll/html/publictv.html"*/
                                youtubeWV.loadUrl(it.data.streamUrl)
                            }
                               // youtubeWV.loadUrl(it.data.streamUrl)
//                            youtubeWV.loadUrl("https://www.youtube.com/embed/${it.data.liveStreamVideoId}")
                        }
                    }, Throwable::printStackTrace)
            )
        } else {
            liveTvProgressBar.visibility = View.GONE
            youtubeWV.visibility = View.VISIBLE
//            youtubeWV.loadUrl("https://www.youtube.com/embed/$videoId")
            val frameVideo = "<html><body style='margin:0px;padding:0px;'>\n" +
                    "        <script type='text/javascript' src='http://www.youtube.com/iframe_api'></script><script type='text/javascript'>\n" +
                    "                var player;\n" +
                    "        function onYouTubeIframeAPIReady()\n" +
                    "        {player=new YT.Player('playerId',{events:{onReady:onPlayerReady}})}\n" +
                    "        function onPlayerReady(event){player.playVideo();}\n" +
                    "        </script>\n" +
                    "        <iframe id='playerId' type='text/html' width='$width' height='$height'\n" +
                    "        src='https://www.youtube.com/embed/" + videoId + "?enablejsapi=1&autoplay=1' frameborder='0'>\n" +
                    "        </body></html>"
            youtubeWV.loadDataWithBaseURL(null, frameVideo, "text/html", "utf8", "")


        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
      //  showInterstitialAd()

    }

    override fun onDestroy() {
        if (youtubeWV != null) {
            youtubeWV.clearCache(true)
            youtubeWV.destroy()
        }
        compsiteDisposable.clear()
        //mInstreamVideoAdView?.destroy()
       // mAdViewContainer?.removeView(mInstreamVideoAdView)
        super.onDestroy()
    }

    override fun onAdClicked(p0: Ad?) {
    }

    override fun onAdVideoComplete(p0: Ad?) {
      //  mAdViewContainer?.removeView(mInstreamVideoAdView)
        mAdViewContainer?.visibility = View.GONE

        loadData()
    }

    override fun onError(p0: Ad?, p1: AdError?) {
       // mAdViewContainer?.removeView(mInstreamVideoAdView)
        mAdViewContainer?.visibility = View.GONE
        loadData()
    }

    override fun onAdLoaded(p0: Ad?) {
        mAdViewContainer?.visibility = View.VISIBLE
     //   mAdViewContainer?.addView(mInstreamVideoAdView)
        liveTvProgressBar.visibility = View.GONE
      //  mInstreamVideoAdView!!.show()

    }

    override fun onLoggingImpression(p0: Ad?) {
    }


     override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        if (newConfig!!.orientation == Configuration.ORIENTATION_LANDSCAPE) {
           // this.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
          //  mGoogleAdView.visibility=View.GONE
        }
        else{
            //mGoogleAdView.visibility=View.VISIBLE

        }

    }

    private fun loadFacebookAd() {

        val adRequest = AdRequest.Builder().build()
      //  youtubeAdView?.visibility = View.VISIBLE
        //youtubeAdView?.loadAd(adRequest)

        /*if (youtube_banner_container != null) {
            facebookAdView =
                AdView(this, "122154748214193_710112169418445", AdSize.BANNER_HEIGHT_50)
            youtube_banner_container?.addView(facebookAdView)
            facebookAdView.setAdListener(object : AdListener {
                override fun onAdClicked(p0: Ad?) {
                }

                override fun onError(p0: Ad?, p1: AdError?) {
                    val adRequest = AdRequest.Builder().build()
                    youtubeAdView?.visibility = View.VISIBLE
                    youtubeAdView?.loadAd(adRequest)
                }

                override fun onAdLoaded(p0: Ad?) {
                }

                override fun onLoggingImpression(p0: Ad?) {
                }

            })
            facebookAdView.loadAd()
    }*/
    }

    override fun onInterstitialAdShown(p0: MInterstitial_Interstitial?) {

    }

    override fun onInterstitialAdLoaded(p0: MInterstitial_Interstitial?) {
    }

    override fun onInterstitialAdDismissed(p0: MInterstitial_Interstitial?) {
    }

    override fun onInterstitialAdClicked(p0: MInterstitial_Interstitial?) {
    }

    override fun onInterstitialAdFailed(p0: MInterstitial_Interstitial?, p1: ErrorCodes?) {
    }
}
