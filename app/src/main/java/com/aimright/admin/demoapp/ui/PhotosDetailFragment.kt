package com.aimright.admin.demoapp.ui

import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.ads.AdRequest.LOGTAG
import com.aimright.admin.demoapp.R
import com.aimright.admin.demoapp.base.BaseFragment
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import com.squareup.picasso.Transformation
import kotlinx.android.synthetic.main.fragment_photos_detail.*


const val PHOTO_URL = "photo_url"

class PhotosDetailFragment : BaseFragment() {


    private var photoUrl: String? = null
    private var albumDisplayTitle: String? = null
//    lateinit var mGoogleAdView: AdView
    private val transformation = object : Transformation {

        override fun transform(source: Bitmap): Bitmap {
            val targetWidth = photo_detail_IV.width

            val aspectRatio = source.height.toDouble() / source.width.toDouble()
            val targetHeight = (targetWidth * aspectRatio).toInt()
            val result = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false)
            if (result != source) {
                // Same bitmap is returned if sizes are the same
                source.recycle()
            }
            return result
        }

        override fun key(): String {
            return "transformation" + " desiredWidth"
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        photoUrl = arguments?.getString(PHOTO_URL)
        albumDisplayTitle = arguments?.getString(ALBUM_TITLE)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_photos_detail, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        /*Business IAB Code
        IAB3-0

        Fashion IAB COde
IAB18-3 */
        super.onViewCreated(view, savedInstanceState)

//            Picasso.with(context)
//                .load(photoUrl)
//                .fit()
//                .placeholder(R.drawable.ic_more_horiz_grey)
//                .into(photo_detail_IV)
        albumTitle.text = albumDisplayTitle
//        mGoogleAdView = view.findViewById(R.id.adView)
//        val adRequest = AdRequest.Builder().build()
//        mGoogleAdView.loadAd(adRequest)
        //adView.refreshAd(adView, "320X50")

        photo_detail_IV.post {
            if (photoUrl != null) {

                Picasso.with(this.context)
                    .load(photoUrl)
                    .error(android.R.drawable.stat_notify_error)
                    .transform(transformation)
                    .into(photo_detail_IV, object : Callback {
                        override fun onSuccess() {
                        }

                        override fun onError() {
                            Log.e(LOGTAG, "error")
//                    holder.progressBar_picture.setVisibility(View.GONE)
                        }
                    })
            }
        }


    }


    companion object {
        fun newInstance(photoUrl: String?, albumTitle: String): PhotosDetailFragment = PhotosDetailFragment().apply {
            arguments = Bundle().apply {
                putString(PHOTO_URL, photoUrl)
                putString(ALBUM_TITLE, albumTitle)
            }
        }

    }
}