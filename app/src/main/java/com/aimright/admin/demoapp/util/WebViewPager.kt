package com.aimright.admin.infomo.util

import android.content.Context
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View


class WebViewPager(context: Context, attrs: AttributeSet) : ViewPager(context, attrs) {

    private var isPagingEnabled = true


    override fun onTouchEvent(event: MotionEvent): Boolean {


        return this.isPagingEnabled && super.onTouchEvent(event)
    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {

        try {
            return this.isPagingEnabled && super.onInterceptTouchEvent(event)
        } catch (exception: IllegalArgumentException) {
            exception.printStackTrace()
        }

        return false

    }

    override fun canScroll(v: View, checkV: Boolean, dx: Int, x: Int, y: Int): Boolean {
        return this.isPagingEnabled && super.canScroll(v, checkV, dx, x, y)
    }

    fun setPagingEnabled(b: Boolean) {
        this.isPagingEnabled = b
    }

    override fun removeView(view: View) {
        super.removeView(view)
    }


    fun getPagingStatus(): Boolean? {
        return isPagingEnabled
    }

}