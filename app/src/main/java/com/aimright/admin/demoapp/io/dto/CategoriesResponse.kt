package com.aimright.admin.demoapp.io.dto

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*
import kotlin.collections.ArrayList

const val NEWS_ITEM_TYPE = "news"
const val VIDEO_ITEM_TYPE = "video"
const val CATEGORY_HYBRID_TYPE ="hybrid"

//data class CategoriesResponse(
//    @Expose
//    @SerializedName("status")
//    var Status: Boolean,
//    @Expose
//    @SerializedName("data")
//    var Data: Data
//)
//
//data class Data(
//    @Expose
//    @SerializedName("results")
//    var Results: List<Results>,
//    @Expose
//    @SerializedName("meta")
//    var Meta: Meta
//)
//
//data class Meta(
//    @Expose
//    @SerializedName("page_count")
//    var PageCount: Int,
//    @Expose
//    @SerializedName("total_results")
//    var TotalResults: Int,
//    @Expose
//    @SerializedName("current_page_no")
//    var CurrentPageNo: Int,
//    @Expose
//    @SerializedName("limit")
//    var Limit: Int,
//    @Expose
//    @SerializedName("last_page")
//    var LastPage: Boolean
//)
//
//data class Results(
//    @Expose
//    @SerializedName("id")
//    var Id: Int,
//    @Expose
//    @SerializedName("item_type")
//    val ItemType: String,
//    @Expose
//    @SerializedName("news_title")
//    val NewsTitle: String,
//    @Expose
//    @SerializedName("tags")
//    var Tags: List<String>,
//    @Expose
//    @SerializedName("featured")
//    var Featured: Boolean,
//    @Expose
//    @SerializedName("breaking_news")
//    var BreakingNews: Boolean,
//    @Expose
//    @SerializedName("is_visible")
//    var IsVisible: Boolean,
//    @Expose
//    @SerializedName("time_stamp")
//    val TimeStamp: String,
//    @Expose
//    @SerializedName("is_recommended")
//    var IsRecommended: Boolean,
//    @Expose
//    @SerializedName("thumbnails")
//    var Thumbnails: List<String>,
//    @Expose
//    @SerializedName("share_url")
//    val ShareUrl: String,
//    @Expose
//    @SerializedName("author")
//    val Author: String
//)
data class CategoriesResponse(
    @Expose
    @SerializedName("status")
    var status: Boolean,
    @Expose
    @SerializedName("data")
    var data: Data
)

data class Data(
    @Expose
    @SerializedName("results")
    var results: ArrayList<Results>,
    @Expose
    @SerializedName("meta")
    var meta: Meta
)

data class Meta(
    @Expose
    @SerializedName("page_count")
    var pageCount: Int,
    @Expose
    @SerializedName("total_results")
    var totalResults: Int,
    @Expose
    @SerializedName("current_page_no")
    var currentPageNo: Int,
    @Expose
    @SerializedName("limit")
    var limit: Int,
    @Expose
    @SerializedName("last_page")
    var lastPage: Boolean
)

@Parcelize
data class Results(
    @Expose
    @SerializedName("id")
    var id: String?,
    @Expose
    @SerializedName("item_type")
    val itemType: String?,
    @Expose
    @SerializedName("video_title")
    val videoTitle: String? = "",
    @Expose
    @SerializedName("video_url")
    val videoUrl: String?,
    @Expose
    @SerializedName("video_thumbnail")
    val videoThumbnail: String? = "",
    @Expose
    @SerializedName("duration")
    val duration: String?,
    @Expose
    @SerializedName("time_stamp")
    val timeStamp: Date?,
    @Expose
    @SerializedName("video_id")
    val videoId: String?,
    @Expose
    @SerializedName("is_visible")
    var isVisible: Boolean?,
    @Expose
    @SerializedName("tag")
    var tag: List<String?>?,
    @Expose
    @SerializedName("comments")
    val comments: String?,
    @Expose
    @SerializedName("likes")
    val likes: String?,
    @Expose
    @SerializedName("views")
    val views: String?,
    @Expose
    @SerializedName("news_title")
    val NewsTitle: String?,
    @Expose
    @SerializedName("tags")
    var Tags: List<String?>?,
    @Expose
    @SerializedName("featured")
    var Featured: Boolean?,
    @Expose
    @SerializedName("breaking_news")
    var BreakingNews: Boolean?,


    @Expose
    @SerializedName("is_recommended")
    var IsRecommended: Boolean?,
    @Expose
    @SerializedName("thumbnails")
    var Thumbnails: List<String>?,
    @Expose
    @SerializedName("share_url")
    val ShareUrl: String?,
    @Expose
    @SerializedName("author")
    val Author: String?,

    @Expose
    @SerializedName("thumbnail")
    val thumbnail: String? = "",
    @Expose
    @SerializedName("album_title")
    val albumTitle: String? = ""


) : Parcelable {
    fun getTitle(): String? {
        return if (getItemViewType() == NEWS_ITEM_TYPE)
            NewsTitle
        else if (getItemViewType() == VIDEO_ITEM_TYPE)
            videoTitle
        else
            albumTitle

    }

    fun getThumbNail(): String {
        return if (getItemViewType() == NEWS_ITEM_TYPE)
            Thumbnails!![0]
        else if (getItemViewType() == VIDEO_ITEM_TYPE)
            videoThumbnail?:""
        else {
            return thumbnail?:""
        }
    }

    fun getDurationString(): String {
        return " $duration "
    }

    fun getItemViewType(): String {
        if (itemType == null) {
            return "video"
        } else {
            return itemType
        }
    }

}