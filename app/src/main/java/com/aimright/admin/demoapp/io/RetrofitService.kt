package com.aimright.admin.demoapp.io

import com.aimright.admin.demoapp.io.dto.*
import com.aimright.admin.demoapp.io.dto.AdsResponse
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.*

interface RetrofitService {

    @GET("{Id}")
    fun fetchNewsByCategory(@Path("Id") id: String?): Observable<CategoriesResponse>

    @GET("{Id}")
    fun fetchNewsByCategoryLoad(@Path("Id") id: String?, @Query("page")  page:Int): Observable<CategoriesResponse>

    @GET("{Id}")
    fun fetchNewsByCategory(@Path("Id") id: String?, @Query("page")  page:Int,  @Query("page_size")  pageSize:Int): Observable<CategoriesResponse>

    @GET("news/listbyid/{Id}")
    fun fetchNewsDetail(@Path("Id") id: String, @Query("version") version: Int, @Query("item_type") itemType: String): Observable<NewsDetailResponse>

    @GET("category/list")
    fun fetchCategories(): Observable<CategoryListResponse>



    @POST("api/push/token")
    fun pushTokenToServer(@Body pushNotificationRequest: PushNotificationRequestDto): Observable<PushNotificationResponseDto>

    //    @GET("news/homepage?page=2&page_size=200")
    @GET("news/homepage?page_size=200")
    fun fetchHomeNews(): Observable<HomeScreenResponseDto>

    @GET("livestream/list")
    fun fetchLiveTvUrl(): Observable<LiveTvResponseDto>

    @GET("find")
    fun searchFromServer(@Query("search") searchString: String): Observable<SearchResponseDto>


    @GET("api/push/listdata")
    fun fetchNotificationsList(): Observable<NotificationsResponseDto>

    @GET("district/list")
    fun getdistictList(): Observable<District>

    @GET("news/get_by_category/{Id}")
    fun fetchNewsByPagination(@Path("Id") id: String?, @Query("page_size") pageSize: Int, @Query("page") page: Int): Single<NewsPagingResponse>

    @GET("api/ads/list/mobile/category/{Id}")
    fun fetchAdsForcategory(@Path("Id") path: String?, @Query("location") location: String): Single<AdsResponse>

    @GET("api/v1/results/cms/urldata")
    fun fetchElectionUrl(): Single<ElectionDto>
}