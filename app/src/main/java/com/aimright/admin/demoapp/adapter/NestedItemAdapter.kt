package com.aimright.admin.demoapp.adapter

import android.content.Context
import android.support.design.widget.TabLayout
import android.support.v4.widget.DrawerLayout
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.aimright.admin.demoapp.R
import com.aimright.admin.demoapp.io.dto.CategoryDataEntity
import com.aimright.admin.demoapp.model.Subcategory
import com.aimright.admin.demoapp.ui.WEBVIEW_TYPE


class NestedItemAdapter// Constructor of the class
    (//All methods in this adapter are required for a bare minimum recyclerview adapter
    private val listItemLayout: Int,
    private val itemList: ArrayList<Subcategory>,
    private val context: Context?,
    private val tabs: TabLayout,
    private val categoriesList: ArrayList<CategoryDataEntity>,
    private val drawerLayout: DrawerLayout
) : RecyclerView.Adapter<NestedItemAdapter.ViewHolder>() {
    private val subCategory: ArrayList<Subcategory> = ArrayList()

    // get the size of the list
    override fun getItemCount(): Int {
        return itemList?.size ?: 0
    }


    // specify the row layout file and click for each row
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.navigation_nested_item, parent, false)
        return ViewHolder(view)
    }

    // load data in each row element
    override fun onBindViewHolder(holder: ViewHolder, listPosition: Int) {
        val item = holder.item
        item.setText(itemList.get(listPosition).category_name)

        holder.nestedList.setLayoutManager( LinearLayoutManager(context))
        holder.nestedList.setItemAnimator( DefaultItemAnimator())



        if (itemList.get(listPosition).subcategory.size!=0)
        {
            holder.items_present.visibility=View.VISIBLE

        }
        else
        {
            holder.items_present.visibility=View.GONE
        }

        holder.itemView.setOnClickListener{
            if (itemList.get(listPosition).subcategory.size!=0)
            {

                if (holder.nestedList.visibility==View.GONE)
                {
                    holder.nestedList.visibility=View.VISIBLE
                    val subList=itemList.get(listPosition).subcategory as Array
                    for (i in 0 until subList.size)
                    {
                        subCategory.add(subList[i])

                    }
                    val nestedItemAdapter =
                        NestedItemAdapter(
                            R.layout.navigation_parent_item,
                            subCategory,
                            context,
                            tabs,
                            categoriesList,
                            drawerLayout
                        )
                    holder.nestedList.setAdapter(nestedItemAdapter)
                    nestedItemAdapter.notifyDataSetChanged()
                }
                else if (holder.nestedList.visibility==View.VISIBLE)
                {
                    holder.nestedList.visibility=View.GONE


                }
            }
            else{
                holder.nestedList.visibility=View.VISIBLE
                val subList=itemList.get(listPosition).subcategory as Array

                tabs.removeAllTabs()
                categoriesList.clear()
                for (i in 0 until itemList.size)
                {
                    val item= itemList.get(i)
                    categoriesList.add(CategoryDataEntity(emptyList(),item.id, item.category_name, "", "",  true, item.category_order_number.toInt(), true, WEBVIEW_TYPE
                    , item.fetch_url))
                }

                categoriesList.forEach{
                    val newTab = tabs.newTab()
                    newTab.text = it.categoryName
                    newTab.tag = it
                    tabs.addTab(newTab)
                }



                subList.sortedBy { it.category_order_number }
                drawerLayout!!.closeDrawers()


            }

        }



    }

    // Static inner class to initialize the views of rows
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        var item: TextView
        var nestedList: RecyclerView
        var items_present:ImageView
        init {
            itemView.setOnClickListener(this)
            item = itemView.findViewById(R.id.row_item)
            nestedList=itemView.findViewById(R.id.nested_list)
            items_present=itemView.findViewById(R.id.items_present)
        }

        override fun onClick(view: View) {
            Log.d("onclick", "onClick " + layoutPosition + " " + item.text)
        }
    }
}