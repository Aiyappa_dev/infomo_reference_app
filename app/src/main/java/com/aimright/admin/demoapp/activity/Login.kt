package com.aimright.admin.demoapp.activity

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInResult

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.TextUtils
import android.util.Base64
import android.view.View
import com.aimright.admin.demoapp.R
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.*
import com.aimright.admin.demoapp.ui.HomeScreenActivity
import com.aimright.admin.demoapp.ui.IS_FROM_PUSH
import com.aimright.admin.demoapp.ui.ITEM_ID
import com.aimright.admin.demoapp.ui.PUSH_NOTIFICATION_TYPE
import kotlinx.android.synthetic.main.activity_login2.*
import java.security.MessageDigest
import java.util.*


class Login : AppCompatActivity() {
    private var callbackManager: CallbackManager? = null
 //   private lateinit var signInButton: SignInButton
    //Google Login Request Code
    private val RC_SIGN_IN = 7
    //Google Sign In Client
    private lateinit var mGoogleSignInClient: GoogleSignInClient
    //Firebase Auth
    private lateinit var mAuth: FirebaseAuth
 private  val EMAIL = "email";

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login2)
        mAuth = FirebaseAuth.getInstance()

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken("974817153880-snrvqtm6gjiubko2443n8mk53ohd36cg.apps.googleusercontent.com")
            .requestEmail()
            .build()

        mGoogleSignInClient = GoogleSignIn.getClient(this,gso)
      /*  signInButton=findViewById(R.id.sign_in_button)
        signInButton.setOnClickListener({
            signIn()
        })*/

        printKeyHash()
        registerOrSignUp()

        signInEmail()

    }

    private fun signInEmail() {
        sign_in.setOnClickListener {
            val email1 = email.getText().toString();
            val password1 = pwd.getText().toString();

            if (TextUtils.isEmpty(email1)) {
                email.setError("Enter email address!")
               // Toast.makeText(getApplicationContext(), "Enter email address!", Toast.LENGTH_SHORT).show();
                return@setOnClickListener;
            }

            if (TextUtils.isEmpty(password1)) {
                pwd.setError("Enter password!")
               // Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show();
                return@setOnClickListener;
            }

            progressBar.setVisibility(View.VISIBLE);

            mAuth.signInWithEmailAndPassword(email1, password1).addOnCompleteListener {
                progressBar.setVisibility(View.GONE)
                if (!it.isSuccessful()) {
                    // there was an error
                    if (password1.length < 6) {
                        pwd.setError("Password too short, enter minimum 6 characters!")
                    } else {
                        Toast.makeText(this, "Authentication Failed", Toast.LENGTH_LONG).show();
                    }
                } else {

                    launchHomeScreen()

                    /* Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                     startActivity(intent);
                     finish();*/
                }
            }
        }

                //authenticate user
              /*  mAuth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(this@Login, {

                            fun onComplete( task :  Task<AuthResult>) {
                                // If sign in fails, display a message to the user. If sign in succeeds
                                // the auth state listener will be notified and logic to handle the
                                // signed in user can be handled in the listener.
                                progressBar.setVisibility(View.GONE)
                                if (!task.isSuccessful()) {
                                    // there was an error
                                    if (password.length() < 6) {
                                        inputPassword.setError(getString(R.string.minimum_password));
                                    } else {
                                        Toast.makeText(LoginActivity.this, getString(R.string.auth_failed), Toast.LENGTH_LONG).show();
                                    }
                                } else {

                                    launchHomeScreen()

                                   *//* Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                    startActivity(intent);
                                    finish();*//*
                                }
                            }
                        })*/

    }

    private fun registerOrSignUp() {
        register.setOnClickListener {
         val intent=Intent(this@Login, RegisterAll::class.java)
            startActivity(intent)
        }
    }

    private fun printKeyHash() {

            val info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES)


            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                val hashKey =  String(Base64.encode(md.digest(), 0));
                Log.i("test", "printHashKey() Hash Key: " + hashKey);
            }




        callbackManager = CallbackManager.Factory.create()
       val loginButton =  findViewById<com.facebook.login.widget.LoginButton>(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList(EMAIL));

        // login_button.setReadPermissions("email", "public_profile")


        val sharedPreferences = getSharedPreferences("Reg", Context.MODE_PRIVATE);
        val  uName = sharedPreferences.getBoolean("isFacebookLogged", false);

        if (uName==false)
        {
            LoginManager.getInstance().logOut()
        }
        login_button.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                Log.d("test", "facebook:onSuccess:$loginResult")
               //profilePic= Profile.getCurrentProfile().getProfilePictureUri(200, 200).toString()
              //  userName=Profile.getCurrentProfile().firstName+" "+Profile.getCurrentProfile().lastName

            val accessToken = AccessToken.getCurrentAccessToken();
           val isLoggedIn = accessToken != null && !accessToken.isExpired();


              //  handleFacebookAccessToken(loginResult.accessToken)
               val  sharedPreferences = getApplicationContext().getSharedPreferences("Reg", 0);
             val   editor = sharedPreferences.edit();
                editor.putBoolean("isFacebookLogged",true);
                editor.commit();
                launchHomeScreen()
            }

            override fun onCancel() {
                Log.d("test", "facebook:onCancel")
                // ...
            }

            override fun onError(error: FacebookException) {
                Log.d("test", "facebook:onError", error)
                // ...
            }
        })



// ..
    }




    private fun signIn() {
        val signInIntent = mGoogleSignInClient.getSignInIntent()
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        // Pass the activity result back to the Facebook SDK
        callbackManager!!.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {

            val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            handleSignInResult(result)


            /*val task = GoogleSignIn.getSignedInAccountFromIntent(data)

            try {
                // Google Sign In was successful, authenticate with Firebase
                val account = task.getResult(ApiException::class.java)
                if (account != null) {
                    firebaseAuthWithGoogle(account)
                }
            } catch (e: ApiException) {
                // Google Sign In failed, update UI appropriately
                Log.w("Login", "Google sign in failed", e)
                // ...
            }*/

        }
    }



    private fun handleSignInResult(result: GoogleSignInResult) {
        if (result.isSuccess) {
            val account = result.signInAccount
           val idToken = account!!.idToken
           val name = account.displayName
           val email = account.email
            // you can store user data to SharedPreference
            val credential = GoogleAuthProvider.getCredential(idToken, null)
            firebaseAuthWithGoogle(credential)
        } else {
            // Google Sign In failed, update UI appropriately
            Log.e("test", "Login Unsuccessful. $result")
            Toast.makeText(this, "Login Unsuccessful", Toast.LENGTH_SHORT).show()
        }
    }

    /*private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {
        Log.d("Login", "firebaseAuthWithGoogle:" + acct.id!!)

        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
        mAuth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("Login", "signInWithCredential:success")
                    val user = mAuth.currentUser
                    updateUI(user)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("Login", "signInWithCredential:failure", task.exception)
                    Toast.makeText(this,"Auth Failed",Toast.LENGTH_LONG).show()
                    updateUI(null)
                }

                // ...
            }
    }*/

    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
    }

    private fun firebaseAuthWithGoogle(credential: AuthCredential) {

        mAuth.signInWithCredential(credential)
            .addOnCompleteListener(this,
                OnCompleteListener<AuthResult> { task ->

                    if (task.isSuccessful) {
                        if (intent.extras != null && intent.extras.getString(ITEM_ID) != null ) {
//            launchHomeScreen()
//            launchFromPush()
                            val subscribe = Completable.timer(0, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                                .subscribe(this::launchFromPush)
                        } else {


                             val subscribe = Completable.timer(0, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                                 .subscribe(this::launchHomeScreen)
                        }

                    }
                    else {
                        Log.w(
                            "test",
                            "signInWithCredential" + task.exception!!.message
                        )
                        task.exception!!.printStackTrace()
                        Toast.makeText(
                            this@Login, "Authentication failed.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                })
    }

    private fun handleFacebookAccessToken(token: AccessToken) {
        Log.d("test", "handleFacebookAccessToken:$token")

        val credential = FacebookAuthProvider.getCredential(token.token)
        mAuth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("test", "signInWithCredential:success")


                        if (intent.extras != null && intent.extras.getString(ITEM_ID) != null ) {
//            launchHomeScreen()
//            launchFromPush()
                            val subscribe = Completable.timer(0, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                                .subscribe(this::launchFromPush)
                        } else {


                            val subscribe = Completable.timer(0, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                                .subscribe(this::launchHomeScreen)
                        }
                    val user = mAuth.currentUser
                    updateUI(user)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("test", "signInWithCredential:failure", task.exception)
                    Toast.makeText(baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                    updateUI(null)
                }

                // ...
            }
    }

    private fun launchFromPush() {
        val homeIntent = Intent(this, HomeScreenActivity::class.java)
        homeIntent.putExtra(IS_FROM_PUSH, true)
        homeIntent.putExtra(ITEM_ID, intent.extras.getString(ITEM_ID))
        homeIntent.putExtra(IS_FROM_PUSH, intent.extras.getString(PUSH_NOTIFICATION_TYPE) as String)
        startActivity(intent)
    }

    private fun launchHomeScreen() {
        intent.extras
        val intent = Intent(this, HomeScreenActivity::class.java)
        if (intent.extras != null)
            intent.putExtras(intent.extras)
        startActivity(intent)
        finish()
//        FirebaseMessaging.getInstance().isAutoInitEnabled = true

    }


        override fun onStart() {
        super.onStart()
// Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = mAuth.currentUser
       // userName=currentUser.toString()
        updateUI(currentUser)
    }

    override fun onStop() {
        super.onStop()

    }
    fun updateUI(user: FirebaseUser?){
        if(user != null){
            //Do your Stuff
            Toast.makeText(this,"Hello ${user.displayName}",Toast.LENGTH_LONG).show()
        }
    }

    companion object
    {
        // var userName:String=""
         //var profilePic:String=""
    }
}
