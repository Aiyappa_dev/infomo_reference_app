package com.aimright.admin.demoapp.ui.paging

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.aimright.admin.demoapp.R
import com.aimright.admin.demoapp.io.dto.NewsEntity
import com.aimright.admin.demoapp.ui.NEWS_LIST
import com.aimright.admin.demoapp.ui.NEWS_SELECTED_POSITION
import com.aimright.admin.demoapp.ui.NewsDetailPagerActivity
import com.squareup.picasso.Picasso

class NewsPagingAdapter(private val newsPagingList: ArrayList<NewsEntity>) :
    RecyclerView.Adapter<NewsPagingAdapter.NewspagingViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, itemType: Int): NewspagingViewHolder {
        return NewspagingViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.news_list_item, parent, false))
    }

    override fun getItemCount(): Int {
        return newsPagingList.size
    }

    override fun onBindViewHolder(pagingHolder: NewspagingViewHolder, position: Int) {

        val newsItem = newsPagingList[position]
        pagingHolder.itemView.setOnClickListener {
            val intent = Intent(pagingHolder.itemView.context, NewsDetailPagerActivity::class.java)
            intent.putParcelableArrayListExtra(NEWS_LIST, newsPagingList)
            intent.putExtra(NEWS_SELECTED_POSITION, position)
            pagingHolder.itemView.context.startActivity(intent)
        }

        pagingHolder.title.text = newsItem.newsTitle
        val now = System.currentTimeMillis()

        val relativeTimeSpanString = DateUtils.getRelativeTimeSpanString(
            newsItem.timeStamp.time,
            now,
            DateUtils.MINUTE_IN_MILLIS
        )
        pagingHolder.timestampTv.text = relativeTimeSpanString

        if (newsItem.thumbnails.isNotEmpty() && newsItem.thumbnails[0].isNotEmpty()) {
            Picasso.with(pagingHolder.itemView.context)
                .load(newsItem.thumbnails[0])
                .fit()
                .centerCrop()
                .placeholder(R.drawable.ic_more_horiz_grey)
                .into(pagingHolder.newsListImageView)
        } else {
            pagingHolder.newsListImageView.setImageResource(R.drawable.ic_more_horiz_grey)
        }


        pagingHolder.moreIv.visibility = View.GONE

        pagingHolder.headerLayout.visibility = View.GONE

        if (newsItem.author != null) {
            val authorText = "Reported By - " + newsItem.author
            pagingHolder.authorTextView.text = authorText
        }
        pagingHolder.categoryTextView.visibility = View.GONE
    }


    inner class NewspagingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title = itemView.findViewById(R.id.category_title_tv) as TextView
        val timestampTv = itemView.findViewById(R.id.news_timestamp) as TextView
        val newsListImageView = itemView.findViewById(R.id.news_list_IV) as ImageView
        val moreIv: ImageView = itemView.findViewById(R.id.more_IV)
        val videoDurationTv = itemView.findViewById(R.id.videoDuration) as TextView
        val authorTextView = itemView.findViewById(R.id.reportedBy) as TextView
        val categoryTextView = itemView.findViewById(R.id.category) as TextView
        val headerLayout = itemView.findViewById(R.id.header) as LinearLayout
    }


    fun addItems(list: ArrayList<NewsEntity>) {
        newsPagingList.addAll(list)
        notifyItemRangeInserted(newsPagingList.size - list.size+1, list.size)
    }

}