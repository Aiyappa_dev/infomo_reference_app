package com.aimright.admin.demoapp.ui

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.aimright.admin.demoapp.io.dto.Results

class NewsPagerAdapter(private val newsItemsList: ArrayList<Results>, fragmentManager: FragmentManager) :
    FragmentPagerAdapter(fragmentManager) {


    override fun getItem(position: Int): Fragment = NewsDetailFragment.newInstance(newsItemsList[position].id!!,newsItemsList[position].getItemViewType())


    override fun getCount(): Int = newsItemsList.size

}